This project comprises the code and various resources that form the (web) client part of the NOMAD Encyclopedia (Encyclopedia GUI). 
The app is a frontend running on the web browser that retrieves matrial data from the Encyclopedia API and represents it in a convenient way for the user.

The app has been built as a modern modular SPA. It's a vanilla JavaScript application, using the ECMAScript 6 standard. Webpack is used as code builder. 
So the only requirement to build this code is the use of Webpack. For old browsers support the Babel transpiler must be used.


Example of Webpack use:

`cd PROJECTS/encyclopedia-gui/client/`

`webpack --watch`

Example of Babel use:

`babel client/bundle.js -o client/bundle-es5.js`

