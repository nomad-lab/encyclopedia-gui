
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This file is the application entry point.
   It defines some app level components (Breadcrumb) and
   initializes several more (app level events, app routing, authentication)
 */


"use strict";

let util = require('./common/util.js');
let LoadingPopup = require('./common/LoadingPopup.js');
let FlaggingFormPopup = require('./common/FlaggingFormPopup.js');
let PubSub = require('./common/PubSub.js');
let Router = require('./common/Router.js');
let MaterialMod = require('./material-mod/MaterialMod.js');
let SearchModule = require('./search-mod/NewSearchMod.js');
let UserGuidance = require('./common/UserGuidance.js');
let DataStore = require('./material-mod/DataStore.js');


// main DOM elements
let contentElement = document.getElementById('content');
let titleElement = document.querySelector('title');


/********* User flagging side tab ****************/

/* This side vertical tab is hidden initially
   but it has to be set up when the app starts */

let flaggingTab = document.getElementById('calc-flagging-tab');
flaggingTab.style.top = (window.innerHeight/2)+'px';

flaggingTab.addEventListener('click',e => {
  FlaggingFormPopup.show(MaterialModule.getCurrentPageStatus());
});



/*********** App Breadcrumb component definition ***************/

class Breadcrumb {

  constructor() {

    this.element = document.querySelector('#breadcrumb-placeholder');
    this.element.innerHTML = `
      <span class="goto-page Search">Search</span>
      <span class="goto-page Overview">&nbsp; > &nbsp; <span>Overview</span></span>
      <span class="Details">
        &nbsp; > &nbsp;
        <select class="details-dropdown" >
          <option value="structure">Structure</option>
          <option value="electronicstruct">Electronic structure</option>
          <option value="methodology">Methodology</option>
          <option value="thermalprops">Thermal Properties</option>
          <!-- elasticconst-->
        </select>
      </span>
    `;
    this.overviewSel = this.element.querySelector('.Overview');
    this.detailsSel = this.element.querySelector('.Details');
    this.detailsDropDown = this.element.querySelector('.details-dropdown');

    // Events
    this.element.querySelector('.Search').addEventListener( "click", e => {
      util.setBrowserHashPath('search');
    });

    this.overviewSel.addEventListener('click', () => {
      util.setBrowserHashPath('material', util.materialId);
    });

    this.detailsDropDown.addEventListener('change', e => {
      util.setBrowserHashPath('material',
        DataStore.getMaterialData().id+'/'+e.target.value);
    });

    let self = this;
    function adjustDropdownOptions() {
      let esOption = self.detailsDropDown.querySelector('option[value="electronicstruct"]');
      if (!DataStore.hasElecStructureData) self.detailsDropDown.removeChild(esOption);

      let thOption = self.detailsDropDown.querySelector('option[value="thermalprops"]');
      if (!DataStore.hasThermalData) self.detailsDropDown.removeChild(thOption);
      // Remove because we want it's executed once
      self.detailsDropDown.removeEventListener('focus', adjustDropdownOptions);
    }

    this.detailsDropDown.addEventListener('focus', adjustDropdownOptions);
  }


  setState(appModule, param){
    let overviewSelLabel = this.overviewSel.querySelector('span');
    overviewSelLabel.style.fontWeight = 'normal';

    if (appModule === 'search'){
      this.overviewSel.style.display = 'none';
      this.detailsSel.style.display = 'none';

      /*
      if (param === 'results'){
        this.resultsSel.style.display = 'inline';
        this.resultsSel.querySelector('span').style.fontWeight = 'bold';
        this.element.style.visibility = 'visible';
      }else   this.element.style.visibility = 'hidden';
      */
      this.element.style.visibility = 'hidden';

    }else if (appModule === 'material'){
      this.element.style.visibility = 'visible';
      this.overviewSel.style.display = 'inline';

      if (param === undefined){ // Overview page
        this.detailsSel.style.display = 'none';
        overviewSelLabel.style.fontWeight = 'bold';
      }else{ // Details page
        this.detailsSel.style.display = 'inline';
        this.detailsDropDown.value = param;
      }
    }
  } // setState

} // class Breadcrumb


/***************************
         App setup
***************************/

let breadcrumb = new Breadcrumb();

let searchMod;
let MaterialModule;
let materialModDOM;
let currentModule; // current module DOM being shown


function showModuleDOM(module){
  if (currentModule) contentElement.removeChild(currentModule);
  currentModule= module;
  contentElement.appendChild(currentModule);
}


/******  App level events setup ********/

PubSub.subscribe('show-material', data => {
  console.log('Handling event show-material: '+data.id+' view: '+data.view);

  //titleElement.innerHTML = 'NOMAD Encyclopedia - Material '+data.id;
  breadcrumb.setState('material',data.view);

  if (typeof materialModDOM === 'undefined'){
    MaterialModule = new MaterialMod();
    materialModDOM= MaterialModule.element;
  }
  MaterialModule.setMaterialView(data);
  showModuleDOM(materialModDOM);

  // In case the app comes from the search module through the url (back button)
  UserGuidance.show(false);

  //console.log('User data:',util.getUserData());
  if (util.getUserData() !== null) flaggingTab.style.visibility = 'visible';
});


PubSub.subscribe('show-search', search => {
  console.log('Handling event show-search: '+search);

  titleElement.innerHTML = 'NOMAD Encyclopedia - Search';
  breadcrumb.setState('search',search);

  if (search === undefined){
    searchMod.showSearchPage();
    LoadingPopup.hide();  // In case it comes from the result page

  }//else if (search === 'results')  searchMod.showSearchResults();

  showModuleDOM(searchMod.element);

  if (flaggingTab.style.visibility !== 'hidden')
    flaggingTab.style.visibility = 'hidden';
});



/******  App routing config  ******/

Router.add('search', search => PubSub.publish('show-search', search));
Router.add('material', (matId, view) => PubSub.publish('show-material', {'id': matId, 'view': view}));



/****** init ******/

searchMod = new SearchModule();

//console.log('document.location: '+document.location.hash);
if (document.location.hash === '') document.location += "#/search";
Router.route();


/********* User authentication ***********/

let userNameElement = document.querySelector('#user-name');
let logoutButton = document.querySelector('#logout-button');

function setAppAuthenticated(data){
  if (data.status === 'Authenticated'){
    userNameElement.innerHTML = data.user.username;
    document.querySelector('#guest-user').style.display = 'none';
    document.querySelector('#auth-user').style.display = 'inline';
    util.setAuthRequestHeader(data.user, data.token.data);
    if (currentModule === materialModDOM) flaggingTab.style.visibility = 'visible';
  }
}


function setAppLoggedOut(){
  userNameElement.innerHTML = '';
  document.querySelector('#guest-user').style.display = 'inline';
  document.querySelector('#auth-user').style.display = 'none';
  util.setAuthRequestHeader();

  if (flaggingTab.style.visibility !== 'hidden')
    flaggingTab.style.visibility = 'hidden';
}


function getCookie(name) {
  let value = "; " + document.cookie;
  let parts = value.split("; " + name + "=");
  if (parts.length === 2) return parts.pop().split(";").shift();
}


function parseCookie(userData) {
  return userData.substring(1, userData.length-1).replace(/\\054/g,',').replace(/\\/g,'');
}


let userInfoCookie = getCookie('user_info');
//console.log('Cookies: ', document.cookie, userInfoCookie);

if (userInfoCookie !== undefined){
  let userInfoData = JSON.parse(parseCookie(userInfoCookie));
  //console.log('userInfoData: ', userInfoData);
  setAppAuthenticated(userInfoData);
}


// Logout
logoutButton.addEventListener( "click", e => {

  document.cookie='user_info=; expires=Thu, 01 Jan 1970 00:00:00 GMT; domain='+
    util.USER_COOKIE_DOMAIN+'; path=/';
  //console.log('Logging out document.cookie ',document.cookie);

  setAppLoggedOut();
  //console.log('Logging out ',userNameElement.innerHTML);
});
