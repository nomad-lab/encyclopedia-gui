
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This component implements the list of materials found in the search
 */

"use strict";

let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let LoadingPopup = require('../common/LoadingPopup.js');

const RESULTS_PER_PAGE = 20;


class MaterialList {

  constructor(){
    this.element = document.createElement('div');
    this.element.setAttribute("id",'matlist');
    // map containing an entry per system type (if there are materials for it)
    this.matMap = new Map();
    this.currentSystemType = 'bulk';
    this.esQueryObject = null;

    this.element.innerHTML=
    `
    <div id="system-type-tabs" style=" display: inline-block">
      <button id="system-tab-bulk" class="selected">BULK</button>
      <button id="system-tab-2D" >2D</button>
      <button id="system-tab-1D"  >1D</button>
        <span class="results-total" >Results<span>
    </div>
    <div class="pag-header">
      <span class="prevButton">
        <img src="img/prev.svg" style="display: inline;" width="7px"/> &nbsp; prev
      </span>   &nbsp;&nbsp;
      <span class="page"> X </span> &nbsp;&nbsp;
      <span class="nextButton"> next &nbsp;
        <img src="img/next.svg" width="7px" />
      </span>
    </div>
    <div class="data-container"> </div>`; // results data container

    this.titleBox = this.element.getElementsByClassName("results-total")[0];
    this.tabsBox = this.element.querySelector("#system-type-tabs");
    // Pagination components
    this.resultsNrTag = this.element.getElementsByClassName("pag-header")[0];

    this.prevButton = this.element.getElementsByClassName("prevButton")[0];
    this.pageElement = this.element.getElementsByClassName("page")[0];
    this.nextButton = this.element.getElementsByClassName("nextButton")[0];

    this.resultsContainer = this.element.getElementsByClassName("data-container")[0];

    this._hide();
  }


  attachAndSetEvents(element){
    element.appendChild(this.element);
    this._events();
  }

  _hide() {
    this.element.style.display = 'none';
  }

  _show() {
    this.element.style.display = 'block';
  }

  _events() {

    this.tabsBox.addEventListener( "click", (e) => {
      if (e.target !== e.currentTarget) { // When the event source is a child
        let tabId = e.target.id;
        let index = tabId.indexOf('system-tab');

        if (index >= 0){
          let selectingTab = tabId.substring('system-tab'.length+1);

          this._updateUI(selectingTab);
          // Change the styles of the tabs
          this.element.querySelector('#system-tab-'+this.currentSystemType).className = '';
          this.element.querySelector('#system-tab-'+selectingTab).className = 'selected';

          this.currentSystemType = selectingTab;
        }
      }

    });

    this.nextButton.addEventListener('click', e => {
      let systemData = this.matMap.get(this.currentSystemType);
      if (systemData.page === systemData.totalPages) return;
      systemData.page++;
      this._paginationSearch();
    });

    this.prevButton.addEventListener('click', e =>  {
      let systemData = this.matMap.get(this.currentSystemType);
      if (systemData.page === 1) return;
      systemData.page--;
      this._paginationSearch();
    });

    this.resultsContainer.addEventListener('click', (e) => {
      if (e.target !== e.currentTarget) { // When the event source is a child

        let element;
        if (e.target.className === 'mat-row') element = e.target;
        else if (e.target.parentElement.className === 'mat-row')
          element = e.target.parentElement;
        else if (e.target.parentElement.parentElement.className === 'mat-row')
          element = e.target.parentElement.parentElement;

        if (element) {
          util.setBrowserHashPath('material', element.getAttribute('data-mat-id'));
        }
        e.stopPropagation();
      }
    });

  }


  _paginationSearch(){

    let systemTypePosition = this.esQueryObject.bool.must.length-1;
    this.esQueryObject.bool.must[systemTypePosition].match.system_type = this.currentSystemType;
    let page = this.matMap.get(this.currentSystemType).page;

    let req = util.serverReqPOST(util.getSearchURL()+'?page='+page+
            '&per_page='+RESULTS_PER_PAGE, JSON.stringify(this.esQueryObject), e => {

      let data = JSON.parse(e.target.response);

      this.matMap.set(this.currentSystemType, this._createSystemTypeData(data, page, true));
      //console.log('this.matMap: ', this.matMap);
      this._updateUI(this.currentSystemType);
    });

  }


  initSearchTest(rootQueryObj){
    //this.resultsContainer.style.visibility = 'hidden';
    this.matMap.clear();

    let qqq = { 'bool' : {} };
    //qqq.bool.must = {"terms":{"crystal_system":{"operator":"or","query":"cubic"}}};
    qqq.bool.must = {"terms":{"crystal_system": ["hexagonal","cubic"]}};

    console.log('SENDING: ', JSON.stringify(qqq)); console.log(': ', qqq.bool.must);

    let bulkReq = util.serverReqPOST(util.getSearchURL()+'?page=1'+
            '&per_page='+RESULTS_PER_PAGE, JSON.stringify(qqq), bulke => {

      let bulkData = JSON.parse(bulke.target.response);  console.log('GETTING: ', bulkData);

      this.matMap.set('bulk', this._createSystemTypeData(bulkData, 1, bulke.target.status === 200));
      LoadingPopup.hide();
    });

  }


  initSearch(rootQueryObj){
    //this.resultsContainer.style.visibility = 'hidden';
    this.matMap.clear();

    rootQueryObj.bool.must.push( { "match": {"system_type" : "bulk"} } );
    console.log('SENDING: ', JSON.stringify(rootQueryObj)); console.log(': ', rootQueryObj.bool.must);
    let systemTypePosition = rootQueryObj.bool.must.length-1;
    this.esQueryObject = rootQueryObj;

    LoadingPopup.show();

    //let oReq = util.serverReqPOST('http://enc-staging-nomad.esc.rzg.mpg.de/current/v1.0/esmaterials', postQuery, e => {
    // Bulk materials request
    let bulkReq = util.serverReqPOST(util.getSearchURL()+'?page=1'+
            '&per_page='+RESULTS_PER_PAGE, JSON.stringify(rootQueryObj), bulke => {

      let bulkData = JSON.parse(bulke.target.response);  console.log('GETTING: ', bulkData);

      this.matMap.set('bulk', this._createSystemTypeData(bulkData, 1, bulke.target.status === 200));


      // 2D materials request
      rootQueryObj.bool.must[systemTypePosition].match.system_type = '2D';
      let twoDReq = util.serverReqPOST(util.getSearchURL()+'?page=1'+
              '&per_page='+RESULTS_PER_PAGE, JSON.stringify(rootQueryObj), twoDe => {

        let twoDData = JSON.parse(twoDe.target.response);

        this.matMap.set('2D', this._createSystemTypeData(twoDData, 1, twoDe.target.status === 200));


        // 1D materials request
        rootQueryObj.bool.must[systemTypePosition].match.system_type = '1D';
        let oneDReq = util.serverReqPOST(util.getSearchURL()+'?page=1'+
                '&per_page='+RESULTS_PER_PAGE, JSON.stringify(rootQueryObj), oneDe => {

          let oneDData = JSON.parse(oneDe.target.response);

          this.matMap.set('1D', this._createSystemTypeData(oneDData, 1, oneDe.target.status === 200));

          let selectedSystemType = null;
          this.matMap.forEach( (materials, systemType) => {
            this.element.querySelector('#system-tab-'+systemType).disabled =
              (materials.total === 0);
            if (selectedSystemType === null && materials.total > 0) {
              selectedSystemType = systemType;
              this.element.querySelector('#system-tab-'+this.currentSystemType).className = '';
              this.element.querySelector('#system-tab-'+selectedSystemType).className = 'selected';

              this.currentSystemType = selectedSystemType;
            }
          });
          rootQueryObj.bool.must[systemTypePosition].match.system_type = this.currentSystemType;
          this._updateUI(this.currentSystemType);

          //this.resultsContainer.style.visibility = 'visible';
          this._show();
          LoadingPopup.hide();
          this._launchMaterialViewerIfOnlyOne();
        });

      });

    });

/*
    oReq.addEventListener("error", e => { // Not valid query
      console.log('Search ERROR - Not valid query ');
      this.total_results= 0;
      this.setData([]);
      this._updateUI();
      this.resultsContainer.style.visibility = 'visible';
      LoadingPopup.hide();
    });
*/


  }

  _launchMaterialViewerIfOnlyOne(){
    let mat;
    let sum = 0;
    this.matMap.forEach( (materials, systemType) => {
      sum += materials.total;
      if (materials.total === 1)
        mat = materials.materials.values().next().value[0];
    });

    if (sum === 1)  util.setBrowserHashPath('material',+(mat.id));
  }


  _createSystemTypeData(data, page, status){

    if (status){
        let systemTypeMatMap = new Map();

        data.results.forEach( mat => {
          if (systemTypeMatMap.has(mat.formula_reduced)){
            let matArray= systemTypeMatMap.get(mat.formula_reduced);
            matArray.push(mat);
          }else{
            let newArray= []; newArray.push(mat);
            systemTypeMatMap.set(mat.formula_reduced, newArray);
          }
        });

        return { 'materials': systemTypeMatMap,
                 'page': page,
                 'total': data.total_results,
                 'totalPages': Math.ceil(data.total_results/RESULTS_PER_PAGE)
               };
    }else return null; // Right query - results not found
      //this.total_results = 0;
  }


  _updateUI(systemType){

    let systemData = this.matMap.get(systemType);
    //console.log('_updateUI',systemType, systemData);
    this.titleBox.innerHTML= 'Results (total: '+systemData.total+')';
    this.pageElement.innerHTML= 'page '+systemData.page+' / '+systemData.totalPages;

    let html = '';

    if (systemData === null) {
      this.resultsNrTag.style.display = 'none';
      this.titleBox.style.display = 'none';

      //if (this.page === 1)
        //html+= `<div class="not-found"> No results found </div>`;
      // If there is search without results IF PAGE == 0 THERE ISN'T SEARCH

    }else{
      this.resultsNrTag.style.display = 'block';
      this.titleBox.style.display = 'inline';

      html +=`
        <table>
          <thead> <tr>
            <th style="width: 24%;"></th>
            <th style="width: 16%;">
              <span info-sys-data="space-group">Space group</span>
            </th>
            <th style="width: 20%;">
              <span >Space gr. int. symbol</span>
            </th>

            <th style="width: 22%;">
              <span info-sys-data="structure-type">Structure type</span>
            </th>
            <th style="width: 18%;">Nº calculations</th>
          </tr> </thead>
          <tbody>
      `;

      systemData.materials.forEach((mats, formula) => {

        let rFormula = util.getSubscriptedFormula(formula);
        html+= '<tr> <td class="formula" colspan="5"><b>'+rFormula+'</b>';
        if ( mats.length > 1)
        html += '<span style="font-size: 0.86em;"> ('+mats.length+' structures)</span>';
        html += '</td></tr>';

        mats.forEach( mat => {
          let label= (mat.material_name !== null ? mat.material_name : rFormula);
          //console.log("MATERIAL ",mat, mat.system_type);
          html+=
          `<tr class="mat-row" data-mat-id="${mat.id}">
            <td  > ${label} [${mat.formula}] </td>
            <td style="text-align:center" >
              ${mat.space_group_number === null ? '' : mat.space_group_number}
            </td>
            <td>
              ${mat.space_group_international_short_symbol === null ? '' :
                mat.space_group_international_short_symbol}
            </td>

            <td> ${mat.structure_type === null ? '' : mat.structure_type } </td>
            <td style="text-align:center" > ${mat.nr_of_calculations} </td>
          </tr>`;
        });
      });

      html +=` </tbody> </table>`;
    }

    this.resultsContainer.innerHTML = html;

    InfoSys.addToInfoSystem(this.resultsContainer);
  }

}

module.exports = MaterialList;
