"filter-quantity-box"

/**
 * Copyright 2016-2019 Iker Hurtado, Georg Huhs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*

 */


"use strict";

let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let AutocompleteMultiselectList = require('./Autocomplete.view.js').AutocompleteMultiselectList;


class FilterPanel {

  constructor() {
    this.element = document.createElement('div');
    this.element.setAttribute("id",'filter-panel-placeholder');
    this.element.innerHTML=
    `     <br>
          <div class="filter-section-box">
          <div class="title">Structure</div>
          <br>
          <div class="filter-quantity-box">
            <div class="field-title">System type</div>
            <input type="checkbox" class="system-type-field" value="bulk"> Bulk<br>
            <input type="checkbox" class="system-type-field" value="2D"> 2D<br>
            <input type="checkbox" class="system-type-field" value="1D"> 1D<br>
          </div>

          <div class="filter-quantity-box">
            <div class="field-title"><span info-sys-data="crystal-system">Crystal system</span></div>
            <input type="checkbox" class="crystal-system-field" value="cubic">
              <span info-sys-data="crystal-system.value:cubic">Cubic</span><br>
            <input type="checkbox" class="crystal-system-field" value="hexagonal">
              <span info-sys-data="crystal-system.value:hexagonal">Hexagonal</span><br>
            <input type="checkbox" class="crystal-system-field" value="trigonal">
              <span info-sys-data="crystal-system.value:trigonal">Trigonal</span><br>
            <input type="checkbox" class="crystal-system-field" value="tetragonal">
              <span info-sys-data="crystal-system.value:tetragonal">Tetragonal</span><br>
            <input type="checkbox" class="crystal-system-field" value="orthorhombic">
              <span info-sys-data="crystal-system.value:orthorhombic">Orthorhombic</span><br>
            <input type="checkbox" class="crystal-system-field" value="monoclinic">
              <span info-sys-data="crystal-system.value:monoclinic">Monoclinic</span><br>
            <input type="checkbox" class="crystal-system-field" value="triclinic">
              <span info-sys-data="crystal-system.value:triclinic">Triclinic</span><br>
          </div>

          <div class="filter-quantity-box">
            <div class="field-title"><span info-sys-data="space-group">Space group</span></div>
            <select id="space-group-dropdown-list">
              <option value="by-number">by number</option>
              <option value="by-symbol">by short symbol</option>
            </select>
            <input type="text" class="space-group-textfield" style="width: 60px">

          </div>

<!--          <div class="filter-quantity-box">
            <div class="field-title"><span info-sys-data="structure-type">Structure type</span></div>
            <select class="structure-type-field" style="max-width: 174px">
              <option ></option>
            </select>
          </div>
-->
          <div class="filter-quantity-box">
            <div class="field-title" id="strukturbericht-title"><span info-sys-data="structure-type">Structure type</span></div>
            <div class="structuretype-placeholder"></div>
          </div>

          <div class="filter-quantity-box">
            <div class="field-title" id="strukturbericht-title">Strukturbericht designation</div>
            <div class="strukturbericht-placeholder"></div>
          </div>

          </div>

          <div class="filter-section-box">

          <div class="title">Properties</div>
          <br>

          <div class="filter-quantity-box">
            <div class="field-title"><span info-sys-data="mass-density">Mass density</span> <span style="font-weight: normal;">(kg/m<sup>3</sup>)</span></div>
            Min: <input type="text" class="mass-density-min-field">&nbsp;&nbsp;
            Max: <input type="text" class="mass-density-max-field">

          </div>

          <!--
          <div class="filter-quantity-box">
            <div class="field-title"><span info-sys-data="band-gap">Band gap</span> <span style="font-weight: normal;">(eV)</span></div>
            Min: <input type="text" class="band-gap-min-field">&nbsp;&nbsp;
            Max: <input type="text" class="band-gap-max-field">
            -->
            <!--
            <input type="range" class="band-gap-slider" min="5" max="200" step="5" value="100"/>
            <input type="range" class="band-gap-slider" min="5" max="200" step="5" value="100"/>
            -->
            <!--
            <div class="field" >
              <input type="radio" name="band-gap-type" value="direct"> Direct<br>
              <input type="radio" name="band-gap-type" value="indirect"> Indirect<br>
              <input type="radio" name="band-gap-type" value="both" checked> Both<br>
            </div>
          </div>
          -->

          <div class="filter-quantity-box">
            <!--<div style="font-weight: bold; padding-bottom: 6px" >Results containing...</div> -->

            <div class="field-title">Results containing...</div>
            <input type="checkbox" class="has-band-structure-field" value="Band structure">
              <span info-sys-data="has-band-structure">Band structure</span><br>
            <input type="checkbox" class="has-dos-field" value="DOS">
              <span info-sys-data="has-dos">DOS</span><br>
            <!--
            <input type="checkbox" class="has-fermi-surface-field" value="Fermi surface">
              <span info-sys-data="has-fermi-surface">Fermi surface</span><br>
            -->

            <input type="checkbox" class="has-thermal-properties-field" value="Thermal properties">
              <span info-sys-data="has-thermal-properties">Thermal properties</span>

          </div>
        </div>

    `;

    this.structuretypeField = new AutocompleteMultiselectList("structuretype");
    this.structuretypeField.element.placeholder = "Search and select options";
    this.structuretypeField.element.classList.add('textfield-filter');
    let structuretypePlaceholder = this.element.querySelector('.structuretype-placeholder');
    this.structuretypeField.replaceElement(structuretypePlaceholder);

    let r1 = util.serverReq(util.getSuggestionURL('structure_type'), (e) => {
      let names = JSON.parse(r1.response).structure_type;
      this.structuretypeField.autocomplete(names);
    });

    this.strukturberichtField = new AutocompleteMultiselectList("strukturbericht");
    this.strukturberichtField.element.placeholder = "Search and select options";
    this.strukturberichtField.element.classList.add('textfield-filter');
    let strukturberichtPlaceholder = this.element.querySelector('.strukturbericht-placeholder');
    this.strukturberichtField.replaceElement(strukturberichtPlaceholder);

    let r2 = util.serverReq(util.getSuggestionURL('strukturbericht_designation'), (e) => {
      let names = JSON.parse(r2.response).strukturbericht_designation;
      this.strukturberichtField.autocomplete(names);
    });

    // let structureTypeSelect  = this.element.querySelector('.structure-type-field');
    // let r1 = util.serverReq(util.getSuggestionURL('structure_type'), () => {
    //   JSON.parse(r1.response).structure_type.forEach( structureType => {
    //     structureTypeSelect.innerHTML += '<option>'+structureType+'</option>';
    //   });
    // });

    InfoSys.addToInfoSystem(this.element);

/*  code for the MaxMinSlider component testing
    this.testSlider = this.element.querySelector('.test-slider');
    console.log("TAB: ",this.testSlider);
    this.slider = new MaxMinSlider();
    this.slider.setRange(0,10000);
    this.testSlider.appendChild(this.slider.element);
*/
  }

  getValues(){
    //let map = this.getPropsWithValueFromCurrentTab(false);
    let filterMap = new Map();
    this.addFilterFromCheckboxes(filterMap,'system-type');
    this.addFilterFromCheckboxes(filterMap, 'crystal-system');
    this.addSpaceGroupFilter(filterMap);//this.addFilterFromTextField(filterMap, 'space-group-number');
    this.addFilterFromMultiSelect(filterMap, 'structure_type', this.structuretypeField);
    this.addFilterFromMultiSelect(filterMap, 'strukturbericht_designation', this.strukturberichtField);

    this.addRangeFilter(filterMap, 'mass-density');
    /* this.addRangeFilter(filterMap, 'band-gap');
    this.addBandgapTypeFilter(filterMap); */

    this.addFiltersFromBoolCheckboxes(filterMap,
      ['has-band-structure', 'has-dos', 'has-thermal-properties']); // 'has-fermi-surface',
    console.log('FilterPanel selected:', filterMap);
    return filterMap;
  }

  addFilterFromTextField(filterMap, filterName){
    let field = this.element.querySelector('.'+filterName+'-field');
    if (field.value !== '')  filterMap.set(filterName, [field.value]);
  }

  addFilterFromCheckboxes(filterMap, filterName){
    let checkboxes = this.element.querySelectorAll('.'+filterName+'-field');
    let value = [];
    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked)  value.push(checkboxes[i].value);
        //if (reset) checkboxes[i].checked = false;
    }
    if (value.length > 0) filterMap.set(filterName, value);
  }

  addFilterFromDropdownList(filterMap, filterName){
    let field = this.element.querySelector('.'+filterName+'-field');
    let value = field.options[field.selectedIndex].value;
    if (value.length > 2) filterMap.set(filterName, [value]);
    //if (reset) field.selectedIndex = 0;
  }

  addFilterFromMultiSelect(filterMap, filterName, multiselectObject){
    let values = multiselectObject.getSelected();
    if (values.length > 0) filterMap.set(filterName, values);
  }

  addSpaceGroupFilter(filterMap){
    let filterName;
    let field = this.element.querySelector('#space-group-dropdown-list');
    let type = field.options[field.selectedIndex].value;
    if (type === 'by-number') filterName = 'space-group-number';
    else filterName = 'space-group-international-short-symbol';
    let value = this.element.querySelector('.space-group-textfield').value;
    if (value.trim() !== '') filterMap.set(filterName, [value]);
    //if (reset) field.selectedIndex = 0;
  }

  addRangeFilter(filterMap, filterName){
    let minField = document.querySelector('.'+filterName+'-min-field');
    let maxField = document.querySelector('.'+filterName+'-max-field');
    let value = ':';
    //let label = 'Mass Density';
    if (minField.value !== ''){
      //label = minField.value+' < '+label;
      value = minField.value+value;
      //if (reset) minField.value = '';
    }
    if (maxField.value !== ''){
      //label += ' < '+maxField.value;
      value = value+maxField.value;
      //if (reset) maxField.value = '';
    }
    if (value !== ':')  filterMap.set(filterName, value);
  }


  addBandgapTypeFilter(filterMap){
    let val = document.querySelector('input[name="band-gap-type"]:checked').value;
    filterMap.set("band-gap-type", val);
  }

  addFiltersFromBoolCheckboxes(filterMap, boolFilters){
    boolFilters.forEach( filterName => {
      let checkboxes = this.element.querySelectorAll('.'+filterName+'-field');
      //let value = [];
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked){
          //value.push(checkboxes[i].value);
          filterMap.set(filterName, true);
        }
      }
      //if (value.length > 0) propsMap.set(propName, value);
    });
  }


  setAddPropertiesListener(listener) {
    this.addPropertiesListener= listener;
  }


}

// EXPORTS
module.exports = FilterPanel;



/*  To be implemented in the future

class MaxMinSlider{

  constructor(){

    this.element = document.createElement('div');
    this.element.innerHTML = `
      <svg class="maxminslider" xmlns="http://www.w3.org/2000/svg" width="100px" height="40px"
        viewBox="0 0 100 40" >
        <line class="slider-bar" x1="10" x2="90" y1="30" y2="30" stroke="blue"
        stroke-width="4"/>
        <circle class="min-btn" cx="10" cy="30" r="6" fill="black"/>
        <text class="min-text maxminslider-text" x="10" y="10" text-anchor="start"></text>
        <circle class="max-btn" cx="90" cy="30" r="6" fill="black"/>
        <text class="max-text maxminslider-text" x="90" y="10" text-anchor="end"></text>
      </svg>
    `;

    //this.bar = this.element.querySelector('.slider-bar');
    this.svg = this.element.querySelector('svg');
    this.minButton = this.element.querySelector('.min-btn');
    this.minText = this.element.querySelector('.min-text');
    this.maxButton = this.element.querySelector('.max-btn');
    this.maxText = this.element.querySelector('.max-text');

    this.BUTTON_R = 6;

    this.minButtonDown = false;
    this.minButtonInitX = null;

    this.MIN_BUTTON_INIT_X = 10;
    this.minX = 0;

    this.maxButtonDown = false;
    this.maxButtonInitX = null;
    this.MAX_VALUE = 80;//this.MAX_BUTTON_INIT_X = 90;
    this.maxX = this.MAX_VALUE;

    console.log('minButton', this.minButton.getBoundingClientRect());

    this._events();
  }


  _events() {

    this.minButton.addEventListener( "mousedown", e => this.minButtonDown = true );
    this.minButton.addEventListener( "mouseup", e => this.minButtonDown = false );
    this.minButton.addEventListener( "mouseleave", e => this.minButtonDown = false );

    this.minButton.addEventListener( "mousemove", e => {
      //e.preventDefault();
      if (this.minButtonInitX === null){
        //this.minButtonInitX = this.svg.getBoundingClientRect().left;
        this.minButtonInitX = this.minButton.getBoundingClientRect().left+this.BUTTON_R;//
        //console.log('left', this.minButtonInitX);
      }

      if (this.minButtonDown){
        this.minX  = e.clientX-this.minButtonInitX ;
        if (this.minX > 0 && this.minX < this.maxX-this.BUTTON_R){
          this.minButton.setAttribute('cx', this.MIN_BUTTON_INIT_X + this.minX);
          this.minText.textContent = this.minX*this.factor-250;
        }

      }
    });


    this.maxButton.addEventListener( "mousedown", e => this.maxButtonDown = true );
    this.maxButton.addEventListener( "mouseup", e => this.maxButtonDown = false );
    this.maxButton.addEventListener( "mouseleave", e => this.maxButtonDown = false );

    this.maxButton.addEventListener( "mousemove", e => {
      //e.preventDefault();

      if (this.maxButtonInitX === null)
        this.maxButtonInitX = this.maxButton.getBoundingClientRect().left+this.BUTTON_R;//

      if (this.maxButtonDown){

        this.maxX  = e.clientX - this.minButtonInitX;
        //console.log('maxButton', e.clientX, this.maxButtonInitX, this.maxX);
        if (this.maxX < this.MAX_VALUE  && this.minX+this.BUTTON_R < this.maxX){
          this.maxButton.setAttribute('cx', this.MIN_BUTTON_INIT_X + this.maxX);
          this.maxText.textContent = this.maxX*this.factor;
        }

      }

    });


  }


  setRange(min, max){
    this.factor = (max-min)/80;
  }

}

*/
