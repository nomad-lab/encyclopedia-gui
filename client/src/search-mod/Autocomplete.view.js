/**
 * Copyright 2019-2019 Georg Huhs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   Basic class for extending a text field with autocomplete functionality
 */

"use strict";

class AutocompleteTextField {

  constructor(name = "", allowEmptyInput = false) {
    this.classPostfix = name;
    this.allowEmptyInput = allowEmptyInput;
    this.element = document.createElement('input');
    this.element.type = 'text';
    this.element.className = 'autocomplete-textfield-' + this.classPostfix;
    this.currentFocus = -1;
    this.selectListener = undefined;
  }

  replaceElement(oldElement) {
    oldElement.parentElement.replaceChild(this.element, oldElement);
  }

  setSelectListener(listener) {
    this.selectListener = listener;
  }

  autocomplete(allAcValues) {
    /* the autocomplete function takes an array of possible autocomplete values.
       in the following we will use 'ac' as abbrevation for 'autocomplete'
    */

    /* process input when someone writes in the text field:*/

    this.element.addEventListener("input", (e) => {
      this._processInput(allAcValues);
    });

    /* react to keyboard navigation */
    this.element.addEventListener("keydown", (e) => {
      if (e.keyCode == 40) { // arrow DOWN
        this._setActive(this.currentFocus + 1);
      } else if (e.keyCode == 38) { // arrow UP
        this._setActive(this.currentFocus - 1);
      }
    });

    /* react to enter key */
    this.element.addEventListener("keypress", (e) => {
      if (e.keyCode == 13) { // ENTER
        /* simulate a click on the "active" item:*/
        this._clickActive();
      }
    });

    /* react to klicking into the textfield */
    this.element.addEventListener("click", (e) => {
      this._processInput(allAcValues);
      e.stopPropagation();
    });

    // close lists when someone clicks in the document:*/
    document.addEventListener("click", (e) => {
      this._closeAllLists();
    });
  }

  _processInput(allAcValues) {
    let currentInput = this.element.value;
   /*close any already open lists of autocompleted values*/
    this._closeAllLists();

    // in case of an empty input field
    if (!this.allowEmptyInput && !currentInput) {
      return false;
    }

    /*create a DIV element that will contain the items (values):*/
    let listContainer = document.createElement("DIV");
    listContainer.setAttribute("id", "autocomplete-list");
    listContainer.classList.add("autocomplete-items");
    listContainer.classList.add("autocomplete-items-"+ this.classPostfix);
    /*append the DIV element as a child of the autocomplete container:*/
    this.element.parentNode.appendChild(listContainer);

    /*for each item in the array...*/
    let acItemIndex = 0;
    for (var i = 0; i < allAcValues.length; i++) {
      /*check if the item contains the same letters as the text field value:*/
      let acValue = allAcValues[i];
      let pos = 0;
      if (currentInput) {
        pos = acValue.toUpperCase().search(currentInput.toUpperCase());
      }
      if (pos >= 0){
        let listItem = this._generateListItem(acValue,
                                              currentInput,
                                              acItemIndex)
        listContainer.appendChild(listItem);

        /* check if a valid option was completely entered.
           if so, set the focus to the element corresponding to the input */
        if (acValue.toUpperCase() === currentInput.toUpperCase()) {
          this._setActive(acItemIndex);
        }
        acItemIndex++;
      }
    }

  }

  _generateListItem(acText, inputText, itemIndex) {
    /*create a DIV element for each matching element:*/
    let listItem = document.createElement("DIV");
    /*make the matching letters bold:*/
    if (inputText){
      let pos = acText.toUpperCase().search(inputText.toUpperCase());
      listItem.innerHTML = acText.substr(0, pos);
      listItem.innerHTML += "<strong>";
      listItem.innerHTML += acText.substr(pos, inputText.length);
      listItem.innerHTML += "</strong>";
      listItem.innerHTML += acText.substr(pos + inputText.length);
    } else {
      listItem.innerHTML = acText;
    }
    /* clicking on the as list item puts selects the corresponding name for searching */
    listItem.addEventListener("click", (e) => {
      /*insert the value for the autocomplete text field:*/
      this._setText(acText);
    });

    /* hovering puts the focus on the related list item */
    listItem.addEventListener("mouseover", (e) => {
      this._setActive(itemIndex);
    });
    return listItem;
  }

  _setText(value) {
    /*insert the value for the autocomplete text field:*/
    this.element.value = value;
    /* notify listener */
    if (this.selectListener) {
      this.selectListener();
    }

    /*close the list of autocompleted values,
    (or any other open lists of autocompleted values)*/
    this._closeAllLists();
  }

  _setActive(index) {
    let listItems = document.getElementById("autocomplete-list")
                              .getElementsByTagName("div");
    /* remove the active status from all list items */
    Array.from(listItems).forEach(item => {
      item.classList.remove("autocomplete-active");
    });

    /* ensure to stay in the list
       out of boundary indices are mapped to the closest border */
    let newFocus = Math.max(0, index);
    newFocus = Math.min(newFocus, listItems.length-1);
    this.currentFocus = newFocus;

    /* mark the active status by a style class */
    listItems[newFocus].classList.add("autocomplete-active");
  }

  _clickActive() {
    if (this.currentFocus > -1) {
      let listItems = document.getElementById("autocomplete-list")
                                .getElementsByTagName("div");
      listItems[this.currentFocus].click();
    }
  }

  _closeAllLists() {
    /*close all autocomplete lists in the document */
    let allAcLists = document.getElementsByClassName("autocomplete-items");
    for (let acList of allAcLists) {
      acList.parentNode.removeChild(acList);
    }
    this.currentFocus = -1;
  }
}















class AutocompleteMultiselectList {

  constructor(name = "") {
    this.classPostfix = name;
    this.element = document.createElement('input');
    this.element.type = 'text';
    this.element.className = 'autocomplete-multiselectlist-' + this.classPostfix;
    this.currentFocus = -1;
    this.selectListener = undefined;

    /* the items member variable keeps track of the selected options
       by storing a (sorted) list of all possible values and their selection state.
       A list entry looks like {"value": "A1", "selected": false}
    */
    this.items = [];
  }

  replaceElement(oldElement) {
    oldElement.parentElement.replaceChild(this.element, oldElement);
  }

  setSelectListener(listener) {
    this.selectListener = listener;
  }

  autocomplete(allAcValues) {
    /* the autocomplete function takes an array of possible autocomplete values.
       in the following we will use 'ac' as abbrevation for 'autocomplete'
    */

    /* store possible values and initialize them as not selected */
    for (var i = 0; i < allAcValues.length; i++) {
      this.items.push({value: allAcValues[i],
                       selected: false}
                     );
    }

    /* process input when someone writes in the text field:*/
    this.element.addEventListener("input", (e) => {
      this._processInput(allAcValues);
    });

    /* react to keyboard navigation */
    this.element.addEventListener("keydown", (e) => {
      if (e.keyCode == 40) { // arrow DOWN
        this._setActive(this.currentFocus + 1);
      } else if (e.keyCode == 38) { // arrow UP
        this._setActive(this.currentFocus - 1);
      } else if (e.keyCode == 27) { // ESC key
        this.element.value = '';
        this._closeAllLists();
      }
    });

    /* react to enter key */
    this.element.addEventListener("keypress", (e) => {
      if (e.keyCode == 13) { // ENTER
        /* simulate a click on the "active" item:*/
        this._clickActive();
      }
    });

    /* react to klicking into the textfield */
    this.element.addEventListener("click", (e) => {
      this._processInput();
      e.stopPropagation();
    });

    // close lists when someone clicks in the document:*/
    document.addEventListener("click", (e) => {
      this.element.value = '';
      this._closeAllLists();
    });
  }

  getSelected() {
    let values = [];
    for (let item of this.items) {
      if (item.selected) {
        values.push(item.value);
      }
    }
    return values;
  }

  _processInput() {
    let currentInput = this.element.value;
   /*close any already open lists of autocompleted values*/
    this._closeAllLists();

    /*create a DIV element that will contain the items (values):*/
    let listContainer = document.createElement("DIV");
    listContainer.setAttribute("id", "autocomplete-list");
    listContainer.classList.add("autocomplete-items");
    listContainer.classList.add("autocomplete-items-"+ this.classPostfix);
    /*append the DIV element as a child of the autocomplete container:*/
    this.element.parentNode.appendChild(listContainer);

    /* keyboard interaction */
    listContainer.setAttribute("tabindex", "0");
    listContainer.addEventListener("keydown", (e) => {
      if (e.keyCode == 40) { // arrow DOWN
        this._setActive(this.currentFocus + 1);
        e.preventDefault();
      } else if (e.keyCode == 38) { // arrow UP
        this._setActive(this.currentFocus - 1);
        e.preventDefault();
      } else if (e.keyCode == 27) { // ESC key
        this.element.value = '';
        this._closeAllLists();
      }
    });
    listContainer.addEventListener("keypress", (e) => {
      if (e.keyCode == 13) { // ENTER
        /* simulate a click on the "active" item:*/
        this._clickActive();
      }
    });

    /* show all items matching the input text */
    let acItemIndex = 0;
    for (let item of this.items) {
      /*check if the item contains the same letters as the text field value:*/
      let acValue = item.value;
      let acSelected = item.selected;
      let pos = 0;
      if (currentInput) {
        pos = acValue.toUpperCase().search(currentInput.toUpperCase());
      }
      /* if there is no input text given, pos = 0 and thus an item is generated */
      if (pos >= 0){
        let listItem = this._generateListItem(acValue,
                                              acSelected,
                                              currentInput,
                                              acItemIndex)
        listContainer.appendChild(listItem);

        /* check if a valid option was completely entered.
           if so, set the focus to the element corresponding to the input */
        if (acValue.toUpperCase() === currentInput.toUpperCase()) {
          this._setActive(acItemIndex);
        }
        acItemIndex++;
      }
    }

  }

  _generateListItem(acText, selected, inputText, itemIndex) {
    /*create a DIV element for each matching element:*/
    let listItem = document.createElement("div");
    let itemCheckbox = document.createElement("input");
    itemCheckbox.type = "checkbox";
    itemCheckbox.checked = selected;
    /* TODO: check why catching this event is necessary */
    itemCheckbox.addEventListener("click", e => {
      listItem.click();
      e.stopPropagation();
    });
    listItem.appendChild(itemCheckbox);
    /*make the matching letters bold:*/
    if (inputText && inputText != ""){
      let pos = acText.toUpperCase().search(inputText.toUpperCase());
      listItem.appendChild(document.createTextNode(acText.substr(0, pos)));
      let emText = document.createElement("span");
      emText.className = "autocomplete-em";
      emText.innerHTML = acText.substr(pos, inputText.length);
      /* TODO: check why catching this event is necessary */
      emText.addEventListener("click", e => {
        listItem.click();
        e.stopPropagation();
      });
      listItem.appendChild(emText);
      listItem.appendChild(document.createTextNode(acText.substr(pos + inputText.length)));
    } else {
      listItem.appendChild(document.createTextNode(acText));
    }

    /* clicking on the AS list item puts selects the corresponding name for searching */
    listItem.addEventListener("click", (e) => {
      let checkbox = e.target.getElementsByTagName("input")[0];
      this._toggleSelect(acText, checkbox);
      e.stopPropagation();
    });

    /* hovering puts the focus on the related list item */
    listItem.addEventListener("mouseover", (e) => {
      this._setActive(itemIndex);
    });
    return listItem;
  }

  // _setText(value) {
  //   /*insert the value for the autocomplete text field:*/
  //   this.element.value = value;
  //   /* notify listener */
  //   if (this.selectListener) {
  //     this.selectListener();
  //   }
  //
  //   /*close the list of autocompleted values,
  //   (or any other open lists of autocompleted values)*/
  //   this._closeAllLists();
  // }

  _toggleSelect(value, checkbox) {
    let newSelected;
    for (let item of this.items) {
      if (item.value == value) {
        newSelected = !item.selected;
        item.selected = newSelected;
        break;
      }
    }
    checkbox.checked = newSelected;
  }

  _setActive(index) {
    let listItems = document.getElementById("autocomplete-list")
                              .getElementsByTagName("div");
    /* remove the active status from all list items */
    Array.from(listItems).forEach(item => {
      item.classList.remove("autocomplete-active");
    });

    /* ensure to stay in the list
       out of boundary indices are mapped to the closest border */
    let newFocus = Math.max(0, index);
    newFocus = Math.min(newFocus, listItems.length-1);
    this.currentFocus = newFocus;

    /* mark the active status by a style class */
    listItems[newFocus].classList.add("autocomplete-active");
  }

  _clickActive() {
    if (this.currentFocus > -1) {
      let listItems = document.getElementById("autocomplete-list")
                                .getElementsByTagName("div");
      listItems[this.currentFocus].click();
    }
  }

  _closeAllLists() {
    /*close all autocomplete lists in the document */
    let allAcLists = document.getElementsByClassName("autocomplete-items");
    for (let acList of allAcLists) {
      acList.parentNode.removeChild(acList);
    }
    this.currentFocus = -1;
  }
}


// EXPORTS
module.exports = { AutocompleteTextField: AutocompleteTextField,
                   AutocompleteMultiselectList: AutocompleteMultiselectList
                 };
