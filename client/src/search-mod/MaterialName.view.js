/**
 * Copyright 2016-2019 Iker Hurtado, Georg Huhs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This is the UI component implementing the search for material names.
   The input element features a text-autocomplete functionality,
   based on the data from the related "suggestions" API endpoint
 */

"use strict";

let util = require('../common/util.js');
let AutocompleteTextField = require('./Autocomplete.view.js').AutocompleteTextField;

class MaterialNameBox{

  constructor() {
    this.element = document.createElement('div');
    this.element.setAttribute("id",'material-name-box');
    this.materialNameField = new AutocompleteTextField('materialname');
    this.materialNameField.element.placeholder = 'Start typing a material name';
    this.materialNameField.element.classList.add('textfield-composition');
    //this.materialNameField.element.classList.add("materialname-text-field");

    this.element.innerHTML  =  `  <div style="padding-bottom: 20px;">
          <div class="materialname-placeholder"></div>
          <button class="adding-name-btn" disabled>Add to query</button>
          </div>
        `;

    this.button = this.element.querySelector('.adding-name-btn');

    this.button.addEventListener( "click", (e) => {
      this.listener(this.materialNameField.element.value);
      this.materialNameField.element.value = '';
    });

    /* enable button when a material was selected from the autocomplete list */
    this.materialNameField.setSelectListener( () => {
      this.button.disabled = false;
      this.button.focus();
    });

    let materialNamePlaceholder = this.element.querySelector('.materialname-placeholder');
    this.materialNameField.replaceElement(materialNamePlaceholder);

    this.materialNamesLoaded = false;
  }

  setAddMaterialNameListener(listener) {
    this.listener = listener;
  }

  setAutocomplete() {
    // load material names only once
    if (!this.materialNamesLoaded){
      let r1 = util.serverReq(util.getSuggestionURL('material_name'), (e) => {
        let names = JSON.parse(r1.response).material_name;
        this.materialNameField.autocomplete(names);
      });
      this.materialNamesLoaded = true;
    }
  }

  disableInput() {
    this.materialNameField.element.disabled = true;
    this.button.disabled = true;
  }

  enableInput() {
    this.materialNameField.element.disabled = false;
    this.button.disabled = true;
  }
}

// EXPORTS
module.exports = MaterialNameBox;
