
/**
 * Copyright 2016-2019 Iker Hurtado, Georg Huhs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This file implements the Search Module of the application.
   It's a container UI component that shows the seach part of the application:
   front search interface and search results page.
 */

"use strict";

let util = require('../common/util.js');
let UserGuidance = require('../common/UserGuidance.js');
let ElementTable = require('./ElemenTable.view.js');
let MaterialList = require('./MaterialList.view.js');
let MaterialNameBox = require('./MaterialName.view.js');
let FilterPanel = require('./FilterPanel.view.js');
let SwitchComponent = require('../common/SwitchComponent.js');



// local utility functions
function getTagHtml(tag, isFormula){
  return `<span class="search-label" data-el="${tag}" >
            <img src="img/tag.svg" height="16px" class="remove-label"
              style="vertical-align: bottom"/>
          ${isFormula ? util.getSubscriptedFormula(tag) : tag}
          <img src="img/cross.svg" height="6px" class="remove-label"
            style="vertical-align: middle; padding: 4px 3px 6px 5px;" />
          </span>`;
}


function replaceDashes(s){
  return s.split('-').join('_');
}



class NewSearchMod {

  constructor() {

    this.userGuidance = true; // can enabled/disabled

    //this.showingSearchBox = false;
    this.searchQuery = [];
    this.queryTypes = []; //**** Types associated to query elements
    // Types: element (E), formula (F), symbol (S) and prop names
    this.searchFilters = [];

    this.currentOperator = 'AND';

    this.element = document.createElement('div');
    this.element.setAttribute("id",'search-module');
    this.element.innerHTML=
    `
      <div class="search-filter-side">

        <div class="search-title">Filters</div>

        <!-- <div id="filter-panel-placeholder">  </div> -->

      </div>

      <div class="search-main-side">

        <div class="search-box" >
          <div class="search-query-wrapper" >
            <div class="search-query-box" style="float: left;">
            </div>
            <button class="clean-btn" style="float: right;">Clear all</button>
          </div>

          <button class="search-btn" >Search</button>

        </div>

        <input type="checkbox" id="multiples-of-formula" value="">
          Include multiples of formula
          <br>
        <input type="checkbox" id="allow-other-elements" value="">
          Allow other elements
        <br>



        <div class="add-buttons" >
          <div  class="tab-buttons" style="width: 70%; display: inline-block">
            <button class="element-add-btn" id="add-tab-selected" style="margin-left: 50px">Element</button>
            <button class="formula-add-btn" style="padding: 10px 20px;" >Formula</button>
            <button class="name-add-btn" >Name</button>
          </div>
          <div class="bool-buttons" style="width: 28%; display: inline-block" >
            OR <span id="and-or-switch" ></span> AND
            <button >NOT</button>
            <button class="open-parentheses" >(</button>
            <button class="close-parentheses">)</button>
            <!--<input type="checkbox" name="and-or" class="not-symbol-btn" />NOT-->
          </div>
        </div>


        <div class="add-box">
          <div style="width: 70%; display: inline-block;">
            <div class="triangle-container" style="margin-left: 50px;">
              <div class="triangle element-tri" style="visibility: visible"></div>
            </div>
            <div class="triangle-container" >
              <div class="triangle formula-tri" style="visibility: hidden"></div>
            </div>
            <div class="triangle-container" >
              <div class="triangle name-tri" style="visibility: hidden"></div>
            </div>
          </div>

          <div class="add-panel">
          </div>
        </div>

        <div class="results-panel">  <!-- style="display: none"-->
        </div>

      </div> <!-- search-main-side -->
    `;

    this.filterSidePanel = this.element.querySelector('.search-filter-side');
    this.searchQueryBox= this.element.getElementsByClassName("search-query-box")[0];
    this.searchBox = this.element.querySelector('.search-box');

    this.mainButton= this.element.querySelector('.search-btn');
    this.cleanButton= this.element.querySelector('.clean-btn');
    this.addButtonsBox= this.element.querySelector('.add-buttons');
    this.addElementButton = this.addButtonsBox.querySelector('.element-add-btn');
    this.addFormulaButton = this.addButtonsBox.querySelector('.formula-add-btn');
    this.addMatNameButton = this.addButtonsBox.querySelector('.name-add-btn');

    this.addPanel= this.element.querySelector('.add-panel');

    let andOrSwitch = new SwitchComponent(util.IMAGE_DIR+'switch');
    this.element.querySelector('#and-or-switch').appendChild(andOrSwitch.element);
    andOrSwitch.setListener( e => {
      this.currentOperator = ( e ? 'AND' : 'OR');
    });

    this.openParenthButton = this.element.querySelector('.open-parentheses');
    this.closeParenthButton = this.element.querySelector('.close-parentheses');
    this.openParenthButton.addEventListener( 'click', e => {
      this._addParenthesesInSearchQuery(true);
    });
    this.closeParenthButton.addEventListener( 'click', e => {
      this._addParenthesesInSearchQuery(false);
    });

    this.elementTable= new ElementTable();
    this.elementTable.setClickListener(elementArray => {
      this.addElementsInSearchQuery(elementArray);
      this.addMatNameButton.disabled = true; // Not always necessary but it simplifies the code
    });
    this.elementTable.setDeselectListener(e => this.removeElementORFormulaInSearchQuery(e));


    this.formulaBox = new FormulaBox();
    this.formulaBox.setAddFormulaListener(formula => {
      if (formula.trim() !== ''){
        this.addTagInSearchQuery(formula, 'F');
        //this.formulaBox.disable(true);
        this.addMatNameButton.disabled = true;
        this.materialNameBox.disableInput();
      }
    });


    this.materialNameBox = new MaterialNameBox();
    this.materialNameBox.setAddMaterialNameListener( name => {
      if (name.trim() !== ''){
        this.addTagInSearchQuery(name, 'MN');
        this.addElementButton.disabled = true;
        this.addFormulaButton.disabled = true;
        //TODO: are the following two lines needed, when the related buttons are already diasbled??
        this.formulaBox.disableInput();
        this.materialNameBox.disableInput();
      }
    });
    // add autocomplete functionality to the textfield
//    this.materialNameBox.setAutocomplete();

    this.filterPanel = new FilterPanel();
    this.filterSidePanel.appendChild(this.filterPanel.element);

    this.materialList= new MaterialList();
    this.resultsPage = this.element.querySelector('.results-panel');
    this.materialList.attachAndSetEvents(this.resultsPage);

    this.currentTab = 'element';
    this.addPanel.appendChild(this.elementTable.element);



    this._events();
  }


  _events() {
    // External event - Search button press
    this.mainButton.addEventListener( "click", (e) => {
      //console.log("this.labels: "+JSON.stringify(this.labels));

      if (this.searchQuery.lenght === 0){
        util.showUserMsg('No query');
      }else{
        let searchExpressionQuery;

        // Search by  Material name
        if (this.queryTypes[0] === 'MN'){
          //queryObj.push(this._getESSimpleMatch('material_name', item));
          let rootQueryObj = { 'bool' : {} };
          rootQueryObj.bool.must = [];
          rootQueryObj.bool.must.push( this._getESSimpleMatch('material_name', this.searchQuery[0]) );
          this.materialList.initSearch( rootQueryObj );

        }else{ // Search by complex search expression

          if (this.element.querySelector('#allow-other-elements').checked)
            searchExpressionQuery = this._getESQueryFromSearchQuery_otherElements(
              this.searchQuery, this.queryTypes);
          // Regular case: search containing only the elements in the search expression
          else searchExpressionQuery =
              this._getESQueryFromSearchQuery(this.searchQuery, this.queryTypes);

          this.materialList.initSearch(
            this._addFiltersInSearchQuery( this.filterPanel.getValues(),
              searchExpressionQuery));
          //util.setBrowserHashPath('search','results');

          //TODO: why not identifying by ID???
          this.element.querySelector('.add-box').style.display = 'none';
        }

      }

    });


    this.cleanButton.addEventListener( "click", (e) => {
      this.searchQuery = [];
      this.queryTypes = [];
      this.updateSearchQuery();
      this.addFormulaButton.disabled = false;
      this.addMatNameButton.disabled = false;
      this.addElementButton.disabled = false;
      this.formulaBox.enableInput();
      this.materialNameBox.enableInput();
      this.elementTable.deselectAllElements();
    });

    this.addButtonsBox.addEventListener( "click", (e) => {
      if (e.target !== e.currentTarget) { // When the event source is a child
        let className = e.target.className;
        let index = className.indexOf('add-btn');

        if (index > 0){
          let selectingElement;
          let selectingTab = className.substring(0, index-1);
          if (selectingTab === 'element')
            selectingElement = this.elementTable.element;
          else if (selectingTab === 'name') {
            selectingElement = this.materialNameBox.element;
            // add autocomplete functionality,
            // but load data not before the name tab is activated
            this.materialNameBox.setAutocomplete();
          }
          else if (selectingTab === 'formula')
            selectingElement = this.formulaBox.element;

          this.addPanel.replaceChild(selectingElement, this.addPanel.lastChild);

          this.element.querySelector('.add-box').style.display = 'block';

          // Change the styles of the buttons
          let selEl = this.element.querySelector('.'+this.currentTab+'-add-btn');
          this._setTabSelectedStyles(selEl, false);
          this._setTabSelectedStyles(e.target, true);

          // Change the triangle
          this.element.querySelector('.'+this.currentTab+'-tri').style.visibility = 'hidden';
          this.element.querySelector('.'+selectingTab+'-tri').style.visibility = 'visible';

          this.currentTab = selectingTab;

/*
          if (this.userGuidance){
            if (selectingTab === 'element'){
              UserGuidance.showIndependentTip(7, false);
              UserGuidance.showIndependentTip(3, true);
            }
            else if (selectingTab === 'props'){
              UserGuidance.showIndependentTip(3, false);
              UserGuidance.showIndependentTip(7, true);
            }else if (selectingTab === 'formula'){
              UserGuidance.showIndependentTip(3, false);
              UserGuidance.showIndependentTip(7, false);
            }
          }
          */

        }
      }
    });

    this.searchQueryBox.addEventListener( "click", (e) => {
      let className = e.target.className;
      if (className === 'remove-label'){
        let elSymbol = e.target.parentElement.getAttribute('data-el');
        this.removeElementORFormulaInSearchQuery(elSymbol);
      }
    });

  }


  _getESQueryFromSearchQuery(searchQuery, queryTypes){
    let formulas = [];
    let parFormulas = [];
    let parOperator = null;
    let openIndex = -1;
    searchQuery.forEach( (item, i) => {
      if (queryTypes[i] === 'F' || queryTypes[i] === 'E'){
        addItem( (openIndex >= 0 ? parFormulas : formulas), i,
          item+(queryTypes[i] === 'E' ? '0' : ''));

      }else if ( searchQuery[i] === '(' ){
        if (i-1 >= 0 ) parOperator = searchQuery[i-1];
        openIndex = i;
      }
      else if ( searchQuery[i] === ')' ){
        if (parOperator === null){ // The starting ( was the first symbol of the expression
          formulas = parFormulas;
        }else if (parOperator === 'OR'){ // OR (...
          formulas = formulas.concat(parFormulas);
        }else{ // AND (...
          let rFormulas = [];
          formulas.forEach( formula => {
            parFormulas.forEach( parFormula => rFormulas.push(formula+parFormula) );
          });
          formulas = rFormulas;
        }
        parFormulas = []; // reset the array formulas inside the parentheses

        openIndex = -1;
      }
    });
    console.log('_getESQueryFromSearchQuery: ',formulas, parFormulas);

    //*********** Get the elastic search expression from the search expression
    // the elements inserted must be sorted for this to work
    // queryObj.bool.must.push(this._getESSimpleMatch('atom_labels_keyword', this._sortElements(elements) )); ///elements.join('')));
    // if ( this.element.querySelector('#multiples-of-formula').checked ){ // reduced search
    let rootQueryObj = { 'bool' : {} };
    rootQueryObj.bool.should = [];
    let queryObj = rootQueryObj.bool.should;
    let reduced = this.element.querySelector('#multiples-of-formula').checked;

    formulas.forEach( formula => {
      let pFormula;
      let searchByElement = (formula.indexOf('0') >= 0);
      if (searchByElement){ // There are some element in the search expression
        /** TDO**/
        let f = formula;
        if (reduced) f = this._reduceFormula(formula, false);
        pFormula = this._processFormula(f, 'element-string');
        let tempQueryObj = { 'bool' : { 'must' : [] } };

        if (pFormula[0].length > 0)  // length === 0 No formula, only elements
          tempQueryObj.bool.must.push(
            this._getESOperatorMatch(
              (reduced ? 'formula_reduced_terms' : 'formula_cell_terms'), pFormula[0]));
        tempQueryObj.bool.must.push(
          this._getESSimpleMatch('atom_labels_keyword', pFormula[1]));

        queryObj.push(tempQueryObj);

      }else{ // Only formulas
        if ( reduced ){ // reduced search
          pFormula = this._reduceFormula(formula, false);
          queryObj.push(this._getESSimpleMatch('formula_reduced_keyword', pFormula));
        }else{
          pFormula = this._processFormula(formula, 'canonical-formula');
          queryObj.push(this._getESSimpleMatch('formula_cell_keyword', pFormula));
        }
      }

    });

    return rootQueryObj;

    function addItem(formulas, i, item){
      if (i === 0 || searchQuery[i-1] === '('){
        formulas.push(item);
      }else{
        if (searchQuery[i-1] === 'OR') formulas.push(item);
        else if (searchQuery[i-1] === 'AND')
          formulas.push(formulas.pop()+item);
      }
    }

  } // _getESQueryFromSearchQuery()


  _getESQueryFromSearchQuery_otherElements(searchQuery, queryTypes){
    // Query structure analysis - looking for parentheses (only one level supported)
    if ( searchQuery.indexOf('(') >= 0){ // Recursion

      let openIndex = -1;
      let prodQuery = [];
      let prodTypes = [];
      //let prodQueryIndex = 0;
      for (let i = 0; i < searchQuery.length; i++) {
        if ( searchQuery[i] === '(' ) openIndex = i;
        else if ( searchQuery[i] === ')' ){
          prodQuery.push(this._getESQueryFromSearchQuery_otherElements(
            searchQuery.slice(openIndex+1, i), queryTypes.slice(openIndex+1, i)));
          prodTypes.push('Q');
          openIndex = -1;
        }else if (openIndex < 0){ // outside a parentheses
          prodQuery.push(searchQuery[i]);
          prodTypes.push(queryTypes[i]);
        }
      }
      //console.log('prodQuery', prodQuery, prodTypes);
      return this._getESQueryFromSearchQuery_otherElements(prodQuery, prodTypes);

    }else{  // BASE CASE: there is no parentheses

      let boolOperator;
      searchQuery.forEach( (item, i) => {
        if (searchQuery[i] === 'AND' || searchQuery[i] === 'OR')
          boolOperator = item;
      });

      let rootQueryObj = { 'bool' : {} };
      let queryObj;
      if (boolOperator === 'AND'){
        rootQueryObj.bool.must = [];
        queryObj = rootQueryObj.bool.must;
      }else{ // OR
        rootQueryObj.bool.should = [];
        queryObj = rootQueryObj.bool.should;
      }

      searchQuery.forEach( (item, i) => {

        if (queryTypes[i] === 'F'){ // Formula case
          let esMatchQuery;
          if ( this.element.querySelector('#multiples-of-formula').checked ){ // reduced search
            esMatchQuery = this._getESOperatorMatch('formula_reduced_terms', this._reduceFormula(item));
          }else
            esMatchQuery = this._getESOperatorMatch('formula_cell_terms', this._processFormula(item, 'tokens'));
          queryObj.push(esMatchQuery);

        }else if (queryTypes[i] === 'E'){ // Element case
          queryObj.push(this._getESSimpleMatch('atom_labels_terms', item));

        }else if (queryTypes[i] === 'Q'){
          queryObj.push(item);
        }
      });

      return rootQueryObj;
    } // else
  } // _getESQueryFromSearchQuery_otherElements()




  _addFiltersInSearchQuery(filterMap, searchExpressionQuery){
    let rootQueryObj = { 'bool' : {} };
    rootQueryObj.bool.must = [];
    rootQueryObj.bool.must.push( searchExpressionQuery );


    filterMap.forEach((values/*Array*/, filterName) => {

      let filterNameDef = replaceDashes(filterName);

      if (filterName === 'mass-density' || filterName === 'band-gap'){
        //***** util.eV2J() apply?
        rootQueryObj.bool.must.push( this._getFieldESRange(filterNameDef, values) );

      }else if (filterName === 'band-gap-type'){ // special case
        if ( values !== 'both')
          rootQueryObj.bool.must.push( this._getESSimpleMatch('band_gap_direct',
            ( values === 'direct' ? true : false ) ) );

      }else if (filterName.startsWith('has')){ // has- filters
        rootQueryObj.bool.must.push( this._getESSimpleMatch(filterNameDef, values, false) );

      }else{ // normal case
        //rootQueryObj.bool.must.push( this._getESOperatorMatch(filterNameDef, values, false) );
        rootQueryObj.bool.must.push( this._getESTermsArray(filterNameDef, values) );

        //console.log(this._getESOperatorMatch(filterNameDef, values, false) );
      }

    });


    return rootQueryObj;
  }


  _getESSimpleMatch(field, value){
    return {
      "match": { [field] : value }
    };
  }

  _getESTermsArray(field, value){
    return {
      "terms": { [field] : value }
    };
  }


  _getESOperatorMatch(field, elements, and = true){
    let elementsString = '';
    if (elements.length > 0)  elementsString = elements.join(' ');

    return {
      "match": {
        [field]: {
          "operator": (and ? "and" : 'or'),
          "query": elementsString
        }
      }
    };
  }


  _getFieldESRange(field, valuesString){
    let data = valuesString.split(':');
    console.log('_getFieldESRange data', data);

    return {
      "range": {
          [field]: { "gte" : parseInt(data[0]), "lte" : parseInt(data[1]) }
      }
    };
  }




  _setTabSelectedStyles(element, value){
    /*
    element.style.fontWeight = (value ? 'bold' : 'normal');
    element.style.color = (value ? '#E56400' : '#777');
    element.style.borderColor = (value ? '#E56400' : '#777');
    */
    element.id = (value ? 'add-tab-selected' : '');
  }


/*
  _showSearchBox(){
    if (!this.showingSearchBox){
      this.showingSearchBox = true;
      this.searchLine.style.visibility = 'visible';

      if (this.userGuidance)  UserGuidance.setFinal();
    }
  }*/

  _addItemInSearchQuery(item, type){
      this.searchQuery.push(item);
      this.queryTypes.push(type);
  }


  addTagInSearchQuery(tag, type){
    // If the it's an element and is already in the query it's not inserted
    if (type === 'E' && this.searchQuery.indexOf(tag) >= 0) return;

    if ( this.searchQuery.length > 0
          && this.searchQuery[this.searchQuery.length-1] !== '(')
      this._addItemInSearchQuery(this.currentOperator, 'S');
    this._addItemInSearchQuery(tag, type);
    this.updateSearchQuery();
    //this._showSearchBox();
  }


  addElementsInSearchQuery(elementArray){
    let index = elementArray.length;
    while (index--) {
      this.addTagInSearchQuery(elementArray[index], 'E');
    }
    return true;
  }


  _addParenthesesInSearchQuery(isOpen){

    if ( this.searchQuery.length > 0 && isOpen)
      this._addItemInSearchQuery(this.currentOperator, 'S');
    this._addItemInSearchQuery( (isOpen ? '(' : ')'), 'P');
    this.updateSearchQuery();
    //this._showSearchBox();
  }


  removeElementORFormulaInSearchQuery(item){
    //console.log(" removeElementORFormulaInSearchQuery item: ",item, this.searchQuery.indexOf(item));

    let isMaterialName = (this.queryTypes[0] === 'MN');
    // Travese the array removing the item and the bool operator related
    let itemIndex = this.searchQuery.indexOf(item);
    if (itemIndex >= 0){
      let i, elementsToRemove;
      if (this.queryTypes[itemIndex+1] === 'S'){ // bool operator on the left
        i = itemIndex; elementsToRemove = 2;
      }else if (this.queryTypes[itemIndex-1] === 'S'){ // bool operator on the right
        i = itemIndex-1; elementsToRemove = 2;
      }else{ // case: (item)
        i = itemIndex; elementsToRemove = 1;
      }
      this.searchQuery.splice(i, elementsToRemove);
      this.queryTypes.splice(i, elementsToRemove);
    }

    // Travese the array removing the unnecessary parethesis (only tested for one level nested)
    if ( this.searchQuery.indexOf('(') >= 0){ // Recursion

      for (let i = 0; i < this.searchQuery.length; i++) { // dangerous: modifing a array being traversed
        if ( this.searchQuery[i] === '(' ){
          if ( this.searchQuery[i+1] === ')'){ // '()' case
            this.searchQuery.splice(i, 2);
            this.queryTypes.splice(i, 2);
          }else if (searchQuery[i+2] === ')'){ // '(item)' case
            this.searchQuery.splice(i, 3, this.searchQuery[i+1]);
            this.queryTypes.splice(i, 3, this.queryTypes[i+1]);
          }
        }
      }
    }

    this.updateSearchQuery();

    if (util.ELEMENTS.indexOf(item) >= 0){ // It's an element (being removed)
      this.elementTable.deselectElement(item);
    }

    if (this.queryTypes.length === 0){

      if (isMaterialName){
        this.addElementButton.disabled = false;
        this.addFormulaButton.disabled = false;
        this.formulaBox.enableInput();
      }else{ // element or formula
        this.addMatNameButton.disabled = false;

      }
      this.materialNameBox.enableInput();
    }

    //console.log(" final searchQuery: ",this.searchQuery);
    //}

    return true;
  }


  updateSearchQuery(){
    let html= '';
    for (let i = 0; i < this.searchQuery.length; i++) {
      let type = this.queryTypes[i];

      if (type === 'S' || type === 'P')
        html+= `<span class="search-query-symbol" >  ${this.searchQuery[i]} </span>`;
      else
        html+= getTagHtml(this.searchQuery[i], ( type === 'F' ? true : false));
    }
    console.log('this.updateSearchQuery: ', this.searchQuery ,this.queryTypes);
    this.searchQueryBox.innerHTML = html;
  }

 ///********* DELETE?
  showSearchResults(){
    /*
    this.searchPage.style.display= 'none';
    this.resultsPage.style.display= 'block';
    */
    //if (this.userGuidance) UserGuidance.show(false);
  }


  showSearchPage(){
    /*
    this.searchPage.style.display= 'block';
    this.resultsPage.style.display= 'none';

    if (this.userGuidance){
      setTimeout(() => {
        UserGuidance.init(this.addButtonsBox, this.elementTable.element,
          this.searchBox, this.propertiesBox.tabsElement);
        UserGuidance.show(true, this.currentTab === 'element',
          this.currentTab === 'props');
      }, 400);
    }
    */
  }


  _sortElements(elements){
    let numbers = [];
    let sortedElements = [];
    elements.forEach( e => numbers.push(util.ELEMENTS.indexOf(e)) );
    numbers.sort( (a, b) => a - b ); // atomic number-1
    numbers.forEach( n => sortedElements.push(util.ELEMENTS[n]) );
    //console.log('_sortElements ',numbers, elString);
    return sortedElements;
  }




  _reduceFormula(formula, getTokens = true){
    let index = 0;
    let map = new Map();
    let key;
    while ( index < formula.length ){
      let el2 = formula.substring(index, index+2);
      let el1 = formula.substring(index, index+1);

      if (util.ELEMENTS.indexOf(el2) >= 0){
        map.set(el2, 1); // 1 default value
        index += 2;
        key = el2;
        //console.log('eleemnt 2chars', key);
      }else if (util.ELEMENTS.indexOf(el1) >= 0){
        map.set(el1, 1); // 1 default value
        index++;
        key = el1;
        //console.log('eleemnt 1chars', key);
      }else{ // It's a number
        let num = parseInt(el2);
        if (num >= 10) index += 2; // 2 figures number
        else index++;// 1 figure number
        //console.log('number ', num, key);
        map.set(key, num);
      }
      // console.log('FINAL LOOP', map, index);
    }

    let counter = 0;
    while ( !checkIfReduced(map) ){ // console.log('Reducing', map);
      let div = 1;
      if (isDivisibleBy(map, 2)) div = 2;
      else if (isDivisibleBy(map, 3)) div = 3;
      else if (isDivisibleBy(map, 5)) div = 5;
      else if (isDivisibleBy(map, 7)) div = 7;
      else if (isDivisibleBy(map, 11)) div = 11;

      map.forEach( (value, key) => {
        map.set(key, (value/div));
      });
      //console.log('Reducing DIV', map);
      counter++;
      if (counter > 5) break;
    }

    function checkIfReduced(formulaMap){
      let min = 100;
      formulaMap.forEach( (value, key) => {
        if (value < min) min = value;
      });
      return min === 1;
    }

    function isDivisibleBy(formulaMap, n){
      let div = true;
      formulaMap.forEach( (value, key) => {
        if (value % n !== 0) div = false;
      });
      return div;
    }

    let tokens = [];
    let canonicalFormula = '';
    if (getTokens){
      map.forEach( (value, key) => tokens.push(key+value) );
    }else{
      let sortedElements = this._sortElements( Array.from( map.keys() ) );
      sortedElements.forEach( element => {
        canonicalFormula += element+map.get(element);
        //canonicalFormula += element+(map.get(element) === 1 ? '' : map.get(element));
      });
    }


    console.log('_reduceFormula RETURN: ', map, tokens, canonicalFormula);
    return (getTokens ? tokens : canonicalFormula);
  }


  _processFormula(formula, type){
    let index = 0;
    let map = new Map();
    let key;
    while ( index < formula.length ){
      let el2 = formula.substring(index, index+2);
      let el1 = formula.substring(index, index+1);

      if (util.ELEMENTS.indexOf(el2) >= 0){
        map.set(el2, 1); // 1 default value
        index += 2;
        key = el2;
        //console.log('eleemnt 2chars', key);
      }else if (util.ELEMENTS.indexOf(el1) >= 0){
        map.set(el1, 1); // 1 default value
        index++;
        key = el1;
        //console.log('eleemnt 1chars', key);
      }else{ // It's a number
        let num = parseInt(el2);
        if (num >= 10) index += 2; // 2 figures number
        else index++;// 1 figure number
        //console.log('number ', num, key);
        map.set(key, num);
      }
      // console.log('FINAL LOOP', map, index);
    }



    if (type === 'tokens'){
      let tokens = [];
      map.forEach( (value, key) => tokens.push(key+value) );
      console.log('_processFormula RETURN: ', map, tokens);
      return tokens;
    }else{
      let sortedElements = this._sortElements( Array.from( map.keys() ) );
      if (type === 'canonical-formula'){
        let formulaString = '';
        sortedElements.forEach( element => {
          formulaString += element+map.get(element);
        });
        console.log('_processFormula RETURN: ', map, formulaString);
        return formulaString;
      }else{ // elements-string
        let elementsString = '';
        let elementsInFormulas = [];
        sortedElements.forEach( element => {
          elementsString += element;
          let val = map.get(element);
          if (val !== 0) elementsInFormulas.push(element+val);
        });
        console.log('_processFormula RETURN: ', map, [elementsInFormulas ,elementsString]);
        return [elementsInFormulas ,elementsString];
      }
    }

  }

}


class FormulaBox{

  constructor() {
    this.element = document.createElement('div');
    this.element.setAttribute("id",'formula-box');
    this.element.innerHTML=
    `
    <div style="padding-bottom: 20px;">
<!--
    <input type="checkbox" class="allow-other-elements" value="">
      Allow other elements
      <br>


    <input type="checkbox" class="multiples-of-formula" value="">
      Include multiples of formula
      <br> -->

    <input type="text" class="textfield-composition"
      placeholder="Add formula to the search query above" >
      <button class="adding-formula-btn" disabled>Add to query</button>
    </div>
    `;
    this.formulaTextField = this.element.querySelector('.textfield-composition');
    this.formulaButton = this.element.querySelector('.adding-formula-btn');

    this.formulaButton.addEventListener( "click", (e) => {
      this.addFormulaListener(this.formulaTextField.value);
      this.formulaTextField.value = '';
    });

    this.formulaTextField.addEventListener( 'input', e => {
      //console.log('formulaTextField input: ',this.formulaTextField.value);
      this.formulaButton.disabled = (this.formulaTextField.value === '');
    });

  }


  setAddFormulaListener(listener) {
    this.addFormulaListener= listener;
  }


  disableInput() {
    this.formulaTextField.disabled = true;
    this.formulaButton.disabled = true;
  }

  enableInput() {
    this.formulaTextField.disabled = false;
    this.formulaButton.disabled = true;
  }

/*
  getAllowOtherElements(){
    return  this.element.querySelector('.allow-other-elements').checked;
  }


  getMultiplesOfFormula(){
    return  this.element.querySelector('.multiples-of-formula').checked;
  }*/

}

// EXPORTS
module.exports = NewSearchMod;
