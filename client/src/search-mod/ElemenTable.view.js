
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This is the UI component implementing the interactive  Element Table
   in the search front. It need to communicate to other UI components
   so it exposes several listeners
 */

"use strict";

let util = require('../common/util.js');


// constans

const GROUPS = new Map([
  [1,['H', 'Li', 'Na', 'K', 'Rb', 'Cs', 'Fr']],
  [2,['Be', 'Mg', 'Ca', 'Sr', 'Ba', 'Ra']],
  [3,['Sc', 'Y']],
  [4,['Ti', 'Zr', 'Hf', 'Rf']],
  [5,['V', 'Nb', 'Ta', 'Ha']],
  [6,['Cr', 'Mo', 'W', 'Sg']],
  [7,['Mn', 'Tc', 'Re', 'Ns']],
  [8,['Fe', 'Ru', 'Os', 'Hs']],
  [9,['Co', 'Rh', 'Ir', 'Mt']],
  [10,['Ni', 'Pd', 'Pt', 'Ds']],
  [11,['Cu', 'Ag', 'Au', 'Rg']],
  [12,['Zn', 'Cd', 'Hg', 'Cn']],
  [13,['B', 'Al', 'Ga', 'In', 'Tl', 'Nh']],
  [14,['C', 'Si', 'Ge', 'Sn', 'Pb', 'Fl']],
  [15,['N', 'P', 'As', 'Sb', 'Bi', 'Mc']],
  [16,['O', 'S', 'Se', 'Te', 'Po', 'Lv']],
  [17,['F', 'Cl', 'Br', 'I', 'At', 'Ts']],
  [18,['He', 'Ne', 'Ar', 'Kr', 'Xe','Rn', 'Og']],
  [19,['La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu']],
  [20,['Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr']]
]);

const BLOCKS = new Map([
  ['metalloids',['B', 'Si', 'Ge', 'As', 'Sb', 'Te', 'Po']],
  ['other-non-metals',['H', 'C', 'N', 'O', 'P', 'S', 'Se']],
  ['halogens',['F', 'Cl', 'Br', 'I', 'At', 'Ts']],
  ['noble-gases',['He', 'Ne', 'Ar', 'Kr', 'Xe','Rn', 'Og']],
  ['alkali-metals',['Li', 'Na', 'K', 'Rb', 'Cs', 'Fr']],
  ['alkaline-earth-metals',['Be', 'Mg', 'Ca', 'Sr', 'Ba', 'Ra']],
  ['lanthanoids',['La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er'
    ,'Tm','Yb','Lu']],
  ['actinoids',['Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm'
    ,'Md','No','Lr']],
  ['transition-metals', ['Sc', 'Y','Ti', 'Zr', 'Hf', 'Rf','V','Nb','Ta','Ha'
    ,'Cr','Mo','W','Sg','Mn','Tc','Re','Ns','Fe','Ru','Os','Hs','Co','Rh','Ir'
    , 'Mt','Ni','Pd','Pt', 'Ds', 'Cu','Ag','Au', 'Rg', 'Zn','Cd','Hg', 'Cn']],
  ['post-transition-metals', ['Al','Ga', 'In', 'Tl', 'Nh', 'Sn', 'Pb', 'Fl', 'Bi', 'Mc', 'Lv']]
]);

const BLOCKS_COLORS = new Map([
  ['metalloids','#F9E298'],
  ['other-non-metals','#F2B01D'],
  ['halogens','#85ADC1'],
  ['noble-gases','#F7D660'],
  ['alkali-metals','#D04629'],
  ['alkaline-earth-metals','#F7B57D'],
  ['transition-metals', '#F58737'],
  ['post-transition-metals', '#AE4747'],
  ['lanthanoids','#3B91AE'],
  ['actinoids','#E97147']
]);

let elementNames = ['Hydrogen',  'Helium', 'Lithium', 'Beryllium',
    'Boron',  'Carbon',  'Nitrogen',  'Oxygen',  'Fluorine',
    'Neon', 'Sodium', 'Magnesium', 'Aluminum', 'Silicon',
    'Phosphorus',  'Sulfur',  'Chlorine', 'Argon', 'Potassium',
    'Calcium', 'Scandium', 'Titanium', 'Vanadium',  'Chromium',
    'Manganese', 'Iron', 'Cobalt', 'Nickel', 'Copper',
    'Zinc', 'Gallium', 'Germanium', 'Arsenic', 'Selenium',
    'Bromine', 'Krypton', 'Rubidium', 'Strontium', 'Yttrium',
    'Zirconium', 'Niobium', 'Molybdenum', 'Technetium', 'Ruthenium',
    'Rhodium', 'Palladium', 'Silver', 'Cadmium', 'Indium',
    'Tin', 'Antimony', 'Tellurium', 'Iodine',  'Xenon',
    'Cesium', 'Barium', 'Lanthanum', 'Cerium', 'Praseodymium',
    'Neodymium', 'Promethium', 'Samarium', 'Europium', 'Gadolinium',
    'Terbium', 'Dysprosium', 'Holmium', 'Erbium', 'Thulium',
    'Ytterbium', 'Lutetium', 'Hafnium', 'Tantalum', 'Tungsten',
    'Rhenium', 'Osmium', 'Iridium', 'Platinum', 'Gold',
    'Mercury', 'Thallium', 'Lead', 'Bismuth', 'Polonium',
    'Astatine', 'Radon', 'Francium', 'Radium', 'Actinium',
    'Thorium', 'Protactinium', 'Uranium',  'Neptunium', 'Plutonium',
    'Americium', 'Curium', 'Berkelium', 'Californium', 'Einsteinium',
    'Fermium', 'Mendelevium', 'Nobelium', 'Lawrencium', 'Rutherfordium',
    'Dubnium', 'Seaborgium', 'Bohrium', 'Hassium', 'Meitnerium', 'Darmstadtium',
    'Roentgenium', 'Copernicium', 'Nihonium', 'Flerovium', 'Moscovium',
    'Livermorium', 'Tennessine', 'Oganesson'
];


// utility functions

function getElementBlock(elSymbol){
  let block;
  BLOCKS.forEach(function(value, key) {
    //console.log(key + " " + value);
    if (value.indexOf(elSymbol) >= 0)
      block= key;
  });
  return block;
}


let CELL_WIDTH= 36;

function getCellHtml(elNum){
  let elSymbol= util.ELEMENTS[elNum-1];
  return '<td class="cell '+getElementBlock(elSymbol)+'" data-el="el-'+elSymbol+'">'+
          '<b>'+elSymbol+'</b> <div>'+elNum+'</div> </td>';
}

function getOddCellHtml(elNum){
  let elSymbol= util.ELEMENTS[elNum-1];
  return '<td class="cellpad '+getElementBlock(elSymbol)+'" data-el="el-X">'+
          '<b>&nbsp;</b> <div>&nbsp;</div> </td>';
}


/**  Group selection not implemented

function getGroupSelectorHtml(num){
  return '<th class="group-sel" data-group="'+num+'"><div></div></th>';
}

function getGroupSelectorHtmlLAcAc(num){
  return '<th class="group-sel-la-ac" data-group="'+num+'"><div></div></th>';
}*/

/************************/


class ElemenTable{

  constructor() {

    this.element = document.createElement('div');
    this.element.setAttribute('id','elementable');

    // header with dropdown
    let tempHtml = '<div class="element-info"></div>';

    tempHtml+= '<div class="ptWrapper">';

    tempHtml+= '<table id="pt-main">'; // table zone div

    /* Deactivated for the moment
    // header with group selectors
    tempHtml+= '<thead><tr>';//'<div id="group-selectors">';
    for (let i= 1; i<=18; i++){
      tempHtml+= getGroupSelectorHtml(i);
    }
    tempHtml+= '</tr></thead>';// selectors
    */

    tempHtml+= '<tbody>';
    // row 1
    tempHtml+=  '<tr>'+getCellHtml(1);
    tempHtml+= '<td class="cellpad" colspan="16"></td>';
    tempHtml+= getCellHtml(2)+'</tr>';

    let get8ElementRowHtml= (initPos) => {
      tempHtml+= '<tr>'+getCellHtml(initPos)+getCellHtml(initPos+1);
      tempHtml+= '<td class="cellpad" colspan="10"></td>';
      for (let i= initPos+2; i< initPos+8; i++)  tempHtml+= getCellHtml(i);
      tempHtml+= '</tr>';//div.row
    }

    // row 2 and 3
    get8ElementRowHtml(3);
    get8ElementRowHtml(11);

    // row 4,5, 6
    let counter= 19;
    for (let i= 0; i<4; i++){
      tempHtml+= '<tr>';
      for (let j= 0; j<18; j++){
        if (counter === 57 || counter === 89){
          tempHtml += getOddCellHtml(counter);
          counter += 15;
        }else{
          tempHtml+= getCellHtml(counter);
          counter++;
        }
      }
      tempHtml+= '</tr>';//div.row
    }


    tempHtml+= '</tbody></table>';

    // Lanthanides and Actinides
    tempHtml+= '<div id="specialRows"><table id="pt-laac">';
    for (let i= 0; i<2; i++){
      tempHtml+= '<tr>';
      counter = (i === 0 ? 57 : 89);
      //tempHtml+= getGroupSelectorHtmlLAcAc((i === 0 ? 19 : 20));
      for (let j= 0; j<15; j++){
        tempHtml+= getCellHtml(counter);
        counter++;
      }
      tempHtml+= '</tr>';//div.row
    }
    tempHtml+= '</table></div>'; //div#specialRows


    // Block labels
    tempHtml+= `<div class="legend">
      <div class="alkali-metals">Alkali metals</div>
      <div class="alkaline-earth-metals">Alkaline earth metals</div>
      <div class="transition-metals">Transition metals</div>
      <div class="post-transition-metals">Post-transition metals</div>
      <div class="metalloids">Metalloids</div>
      <div class="other-non-metals">Other nonmetals</div>
      <div class="halogens">Halogens</div>
      <div class="noble-gases">Noble gases</div>
      <div class="lanthanoids">Lanthanoids</div>
      <div class="actinoids">Actinoids</div>
    </div>`;//'<div id="group-selectors">';


    tempHtml+= '</div>'; // ptWrapper

    this.element.innerHTML= tempHtml;
    this.elementInfo= this.element.getElementsByClassName('element-info')[0];
    this.tableZone= this.element.getElementsByClassName('ptWrapper')[0];
    //this.specialRows= this.element.querySelector('#specialRows');

    this._events();
  }


  _events() {

    // One listener for all diferent clicks (simple element, group)
    var adhocListener= (e) => {

      if (e.target !== e.currentTarget) { // When the event source is a child
        let className = e.target.className;
        let element = e.target;
        if (className  === ''){
          element= e.target.parentElement;
          className = e.target.parentElement.className;
        }

        if (className.indexOf('cellpad') >= 0) return; // structural empty table cells

        if (className.indexOf('group-sel') >= 0){ // group selector cells
          //*** Not working at the moment - group selection deactivated
          let elements=  GROUPS.get(parseInt(element.getAttribute('data-group')));
          //console.log("group-sel "+elements);
          this.clickListener(elements);//let done =
          //if (done)
          for (var i = 0; i < elements.length; i++)
            this.selectElement(elements[i]);

        }else if (className.indexOf('cell') >= 0){  // element cells
          let html= element.innerHTML;
          let elSymbol= html.substring(3,html.indexOf('<',3));
          if (elSymbol === '&nbsp;') return;  // blank cells
          //console.log("elSymbol-sel "+elSymbol);
          if (className.indexOf('el-selected') >= 0){ // If selected
            this.deselectListener(elSymbol);//this.deselectElement(elSymbol);
          }else{ // If not selected
            this.clickListener([elSymbol]);//let done = this.clickListener([elSymbol]);
            this.selectElement(elSymbol);//if (done) this.selectElement(elSymbol);
          }
        }
      }
      //e.stopPropagation();
    };

    // Event listener set in the root div element
    this.tableZone.addEventListener('click',adhocListener,true);

    this.tableZone.addEventListener('mouseover',e => {

      let elSymbol= getElement(e);
      //console.log("ENTERIG elSymbol-sel "+elSymbol);
      if (elSymbol !== null){
          //console.log("elSymbol-sel "+elSymbol);
          this.elementInfo.style.display = 'block';
          let borderColor= BLOCKS_COLORS.get(getElementBlock(elSymbol));
          this.elementInfo.style.borderColor = borderColor;
          let number= util.ELEMENTS.indexOf(elSymbol)+1;
          this.elementInfo.innerHTML= `
          <div>
            <div style="float: right; padding: 3px 4px;border-left: 3px solid ${borderColor};
              border-bottom: 3px solid ${borderColor}" > ${number} </div>
            <div style="clear: right;"></div>
          </div>
          <div class="symbol">${elSymbol} </div>
          <div class="">${elementNames[number-1]}  </div>
          `
      }
    });

    this.tableZone.addEventListener('mouseout',e => {
      let element= getElement(e);
      if (element !== null) this.elementInfo.style.display = 'none';
    });

  }

  // Observer pattern
  setClickListener(listener) {
    this.clickListener= listener;
  }


  setDeselectListener(listener) {
    this.deselectListener= listener;
  }


  selectElement(elSymbol) {
    this.element.querySelector('td[data-el="el-'+elSymbol+'"]')
      .className= 'cell el-selected';
  }


  deselectElement(elSymbol) {
    //document.getElementById('el-'+elSymbol).className= 'cell '+getElementBlock(elSymbol);
    this.element.querySelector('td[data-el="el-'+elSymbol+'"]')
      .className= 'cell '+getElementBlock(elSymbol);
  }


  deselectAllElements(){
    let selectedElements = this.element.querySelectorAll('td.el-selected');

    //selectedElements.forEach( element => {
    for (let i = 0; i < selectedElements.length; ++i){
      let elSymbol = selectedElements[i].getAttribute('data-el').substring(3);
      selectedElements[i].className= 'cell '+getElementBlock(elSymbol);
    }
  }

} // class ElemenTable


function getElement(e){

  let element = null;
  let className = null;
  //console.log("TARGET " +e.target.className+'  '+e.target.innerHTML+'   ');
  if (e.target.className.indexOf('cell ') >= 0){
    element= e.target;
    className = e.target.className;

  }else if (e.target.parentElement.className.indexOf('cell ') >= 0){
    element= e.target.parentElement;
    className = e.target.parentElement.className;
  }

  if (element === null) return null;
  else{
    let html= element.innerHTML;
    let elSymbol= html.substring(3,html.indexOf('<',3));
    if (elSymbol === '&nbsp;') return null;  // blank cells
    else return elSymbol;
  }
}

// EXPORTS
module.exports = ElemenTable;
