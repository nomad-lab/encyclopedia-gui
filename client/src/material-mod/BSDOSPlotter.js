
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  Implements a graphical UI component composed of a DOS plotter and a
  Band Structure plotter
 */


let BSPlotter = require('./BSPlotter.js');
let DOSPlotter = require('./DOSPlotter.js');
let svg = require('../common/SVG.js');


class BSDOSPlotter{

  constructor() {
    this.element = document.createElement('div');
    this.element.setAttribute('style','margin: 0 auto');
    this.parentElement= null;
    this.bsPlotter= new BSPlotter();
    this.dosPlotter= new DOSPlotter({left: 4, right: 16, top: 0, bottom: 30});
    this.dosYAxisLabeled = false;
  }


  attach(element, width, height){
    element.appendChild(this.element);
    //this.bsPlotter.attach(this.element, element.clientWidth/2 + 200 -20/*padding*/, height);
    //this.dosPlotter.attach(this.element, element.clientWidth/2 - 200 -20/*padding*/, height);
    this.bsPlotter.attach(this.element, height, height);
    this.height = height;
    this.dosPlotter.attach(this.element, this.height/2+20, height);
    this.parentElement= element;
  }


  isAttached(){
    return this.parentElement !== null;
  }


  setUpAndData(dispData, dosData, codeName){

    this.hasDispData = (dispData !== undefined && dispData !== null);
    this.hasDosData = (dosData !== undefined && dosData !== null);

    // Create a new DOS graph with/without left axis and labels
    let newDosYAxisLabeled;
    if (this.hasDosData && !this.hasDispData) newDosYAxisLabeled = true;
    else newDosYAxisLabeled = false;

    if (this.dosYAxisLabeled !== newDosYAxisLabeled){
      this.element.removeChild(this.dosPlotter.svg);
      let newLeftMargin = (newDosYAxisLabeled ? 40 : 4)
      this.dosPlotter= new DOSPlotter({left: newLeftMargin, right: 16, top: 0, bottom: 30});
      let width = this.height/2 + newLeftMargin;
      this.dosPlotter.attach(this.element, width, this.height);
    }
    this.dosYAxisLabeled = newDosYAxisLabeled;

    if (this.hasDispData){
      this.bsPlotter.setBandStructureData(dispData);
      if (this.hasDosData)
        this.bsPlotter.setRepaintListener( (yZoom, yOffset) => {
          this.dosPlotter.setYZoomAndOffset(yZoom, yOffset);
          this.dosPlotter.repaint();
        });
    }else
      this.bsPlotter.setNoData();

    if (this.hasDosData){

      this.dosPlotter.setPoints(dosData, codeName);

      if (this.hasDispData){
        this.bsPlotter.setExternalYAxisMax(this.dosPlotter.getYAxisMax());

        this.dosPlotter.setRepaintListener( (yZoom, yOffset) => {
          this.bsPlotter.setYZoomAndOffset(yZoom, yOffset);
          this.bsPlotter.repaint();
        });
        // Remove y axis label
        this.dosPlotter.svg.removeChild(this.dosPlotter.yLabelText);
        this.dosPlotter.yLabelText = null;
      }

    }else
      this.dosPlotter.setNoData();

    this.dosPlotter.setYAxisLabelsVisibility(newDosYAxisLabeled);
  }


  setNoData(){
    this.bsPlotter.setNoData();
    this.dosPlotter.setNoData();
  }

}


// EXPORTS
module.exports = BSDOSPlotter;
