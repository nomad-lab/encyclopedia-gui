
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  The StatsViewer class implements a graphical UI component composed of
  a points plotter and a frecuency graph.
  (classes defined in this file MainGraph and FrequencyGraph)
 */


"use strict";

let svg = require('../common/SVG.js');
let PlotterBase = require('../common/PlotterBase.js');


class MainGraph extends PlotterBase{

  constructor() {
    super({left: 54, right: 20, top: 20, bottom: 30});
    this.tooltip;
  }


  drawPoints(points){
    for (let i = 0; i < points.length; i++) {
      //console.log('drawPoint '+this.xRel, i, this.yRel, points[i]);
	    let pointElement = svg.addPoint(this.plotArea,
        this.xRel*i, -this.yRel*(points[i] - this.yMin), 2, 'stats-viewer-point');

	    pointElement.addEventListener('mouseover', (e) => {
		    this.tooltip = svg.addText(this.plotArea, e.target.getBBox().x+6,
          e.target.getBBox().y-4, (points[i]).toFixed(2), 'start', 'tooltip');
        /*getBBox() Rect data (x, y, w, h) of the element were the event happendd,
          coordenate system: the parent element coordinate system */
      });
	    pointElement.addEventListener('mouseout', (e) => {
		    svg.removeElement(this.tooltip);
	    });
	  }
  }

}


class FrequencyGraph extends PlotterBase{

  constructor() {
    super({left: 4, right: 10, top: 20, bottom: 30});
  }

  drawBars(points, yMin, yRange){
	  let ranges = [0,0,0,0,0,0,0,0,0,0];
	  points.forEach(point => {
	    let rangeIndex = Math.floor(((point-yMin)/yRange)*10);
      if (rangeIndex > 9) rangeIndex = 9;
	    ranges[rangeIndex] += 1;
	  });
	  ranges.forEach((value, index) => {
	    let yBar= (index + 0.5)*this.yRel;
	    svg.addLine(this.plotArea, 0, -yBar, value*this.xRel, -yBar, 'bar');
	  });
  }
}


class StatsViewer{

  constructor() {
    this.mainGraph = new MainGraph();
    this.freqGraph = new FrequencyGraph();
  }


  attach(element, width, height){
    this.mainGraph.attach(element, width/2 + 24, height);
	  this.freqGraph.attach(element, width/2 - 24, height);
  }


  drawPoints(points, label, min, max){
    let defMin = min, defMax = max;
    if (max === min){  defMin = min-1;  defMax = max+1;  }

    this.mainGraph.setRangeAndLabels('Calculation', 0, points.length, label,
      defMin, defMax);
	  this.mainGraph.drawAxis(null,(max === min ? 2: 4),(min > 1000 ? 0 : 2));
    this.mainGraph.drawPoints(points);

    let freqRange= (points.length < 10 ? 10 : Math.floor(points.length/10)*10);
    this.freqGraph.setRangeAndLabels('Occurrence', 0, freqRange, '', 0, 10);
	  this.freqGraph.drawAxis(2, 1, 1);
    this.freqGraph.drawBars(points, this.mainGraph.yMin,
      this.mainGraph.yMax - this.mainGraph.yMin);
  }


  clear(){
    this.mainGraph.clear();
    this.freqGraph.clear();
  }

}


// EXPORTS
module.exports = StatsViewer;
