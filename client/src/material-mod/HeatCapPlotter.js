
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  Specific Heat plotter implementation
 */


"use strict";

let svg = require('../common/SVG.js');
let PlotterBase = require('../common/PlotterBase.js');


class HeatCapPlotter extends PlotterBase{

  constructor() {
    super({left: 50, right: 16, top: 10, bottom: 32});
    this.tooltip;
  }


  setData(data){
    //console.log(JSON.stringify(data));//
    this.clear();
    // up to 600K data is taken
    let indexOf600K = data.temperature.indexOf(600)+1;
    let values = data.value.slice(0, indexOf600K);
    let temperatures = data.temperature.slice(0, indexOf600K);

    let yMaxValue = Math.max.apply(null, values);
    //console.log('maxValue: ',maxValue);//
    this.setRangeAndLabels('T (K)', 0, 600, 'Cv (J/K/kg)', 0, Math.ceil(yMaxValue/200)*200);
    this.drawAxis(4, 4, 0);

    let polylinePoints = '';
    temperatures.forEach( (t, i) => {
        let y = values[i];///1e-25;
        polylinePoints+= ' '+this.xRel*t+' -'+this.yRel*(y - this.yMin);
    });
    svg.addPolyline(this.plotArea, polylinePoints, 'plotSpin1');
  }

}


// EXPORTS
module.exports = HeatCapPlotter;
