
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  Equation of State plotter implementation
 */

"use strict";

let svg = require('../common/SVG.js');
let PlotterBase = require('../common/PlotterBase.js');


class EquationOfStateViewer extends PlotterBase{

  constructor() {
    super({left: 60, right: 20, top: 30, bottom: 40});
    this.tooltip;
    this.calcPointMap= new Map();
    this.pointSelected = null;
  }


  draw(pointsX, pointsY, calcIds, eZero){

    for (let i = 0; i < pointsY.length; i++) {
      if (pointsY[i] !== 555) pointsY[i] -= eZero;
    }

    function remove555(points){
      let goodPoints = [];
      points.forEach( p => {
        if (p !== 555) goodPoints.push(p);
      });
      return goodPoints;
    }
    let goodPointsY = remove555(pointsY);

    let xMin = Math.min.apply(null,pointsX);
    let xMax = Math.max.apply(null,pointsX);
    let yMin = 0;
    let yMax = Math.max.apply(null,goodPointsY);

    if (xMin === xMax) { xMin -= 1; xMax += 1; }
    else{
      let gap = xMax - xMin;
      xMin -= gap*0.1; xMax += gap*0.1;
    }
    if (yMin === yMax) { yMin -= 1; yMax += 1; }
    else{
      let gap = yMax - yMin;
      yMin -= gap*0.15; yMax += gap*0.1;
    }

    this.setRangeAndLabels('Volume (Å³)', xMin, xMax,'E - Eₘᵢₙ (eV)', yMin, yMax);
    this.drawAxis(2, null);
    // Y axis labels drawing
    let self = this;
    function drawYAxisLabel(element, y){
      let pixelY = self.y(y);
      svg.addLine(element, 0, pixelY, -3, pixelY, 1);
      let numberToPaint= -(pixelY/self.yRel) + self.yMin;
      svg.addText(element,-5, pixelY+3, numberToPaint.toFixed(3), 'end', 'statisticsviewersteps');
    }
    drawYAxisLabel(this.plotArea, 0);
    drawYAxisLabel(this.plotArea, yMax/2);
    drawYAxisLabel(this.plotArea, yMax);

    // zero line drawing
    svg.addLine(this.plotArea, 0, this.y(0), this.plotRangeX, this.y(0), 'zeroline');

    svg.addText(this.plotArea, this.x(xMax),
      this.y(0)+12, 'Eₘᵢₙ: '+eZero.toFixed(3)+' eV', 'end', 'axis-steps');
    // points drawing
    for (let i = 0; i < pointsX.length; i++) {

      let styleClass = (i === 0 ? 'eos-viewer-sel' : 'eos-viewer');
      let r = (i === 0 ? 6 : 3);
      let yVal = (pointsY[i] === 555 ? 20 : this.y(pointsY[i])); // Trick
	    let pointElement =
        svg.addPoint(this.plotArea,this.x(pointsX[i]), yVal, r
        , styleClass);
      if (i === 0) this.pointSelected = pointElement;

	    pointElement.addEventListener('mouseover', e => {
		    this.tooltip = svg.addText(this.plotArea, e.target.getBBox().x+10,
          e.target.getBBox().y-10, 'Calc '+calcIds[i], 'middle', 'tooltip');
      });
	    pointElement.addEventListener('mouseout', e => {
		    svg.removeElement(this.tooltip);
	    });
      pointElement.addEventListener('click', e => {
		    //console.log('ID',calcIds[i]);
        this.clickPointListener(calcIds[i]);
	    });
      this.calcPointMap.set(calcIds[i], pointElement);
	  }
  }


  selectCalc(calcId){
    this.pointSelected.setAttribute('class', 'eos-viewer');
    this.pointSelected.setAttribute('r', 3);
    this.pointSelected = this.calcPointMap.get(calcId);
    this.pointSelected.setAttribute('class', 'eos-viewer-sel');
    this.pointSelected.setAttribute('r', 6);
  }


  setClickPointListener(listener){
    this.clickPointListener = listener;
  }


  x(x){
    return this.xRel*(x - this.xMin);
  }


  y(y){
    return -this.yRel*(y - this.yMin);
  }

}



// EXPORTS
module.exports = EquationOfStateViewer;
