/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  This class implements the bar to select calculations on a material
 */


class CalcSelectorBar{

  constructor(className, width){
    this.first = true;
    this.last = false;
    this.element = document.createElement('div');
    this.element.className = className;
    if (width !== undefined) this.element.style.width = width;
    this.element.innerHTML = `
      <div class="prev-sel-btn" style="float: left; width: 20%;">
        <div style="padding-left: 10%;">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.069 11.872" width="20px">
            <path transform="scale(0.7) translate(-346.291 -664.481)"
              d="M356.36,666.024l-1.544-1.544-8.525,8.513,8.493,8.447,1.544-1.544-6.8-6.9Z" />
          </svg>
        </div>
      </div>
      <div class="calc-sel-text" style="float: left; width: 60%;">
       NOT Calculation
      </div>
      <div class="next-sel-btn" style="float: right; width: 20%;">
        <div style="padding-right: 10%;">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="-10.069 -11.872 10.069 11.872" width="20px">
            <g transform="rotate(180) scale(0.7)">
              <path d="M10.069,1.544,8.525,0,0,8.513,8.493,16.96l1.544-1.544-6.8-6.9Z"/>
            </g>
          </svg>
        </div>
      </div>
      <div style="clear: both;"></div>
    `;

    this.prevBtn = this.element.querySelector('.prev-sel-btn');
    this.prevIcon = this.element.querySelector('.prev-sel-btn path');
    this.calcSelectorTxt = this.element.querySelector('.calc-sel-text');
    this.nextBtn = this.element.querySelector('.next-sel-btn');
    this.nextIcon = this.element.querySelector('.next-sel-btn path');
    this._styleButtons();
    this._events();
  }


  _events() {
    this.prevBtn.addEventListener( "click", e => {
      e.preventDefault();
      if (this.first) return;
      /*** repensar esto es problematico porque necesita poder ser configurado desde fuera **/
      //if (this.last)  this.last = false;
      this.first = this.prevListener();
      this.last = false;
      this._styleButtons();
    });

    this.nextBtn.addEventListener( "click", e => {
      e.preventDefault();
      if (this.last) return;
      //if (this.first)  this.first = false;
    //  this.last = this.nextListener();
      this.first = false;
      this.last = this.nextListener();
      this._styleButtons();
    });
  }


  _styleButtons(){
    this.prevIcon.setAttribute("class",
      'calc-selector-icon'+(this.first ? '-disabled' : ''));
    this.nextIcon.setAttribute("class",
      'calc-selector-icon'+(this.last ? '-disabled' : ''));
  }


  setPrevListener(listener){
    this.prevListener = listener;
  }


  setNextListener(listener){
    this.nextListener = listener;
  }


  setState(text, first, last){
    this.calcSelectorTxt.innerHTML = text;
    this.first = first;
    this.last = last;
    this._styleButtons();
  }

}


// EXPORTS
module.exports = CalcSelectorBar;
