
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  'Details' view container that shows the material calculations info related to
  their vibrational and thermal properties.
 */

"use strict";

let DetailsViewBase = require('./DetailsViewBase.js');
let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let DataStore = require('./DataStore.js');
let LoadingPopup = require('../common/LoadingPopup.js');

let HeatCapPlotter = require('./HeatCapPlotter.js');
let HelmholtzPlotter = require('./HelmholtzPlotter.js');
let PhononDispDOSPlotter = require('./PhononDispDOSPlotter.js');


class ThermalPropsDetails  extends DetailsViewBase{

  constructor() {
    super('Thermal Properties');
    this.firstId;
    this.lastId;
    this.element.innerHTML+=`
      <div style="float: left; width: 30%;">
        <div class="view-box">
          <div class="title">Calculations </div>
          <div class="navTreeWrapper"></div>
        </div>
      </div>

      <div style="float: right; width: 70%;">
        <div class="view-box thermal-properties-box">

          <div class="title">Vibrational and thermal properties</div>

          <div style="padding-top: 10px;">
            <div class="tree-leaf-title"></div>
          </div>

          <div class="calc-disp-dos-plotter" style="padding: 30px 100px; ">
            <div class="info-fields-label" style="float: left; width: 52%; ">
              <span info-sys-data="phonon-dispersion">Phonon dispersion </span>
            </div>
            <div class="info-fields-label" style="float: left;">
              <span info-sys-data="phonon-DOS">Phonon DOS  </span>
            </div>
            <div style="clear: both;"></div>
          </div>


          <div class="band" >
            <div style="padding: 30px 50px; display: flex; justify-content: space-around; ">

              <div >
                <div class="info-fields-label" >
                  <span info-sys-data="specific-heat-cv">Specific heat</span>
                </div>
                <div class="heat-plotter" >      </div>
              </div>

              <div>
                <div class="info-fields-label" >
                  <span info-sys-data="helmholtz-free-energy">Helmholtz free energy</span>
                </div>
                <div class="helmholtz-plotter" >      </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    `;

    this.navTreeWrapper =
        this.element.getElementsByClassName("navTreeWrapper")[0];

    this.rightBox = this.element.querySelector('.thermal-properties-box');
    this.leafTitle = this.element.querySelector('.tree-leaf-title');

    this.dispDosPlotter = new PhononDispDOSPlotter();
    this.heatPlotter= new HeatCapPlotter();
    this.helmholtzPlotter= new HelmholtzPlotter();

    InfoSys.addToInfoSystem(this.element);
  }


  _events() {
    super._events();
  }


  updateSelection(leafIds/*Set*/){
    //console.log('E-StructureDetails UPDATING calcs: ',calcs.values().next().value);
    if (leafIds.size > 0){
      this.rightBox.style.visibility = 'visible';
      let counter= 0;
      leafIds.forEach( calcId => {
        counter++;
        if (counter === 1) this.firstId = calcId;
        else if (counter === leafIds.size) this.lastId = calcId;
      });
    }else
      this.rightBox.style.visibility = 'hidden';
    //console.log('METHODOLOGY '+this.tbody.innerHTML);
  }



  updateMarkedLeaf(leafId){
    //console.log('updateMarkedCalc ',calc);
    if (leafId === null){
      this.leafTitle.innerHTML = 'NO SELECTION';
      return;
      //this.bandGapField.textContent= '';
    }else{
      if (DataStore.getGroups().has(leafId)){
        this.leafTitle.innerHTML = leafId+
          ' ('+DataStore.getGroups().get(leafId).calcs.size+')';
      }else
        this.leafTitle.innerHTML = leafId;
    }

    let calc = DataStore.getCalc(DataStore.getCalcReprIntId(leafId));

    if (!this.dispDosPlotter.isAttached()){
      this.dispDosPlotter.attach(this.element.querySelector('.calc-disp-dos-plotter'),undefined,360);
      this.heatPlotter.attach(this.element.querySelector('.heat-plotter'),317,317);
      this.helmholtzPlotter.attach(this.element.querySelector('.helmholtz-plotter'),317,317);
    }
    if (calc === null || (!calc.has_phonon_dos && !calc.has_phonon_dispersion
                          && !calc.has_thermal_properties)){
      this.dispDosPlotter.setNoData();
      this.heatPlotter.setNoData();
      this.helmholtzPlotter.setNoData();
    }else{
      LoadingPopup.show();
      let matId = DataStore.getMaterialData().id;



      util.serverReq(util.getMaterialCalcURL(matId, calc.id,'phonon_dos'), e=> {
        let dosData= JSON.parse(e.target.response).phonon_dos;

        util.serverReq(util.getMaterialCalcURL(matId, calc.id,
            'phonon_dispersion'), e2 => {
          let dispersionData= JSON.parse(e2.target.response).phonon_dispersion;
          this.dispDosPlotter.setUpAndData(dispersionData, dosData);

          if (calc.has_thermal_properties){

            util.serverReq(util.getMaterialCalcURL(matId, calc.id,
              'specific_heat_cv'), e3 => {
                let sHeatData= JSON.parse(e3.target.response).specific_heat_cv;
                this.heatPlotter.setData(sHeatData);
            });

            util.serverReq(util.getMaterialCalcURL(matId, calc.id,
              'helmholtz_free_energy'), e4 => {
                let helmholtzData = JSON.parse(e4.target.response).helmholtz_free_energy;
                this.helmholtzPlotter.setData(helmholtzData);
            });

          }else{
            this.heatPlotter.setNoData();
            this.helmholtzPlotter.setNoData();
          }

          LoadingPopup.hide();
        });
      });

    }

  }


  setPrevCalcListener(listener){
    this.prevCalcListener= listener;
  }


  setNextCalcListener(listener){
    this.nextCalcListener= listener;
  }

}

// EXPORTS
module.exports = ThermalPropsDetails;
