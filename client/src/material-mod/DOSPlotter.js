
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  Implements the DOS plotter.
 */


"use strict";

let InteractivePlotterBase = require('../common/InteractivePlotterBase.js');
let svg = require('../common/SVG.js');
let util = require('../common/util.js');


const E_MIN = -6;
const E_MAX = 11;
const E_FACTOR = 1.602176565e-19;
//const DOSVALUE_FACTOR = 1.602176565e-49;


class DOSPlotter extends InteractivePlotterBase{

  constructor(margins) {
    super(margins);
  }

  // detach if necessary and attach
  attach(element, width, height){
    super.attach(element, width, height);
  }


  setPoints(points){

    let dosValueFactor =  E_FACTOR;
    // add exceptional cases here

    this.pointsSpin1 = [];
    this.pointsSpin2 = [];
    this._reset();

    let pSpin1= points.dos_values[0];
    let pSpin2 = null;
    if (points.dos_values.length === 2)  pSpin2 = points.dos_values[1];
    let pointsY= points.dos_energies;
    let pointsXInPlotRange = [];
    let pointsYInPlotRange = [];

    for (var i = 0; i < pointsY.length; i++) {
      let energy = pointsY[i]/E_FACTOR;
      let dos_value_spin1 = pSpin1[i]*dosValueFactor;

      // Arrays to calculate the range to be represented
      pointsXInPlotRange.push(dos_value_spin1);
      pointsYInPlotRange.push(energy);

      this.pointsSpin1.push({x: dos_value_spin1, y: energy});
      if (pSpin2 !== null){
        let dos_value_spin2 = pSpin2[i]*dosValueFactor;
        this.pointsSpin2.push({x: dos_value_spin2, y: energy});
        pointsXInPlotRange.push(dos_value_spin2);
      }
    }

    let maxDosVal = Math.max.apply(null, pointsXInPlotRange);
    let maxEnergyVal = Math.max.apply(null, pointsYInPlotRange);
    let minEnergyVal = Math.min.apply(null, pointsYInPlotRange);

    // x axis steps generation
    let t = util.generateDiagramSteps(maxDosVal);
    let xSteps = t[0], exp = t[1];

    this.setAxisRangeAndLabels(null, 0, xSteps[xSteps.length-1], 'Energy (eV)',
      E_MIN, E_MAX, minEnergyVal, maxEnergyVal, 5);

    svg.addText(this.axisGroup, this.plotRangeX/2, this.margins.bottom,
      'DOS (states/eV/cell)', 'middle', 'axis-steps-big');

    // draw x axis steps
    for (let i = 0; i < xSteps.length; i++) {
      let stepX = (this.plotRangeX*xSteps[i])/xSteps[xSteps.length-1];
      svg.addLine(this.axisGroup, stepX, 0, stepX, 3, 1);
      //console.log('step ',xSteps[i], stepX);
      svg.addText(this.axisGroup, stepX, 13,
        (i === 0 ? '0' : xSteps[i].toFixed(exp)),'middle', 'axis-steps-smaller');
    }

    this.repaint();
  }


  repaintData(){

    let polylinePoints = '';
    for (var i = 0; i < this.pointsSpin1.length; i++) {
      polylinePoints+= ' '+this.xRel*this.pointsSpin1[i].x+
        ' '+this.transformY(this.pointsSpin1[i].y);
    }
    svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin1');

    polylinePoints = '';
    for (var i = 0; i < this.pointsSpin2.length; i++) {
      polylinePoints+= ' '+this.xRel*this.pointsSpin2[i].x+
        ' '+this.transformY(this.pointsSpin2[i].y);
    }
    svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin2');
  }


  setYAxisLabelsVisibility(value){
    if (this.yAxisLabelsGroup !== null)
      this.yAxisLabelsGroup.style.visibility = (value ? 'visible' : 'hidden');
  }

}


// EXPORTS
module.exports = DOSPlotter;
