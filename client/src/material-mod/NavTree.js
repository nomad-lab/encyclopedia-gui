
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  The NavTree class define the UI tree component showing the material calculations.
  There is only one instance that is shared by several 'Details' views.
 */

"use strict";

let util = require('../common/util.js');
let DataStore = require('./DataStore.js');



function buildCalcTree(calcs, calcsInGroups){

  let calcTree = new Map();
  calcsInGroups.forEach( (groupData, groupId) => {

    let representative = DataStore.getCalc(DataStore.getCalcReprIntId(groupId));
    let codeNameTrimed= representative.code_name.trim();
    let functionalType = representative.functional_type;

    /***** Exception: disable grouping for some codes
    if (codeNameTrimed !== 'VASP' && codeNameTrimed !== 'FHI-aims') */
      if (calcTree.has(functionalType)){
        let functionalMap= calcTree.get(functionalType);

        if (functionalMap.has(codeNameTrimed)){
          // Get an array and push a new calc
          functionalMap.get(codeNameTrimed).push(groupId);
        }else // New code name
          functionalMap.set(codeNameTrimed,[groupId]);

      }else{ // New functional
        let newFunctionalMap= new Map();
        newFunctionalMap.set(codeNameTrimed,[groupId]);
        calcTree.set(functionalType,newFunctionalMap);
      }
  });

  // Simple calcs are added to the tree
  calcs.forEach( calc => {
    //if (!DataStore.isInAnyGroup(calc.id)) addToCalcTree(calc, calc.id+'');
    //****** Exception: disable grouping for some codes
    if (!DataStore.isInAnyNotDisabledGroup(calc.id))
      addToCalcTree(calc, calc.id+'');
  });


  return calcTree;

  function addToCalcTree(calc, id){
    let codeNameTrimed= calc.code_name.trim();
    let functionalType = calc.functional_type;
    if (calcTree.has(functionalType)){
      let functionalMap= calcTree.get(functionalType);

      if (functionalMap.has(codeNameTrimed)){
        // Get an array and push a new calc
        functionalMap.get(codeNameTrimed).push(id);
      }else // New code name
        functionalMap.set(codeNameTrimed,[id]);

    }else{ // New functional
      let newFunctionalMap= new Map();
      newFunctionalMap.set(codeNameTrimed,[id]);
      calcTree.set(functionalType,newFunctionalMap);
    }
  }
}


/* Maybe these functions (getNextNode, getPreviousNode) can be removed
searching by node class calc-l and node-selected. Think about this */

function getNextNode(nodeBox){
  let nextCalcNodeBox= nodeBox.nextElementSibling.nextElementSibling;
  // console.log("nextCALC.getNextNode " +nextCalcNodeBox);
  if (nextCalcNodeBox === null){

    if (nodeBox.parentElement.nextElementSibling !== null)
      nextCalcNodeBox= nodeBox.parentElement.nextElementSibling/*nextCodeNodeBox*/
          .nextElementSibling/*nextCodeBox*/.children[0]/*nextCalc*/;

    else if (nodeBox.parentElement.parentElement.nextElementSibling !== null)
      nextCalcNodeBox= nodeBox.parentElement.parentElement/*FunctionalBox*/
          .nextElementSibling.nextElementSibling/*nextFunctionalBox*/
          .children[1]/*nextCodeBox*/.children[0]/*nextCalc*/;
    else //reaching the final node
      nextCalcNodeBox= null;
  }
  return nextCalcNodeBox;
}


function getPreviousNode(nodeBox){
  let prevCalcNodeBox;
  if (nodeBox.previousElementSibling !== null)
    prevCalcNodeBox= nodeBox.previousElementSibling.previousElementSibling;
  // console.log("nextCALC.getNextNode " +nextCalcNodeBox);
  else{

    if (nodeBox.parentElement.previousElementSibling.previousElementSibling !== null)
      prevCalcNodeBox= nodeBox.parentElement.previousElementSibling/*prevCodeNodeBox*/
          .previousElementSibling/*prevCodeBox*/.children[0]/*prevCalc*/;
    else if (nodeBox.parentElement.parentElement.previousElementSibling.previousElementSibling !== null)
      prevCalcNodeBox= nodeBox.parentElement.parentElement/*FunctionalBox*/
          .previousElementSibling.previousElementSibling/*prevFunctionalBox*/
          .lastElementChild/*prevCodeBox*/.lastElementChild.previousElementSibling/*prevCalc*/;
    else //reaching the final node
      prevCalcNodeBox= null;
  }
  return prevCalcNodeBox;
}



class NavTree {

  constructor() {
    this.selectedCalcs= new Set();

    this.element = document.createElement('div');
    this.element.setAttribute('id','navigation-tree');
    this.parentElement= null;
    this.markedNode = null;
    this._events();
  }

  // detach if necessary and attach
  attach(element){
    if (this.parentElement !== null)
      this.parentElement.removeChild(this.element);
    this.parentElement= element;
    this.parentElement.appendChild(this.element);
  }


  build(materialName){
    // Reset
    this.selectedCalcs.clear();
    this.markedNode = null;
    this.element.innerHTML= '';
    this.calcsInGroups = DataStore.getGroups();


    function getNodeHTML(level, label, unfolded, counter = 0){
      let foldingValue= 'node-'+(unfolded ? 'unfolded' : 'folded');
      let counterTag= (counter === 0 ? '' :
        '<span class="node-counter">('+counter+')</span>');
      return `
        <div class="${level}">
          <span class="${foldingValue}"></span>
          <span class="node-checkbox"></span>
          <span class="node-label" >${label}</span>
          ${counterTag}
        </div>
      `;
    }

    function getCalcGraphInfoAvalabilityHTML(calc){
      let html= '';
      if (calc.has_band_structure) html += '<span class="tooltip">B<span class="tooltiptext">Band structure</span></span> ';
      if (calc.has_dos) html += '<span class="tooltip">D<span class="tooltiptext">Density of states</span></span> ';
      if (calc.has_fermi_surface) html += '<span class="tooltip">F<span class="tooltiptext">Fermi surface</span></span>';
      if (calc.has_thermal_properties) html += '<span class="tooltip">T<span class="tooltiptext">Phonons</span></span>';

      return '&nbsp; <span class="calc-graph-aval">'+html+'</span>';
    }

    // Init map to store calculations data
    let calcs = DataStore.getCalculations();

    let tree= buildCalcTree(calcs, this.calcsInGroups);
    let rootElement= document.createElement('div');
    this.element.appendChild(rootElement);
    rootElement.innerHTML= getNodeHTML('material-l',materialName,true);

    let functionalLevelBox= document.createElement('div');
    rootElement.appendChild(functionalLevelBox);

    tree.forEach((codeMap, functionalName) => {
      //console.log(codeMap + " " + value);
      functionalLevelBox.innerHTML+= getNodeHTML('functional-l',functionalName,true);

      let codeLevelBox= document.createElement('div');
      functionalLevelBox.appendChild(codeLevelBox);

      codeMap.forEach( (calcArray, codeName) => {
        codeLevelBox.innerHTML+= getNodeHTML('code-l', codeName, false, calcArray.length);

        let calcLevelBox= document.createElement('div');
        codeLevelBox.appendChild(calcLevelBox);
        calcLevelBox.style.display= 'none';

        for (var i = 0; i < calcArray.length; i++) {
          let graphInfoAvalabilityHTML = getCalcGraphInfoAvalabilityHTML(
            DataStore.getCalc(DataStore.getCalcReprIntId(calcArray[i])));

          let calcIcon = '', calcNumber = '';
          if (DataStore.isGroup(calcArray[i])){
            calcIcon = '<img class="folder-icon" src="'+util.IMAGE_DIR+'folder.png" />'
            calcNumber = '('+this.calcsInGroups.get(calcArray[i]).calcs.size+')';
          }


          calcLevelBox.innerHTML += `
            <div class="calc-l" data-calc-id="${calcArray[i]}" >
              <span></span>
              <span class="node-checkbox"></span>

              <span class="node-label" >
                ${calcIcon} ${calcArray[i]} ${calcNumber} ${graphInfoAvalabilityHTML}
              </span>

              <div style="float: right; padding: 1px 10px 0 0;  display: none;">
                <img  src="${util.IMAGE_DIR}next.png" />
              </div>

            </div>
            <div> </div>`;
        }
      });
    });
  } // build method


  selectAll(initMarkedLeafId){
    let materialNodeBox= this.element.children[0].children[0];
    this._recursiveNodeSelection(materialNodeBox, true);
    keepTreeIntegrity(materialNodeBox, true);
    // No calc marked
  }


  getMarkedLeaf(){
    if (this.markedNode === null)  return null;
    else return this.markedNode.getAttribute('data-calc-id');
  }


  setMarkedLeafIfNoneMarked(leafId){ // If leafId === null first node selected
    if (this.getMarkedLeaf() === null){ // If none marked
      if (leafId === null) this._markFirstSelectedNode();
      else{
        let nodeBox = this.element.querySelector('div[data-calc-id="'+leafId+'"]');
        this._setMarkedCalc(nodeBox);
      }
    }
  }



  _events() {
    this.element.addEventListener('click',(e) => {
      let classString = e.target.className;

      // drop down/up event
      if (classString.indexOf('folded')  >= 0){
        this._foldTreeNode(e.target);

        // descendant selection/deselection event
      }else if ((classString.indexOf('node-checkbox')  >= 0)){

        let selectMode= (e.target.parentElement.className.indexOf('selected') < 0);
        this._recursiveNodeSelection(e.target.parentElement, selectMode);
        keepTreeIntegrity(e.target.parentElement, selectMode);
        this.treeSelectionListener(this.selectedCalcs);
        this._keepCalcMarked(selectMode); //if (this.calcMarked)

      }else if (/*this.calcMarked && */(classString.indexOf('node-label')  >= 0 )
        && (e.target.parentElement.className === 'calc-l node-selected')){
        this._setMarkedCalc(e.target.parentElement/*nodeBox*/);
      }
    });

  }


  _foldTreeNode(dropDowmElement){
    let siblingElement= dropDowmElement.parentElement.nextElementSibling;
    let classString = dropDowmElement.className;

    if (classString.indexOf('-folded')  >= 0) {
      dropDowmElement.className= dropDowmElement.className.replace('folded','unfolded');
      siblingElement.style.display= 'block';
    }else {
      dropDowmElement.className= dropDowmElement.className.replace('unfolded','folded');
      siblingElement.style.display= 'none';
    }
  }


  _recursiveNodeSelection(nodeBox, select){

    let nodeCheckBox= nodeBox.children[1];
    if (select){
        nodeBox.className += ' node-selected';
    }else{ // deselect
        let index= nodeBox.className.indexOf(' node-selected');
        nodeBox.className= nodeBox.className.substring(0,index);
    }

    if (nodeBox.className.indexOf('calc-l')  >= 0)  { // leaf node
      let id= nodeBox.getAttribute('data-calc-id');//let id= parseInt(nodeBox.getAttribute('data-calc-id'));
      if (select)  this.selectedCalcs.add(id);
      else this.selectedCalcs.delete(id);

    }else { // Not leaf node

      let nextLevelBox = nodeBox.nextElementSibling;  // next levelBox
      // Two children per banch: the first one is the node label box
      // and the second one the next level with the descendants
      for (let i = 0; i < nextLevelBox.children.length; i++ ) {
        this._recursiveNodeSelection(nextLevelBox.children[i++], select);
      }
    }
  }



  setTreeSelectionListener(listener){
    this.treeSelectionListener= listener;
  }


  setLeafMarkedListener(listener){
    this.leafMarkedListener = listener;
  }


  getTreeSelectedCalcs(){
    return this.selectedCalcs;
  }


  _keepCalcMarked(select){

    if (select && (this.markedNode === null)){
      this._markFirstSelectedNode();

    }else if (!select){
      let id= parseInt(this.markedNode.getAttribute('data-calc-id'));
      if (this.selectedCalcs.size === 0){
        this.markedNode.className= this.markedNode.className.replace('-marked','');
        this.markedNode= null;
        this.leafMarkedListener(null);
      }else  if (!this.selectedCalcs.has(id)){
        this._markFirstSelectedNode();
      }
    }
  } // _keepCalcMarked


  _markFirstSelectedNode(){
    let calcNodeBoxes = this.element.getElementsByClassName('calc-l');
    for (var i = 0; i < calcNodeBoxes.length; i++)
      if (calcNodeBoxes[i].className.indexOf('node-selected') >= 0){
        this._setMarkedCalc(calcNodeBoxes[i]);
        return;
      }
  }


  _setMarkedCalc(nodeBox){
    if (this.markedNode !== null){
      this.markedNode.className= this.markedNode.className.replace('-marked','');
      this.markedNode.querySelector('div').style.display = 'none';
      let folderIcon = this.markedNode.querySelector('.folder-icon');
      if (folderIcon !== null ) folderIcon.src = util.IMAGE_DIR+'folder.png';
    }

    nodeBox.className += '-marked';
    let folderIcon = nodeBox.querySelector('.folder-icon');
    if (folderIcon !== null ) folderIcon.src = util.IMAGE_DIR+'folder-sel.png';
    nodeBox.querySelector('div').style.display = 'block';
    this.markedNode= nodeBox;

    // The parent tree node is unfolded in order to show the leaf selected
    let foldingElement = nodeBox.parentElement.previousElementSibling.firstElementChild;
    if (foldingElement.className === 'node-folded'){
      foldingElement.className = 'node-unfolded';
      foldingElement.parentElement.nextElementSibling.style.display= 'block';
    }

    if (this.leafMarkedListener !== undefined)
      this.leafMarkedListener(nodeBox.getAttribute('data-calc-id'));
  }

  showCalcsGraphDataAvalability(bool){
    let elements= this.element.getElementsByClassName('calc-graph-aval');
    for (var i = 0; i < elements.length; i++)
      elements[i].style.display= (bool ? 'inline' : 'none');
  }

  setHeight(heightPx){
    this.element.style.height = heightPx+'px';
  }

} // class NavTree



function keepTreeIntegrity(nodeBox, select){

  if (nodeBox.className.indexOf('material-l') >= 0) return;
  let levelBox= nodeBox.parentElement;

  for (let i = 0; i < levelBox.children.length; i++ ) {
    let siblingNodeBox = levelBox.children[i++];
    if (siblingNodeBox !== nodeBox &&
          siblingNodeBox/*.children[1]*/.className.indexOf('selected') < 0)
      return;
  }
  let parentNodeBox= levelBox.previousElementSibling;
  if (select){
    parentNodeBox.className += ' node-selected';
  }else{
    let index= parentNodeBox.className.indexOf(' node-selected');
    parentNodeBox.className= parentNodeBox.className.substring(0,index);
  }
  keepTreeIntegrity(parentNodeBox, select);
}




// EXPORTS
module.exports = NavTree;
