
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  This file implements the Overview view component in the  Material Module.
 */


"use strict";

let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let LoadingPopup = require('../common/LoadingPopup.js');

let BSPlotter = require('./BSPlotter.js');
let DOSPlotter = require('./DOSPlotter.js');
let HeatCapPlotter = require('./HeatCapPlotter.js');
let MaterialMod = require('./MaterialMod.js');
let DataStore = require('./DataStore.js');

const ELEMENT_INCLUDED_MSG = 'ELEMENT ALREADY INCLUDED';


class Overview {

  constructor() {

    this.element = document.createElement('div');
    this.element.setAttribute('id','overview');
    this.materialId;
    this.element.innerHTML=`

    <div class="material-title">
    </div>

    <div style="float: left; width: 40%;">

      <div id="structure-ov" class="view-box">
        <div class="title">Structure
          <img style="float: right" class="to-detail" src="img/more.svg" />
          <div style="clear: both;"></div>
        </div>

        <div class="viz-box" style="height: 260px; position: relative"></div>

        <div class="footer">
          <div ><b><span>System type</span></b>:
            <span class="system-type-field" ></span>
          </div>
          <div class="space-group-field" style="display: none">
            <b><span info-sys-data="space-group">Space group</span></b>:
            <span class="space-group-value" ></span>
          </div>
          <div class="structure-type-field" style="display: none">
            <b><span info-sys-data="structure-type">Structure type</span></b>:
            <span class="structure-type-value" ></span>
          </div>
        </div>
      </div>


<!-- ***** Elastic Constants Box

      <div id="elastic-ov" class="view-box">
        <div class="title">Elastic constants
          <img style="float: right" class="to-detail" src="img/more.svg" />
          <div style="clear: both;"></div>
        </div>

        <div class="info-fields">
          Not analyzed yet
        </div>

      </div>
-->


      <div id="methodology-ov" class="view-box">
        <div class="title">Methodology
          <img style="float: right" class="to-detail" src="img/more.svg" />
          <div style="clear: both;"></div>
        </div>

        <div class="info-fields">

          <div class="info-fields-label" > Available calculations </div>

          <div style="float: left; width: 45%" >
            <b><span info-sys-data="functional-type">Functional</span></b>
            <div class="functional-field" > </div>
          </div>
          <div style="float: right; width: 45%" >
            <b><span info-sys-data="code-name">Code</span></b>
            <div class="code-field"> </div>
           </div>
           <div style="clear: both;"></div>
        </div>

      </div>

    </div>

    <div style="float: right; width: 60%;">

      <div id="e-structure-ov"  class="view-box" > <!--style="height: 610px; "-->
        <div class="title">Electronic structure
          <img style="float: right" class="to-detail" src="img/more.svg" />
          <div style="clear: both;"></div>
        </div>

        <div > <!-- style="margin: 12% 0; " -->

        <div style="float: left; width: 60%;  ">
        <div style="padding: 20px 0 20px 30px">
          <div  class="info-fields-label">
            <span info-sys-data="band-structure">Band structure</span>
          </div>
          <div>
              <div id="band-plotter" >  </div>
          </div>

          <div class="footer-bs-calc"></div>
        </div>
        </div>

        <div style="float: left; width: 40%;  ">
          <div style="padding: 20px 30px 20px 60px">
            <div class="info-fields-label">
              <span info-sys-data="DOS">DOS</span>
            </div>

            <div>
                <div id="dos-plotter" >  </div>
            </div>
            <div class="footer-dos-calc"></div>
          </div>
        </div>


        <div style="clear: both;"></div>

        <div class="spin-legend" style="font-size: 0.9em; padding: 6px 30px 10px; display: none">
          <svg width="15px" height="10px"> <polyline points="0,5 15,5" class="plotSpin1"/></svg>
          Spin <span style='font-size: 1.1em'>⇧</span>  &nbsp;&nbsp;&nbsp;

          <svg width="15px" height="10px"> <polyline points="0,5 15,5" class="plotSpin2"/></svg>
          Spin <span style='font-size: 1.1em'>⇩</span>
        </div>

        </div>

        <!--
        <div class="footer">
          <b>Band gap</b>: <span class="e-struct-field" ></span>
        </div>
        -->
      </div>

      <div id="thermal-props-ov"  class="view-box" >
        <div class="title">Vibrational and thermal properties
          <img style="float: right" class="to-detail thermal-props" src="img/more.svg" />
          <div style="clear: both;"></div>
        </div>

        <div style="padding: 36px; ">
          <div class="info-fields-label">
            <span info-sys-data="heat-capacity-cv">Specific heat</span>
          </div>


          <div>
              <div id="heat-plotter" >  </div>
          </div>
          <div class="footer-heat-calc" style="text-align: center"></div>
        </div>

      </div>

    </div>

    <div style="clear: both;"></div>
    `;

    this.materialTitle= this.element.getElementsByClassName('material-title')[0];

    this.systemType= this.element.querySelector('.system-type-field');
    this.spaceGroupField = this.element.querySelector('.space-group-field');
    this.spaceGroupValue = this.element.querySelector('.space-group-value');
    this.structTypeField= this.element.querySelector('.structure-type-field');
    this.structTypeValue= this.element.querySelector('.structure-type-value');
    //this.band_gap = this.element.getElementsByClassName('e-struct-field')[0];

    //fields= this.element.getElementsByClassName('method-field');
    this.functional= this.element.querySelector('.functional-field');//fields[0];
    this.code= this.element.querySelector('.code-field');//fields[1];

    let fields= this.element.getElementsByClassName('to-detail');
    this.structureDetailBtn= fields[0];
    this.electronicStructDetailBtn= fields[2];
    this.methodologyDetailBtn= fields[1];
    this.thermalDetailBtn= fields[3];
/*
    this.elasticDetailBtn= fields[1];
    this.methodologyDetailBtn= fields[2];
    this.electronicStructDetailBtn= fields[3];
    this.thermalDetailBtn= fields[4];
    */

    this.vizBox = this.element.getElementsByClassName('viz-box')[0];
    //this.cellViewer= null;

    this.bandPlotter= null;
    this.bsCalcIdBox = this.element.getElementsByClassName('footer-bs-calc')[0];
    this.dosPlotter= null;
    this.dosCalcIdBox = this.element.getElementsByClassName('footer-dos-calc')[0];
    this.heatPlotter= null;
    this.heatCalcIdBox = this.element.querySelector('.footer-heat-calc');

    this.spinLegend = this.element.querySelector('.spin-legend');

    // For static ones
    InfoSys.addToInfoSystem(this.element);

    // Store the state of the calcs chosen on the Elec. Structure box
    this.eStructCalcs = { bs: null, dos: null};
  }


  attachAndSetEvents(element){
    element.appendChild(this.element);
    this._events();
  }


  _events() {

    this.structureDetailBtn.addEventListener( "click", (e) => {
      util.setBrowserHashPath('material', this.materialId+'/'+util.MAT_VIEW.structure);
    });

    this.electronicStructDetailBtn.addEventListener( "click", (e) => {
      util.setBrowserHashPath('material', this.materialId+'/'+util.MAT_VIEW.electronicstruct);
    });

    this.methodologyDetailBtn.addEventListener( "click", (e) => {
      util.setBrowserHashPath('material', this.materialId+'/'+util.MAT_VIEW.methodology);
    });

    this.thermalDetailBtn.addEventListener( "click", (e) => {
      util.setBrowserHashPath('material', this.materialId+'/'+util.MAT_VIEW.thermalprops);
    });

/*
    this.elasticDetailBtn.addEventListener( "click", (e) => {
      util.setBrowserHashPath('material', this.materialId+'/'+util.MAT_VIEW.elasticconst);
    });
*/


    //******* Optimize, genralize:
    //this.element.querySelectorAll('.to-detail+.'+detailsId).addEventListener( "click", (e) => {
    // util.setBrowserHashPath('material', this.materialId+'/'+detailsId);
  }


  getEStructChosenCalcs(){
    return this.eStructCalcs;
  }


  setDetailViewsListener(listener){
    this.detailViewsListener= listener;
  }

  setVisible(){
    this.element.style.display= 'block';
  }


  setMaterialData() {

    let data = DataStore.getMaterialData();
    this.materialTitle.innerHTML= util.getMaterialTitle(data);
    this.materialId = data.id;

    let isBulk = (data.system_type === 'bulk');
    this.systemType.textContent= data.system_type;
    this.structTypeField.style.display =
      (isBulk && data.structure_type !== null ? 'block' : 'none');
    this.spaceGroupField.style.display = (isBulk ? 'block' : 'none');

    if (isBulk){
      this.structTypeValue.textContent= data.structure_type;
      this.spaceGroupValue.textContent = data.space_group_number+
        ' ('+data.space_group_international_short_symbol+')';
      InfoSys.addElementToInfoSystem(this.spaceGroupValue,
        'space-group.value:'+data.space_group_number);
    }

  }


  _evaluateCalc(calc){
    let value = 0;
    if (calc.functional_type === 'GGA') value += 100;
    if (calc.has_band_structure && calc.has_dos) value += 10;
    switch (calc.code_name.trim()) {
      case 'FHI-aims': value += 3;  break;
      case 'VASP': value += 2;  break;
      case 'Quantum Espresso': value += 1;  break;
    }
    return value;
  }


  setCalcsData(markedTreeLeafs) {
    //console.log('setCalcsData: '+JSON.stringify(data));
    let calcs = DataStore.getCalculations();

    let functionalMap = new Map();
    let codeMap = new Map();
    let calcWithBS = null, calcWithDOS = null, calcWithHeat = null;
    let calcBSEvaluation = -1, calcDOSEvaluation = -1;

    for (let i = 0; i < calcs.length; i++) {

      if (functionalMap.has(calcs[i].functional_type)){
        let num= functionalMap.get(calcs[i].functional_type);
        // Because it's a number we can't use the returned value as a reference
        functionalMap.set(calcs[i].functional_type,++num);
      }else
        functionalMap.set(calcs[i].functional_type,1);

      let codeNameTrimed= calcs[i].code_name.trim();
      if (codeMap.has(codeNameTrimed)){
        let num= codeMap.get(codeNameTrimed);
        // Because it's a number we can't use the returned value as a reference
        codeMap.set(codeNameTrimed,++num);
      }else
        codeMap.set(codeNameTrimed,1);

      let calcEvaluation = this._evaluateCalc(calcs[i]);
      //console.log('calcEvaluation',calcEvaluation);

      if (calcs[i].has_band_structure && calcEvaluation > calcBSEvaluation){
        calcBSEvaluation = calcEvaluation;
        calcWithBS = calcs[i];
      }

      if (calcs[i].has_dos && calcEvaluation > calcDOSEvaluation){
        calcDOSEvaluation = calcEvaluation;
        calcWithDOS = calcs[i];
      }
      //console.log('BS DOS Evaluation',calcBSEvaluation, calcDOSEvaluation);

      if (calcWithHeat === null && calcs[i].has_thermal_properties)
        calcWithHeat = calcs[i];
    }

    if (calcWithBS !== null)  this.eStructCalcs.bs = calcWithBS.id;
    if (calcWithDOS !== null)  this.eStructCalcs.dos = calcWithDOS.id;

    let tempCalcId = null;
    if (calcWithBS !== null)  tempCalcId = calcWithBS.id;
    else if (calcWithDOS !== null)  tempCalcId = calcWithDOS.id;

    if (tempCalcId === null) markedTreeLeafs.eStruct = null; // no graph data
    else if (DataStore.isInAnyNotDisabledGroup(tempCalcId)){
      markedTreeLeafs.eStruct = DataStore.getGroupLeafId(tempCalcId);
    }else
      markedTreeLeafs.eStruct = +tempCalcId;

    if (calcWithHeat === null) markedTreeLeafs.thermalProps = null;
    else if (DataStore.isInAnyNotDisabledGroup(calcWithHeat.id)){
      markedTreeLeafs.thermalProps = DataStore.getGroupLeafId(calcWithHeat.id);
    }else
      markedTreeLeafs.thermalProps = +calcWithHeat.id;
    //console.log('Overview - thermalPropsDetailsTreeLeaf: ', markedTreeLeafs.thermalProps);

    //this.band_gap.innerHTML= util.getBandGapStatsValue(calcs);

    let functionalHTML= '';
    functionalMap.forEach((number,functional) => {
      functionalHTML+= '<span info-sys-data="functional-type.value:'+functional+
        '">'+number+' '+functional+'</span> <br> ';
    });

    this.functional.innerHTML= functionalHTML;
    InfoSys.addToInfoSystem(this.functional);

    let codeHTML= '';
    codeMap.forEach((number,codeName) => {
      codeHTML+= '<span info-sys-data="code-name.value:'+codeName+
        '">'+number+' '+codeName+'</span> <br> ';
      //codeHTML+= number+' '+codeName+' <br> ';
    });
    this.code.innerHTML= codeHTML;
    InfoSys.addToInfoSystem(this.code);


    if (calcWithBS === null && calcWithDOS === null){
      document.getElementById('e-structure-ov').style.display = 'none';
      DataStore.hasElecStructureData = false;
    }else{
      document.getElementById('e-structure-ov').style.display = 'block';
      DataStore.hasElecStructureData = true;

      if (this.bandPlotter === null){
        this.bandPlotter= new BSPlotter();
        this.bandPlotter.attach(document.getElementById('band-plotter'),undefined,316);
      }
      if (this.dosPlotter === null){
        this.dosPlotter= new DOSPlotter({left: 40, right: 16, top: 0, bottom: 30});
        this.dosPlotter.attach(document.getElementById('dos-plotter'),undefined,317);
      }

      if (calcWithBS === null){
        this.bandPlotter.setNoData();
        this.bsCalcIdBox.innerHTML = '';
      }else{
        let url = util.getMaterialCalcURL(calcWithBS.material,calcWithBS.id,
            'band_structure');
        LoadingPopup.show();
        util.serverReq(url, e => {
          if (e.target.status === 200){
            let bandStructData= JSON.parse(e.target.response).band_structure;
            this.bandPlotter.setBandStructureData(bandStructData);
            this.bsCalcIdBox.innerHTML = 'From calculation <b>'+calcWithBS.id+
              '</b><br><span style="font-size: 0.8em">('+calcWithBS.functional_type+' - '+calcWithBS.code_name+')</span>';
            if (bandStructData.segments[0].band_energies.length === 2)
              this.spinLegend.style.display = 'block';
          }
          LoadingPopup.hide();
        });
      }

      if (calcWithDOS === null){
        this.dosPlotter.setNoData();
        this.dosCalcIdBox.innerHTML = '';
      }else{
        let url = util.getMaterialCalcURL(calcWithDOS.material,calcWithDOS.id,
            'dos');
        LoadingPopup.show();
        util.serverReq(url, e => {
          if (e.target.status === 200){
            let dosData= JSON.parse(e.target.response).dos;

            this.dosPlotter.setPoints(dosData);//paintPointsLine(dosData);
            this.dosCalcIdBox.innerHTML = 'From calculation <b>'+calcWithDOS.id+
            '</b><br><span style="font-size: 0.8em">('+calcWithDOS.functional_type+' - '+calcWithDOS.code_name+')</span>';
            if (dosData.dos_values.length === 2)
              this.spinLegend.style.display = 'block';
          }
          LoadingPopup.hide();
        });
      }
    }

    if (calcWithHeat === null){
      document.getElementById('thermal-props-ov').style.display = 'none';
      DataStore.hasThermalData = false;
    }else{
      document.getElementById('thermal-props-ov').style.display = 'block';
      DataStore.hasThermalData = true;

      if (this.heatPlotter === null){
        this.heatPlotter= new HeatCapPlotter();
        this.heatPlotter.attach(document.getElementById('heat-plotter'),undefined,317);
      }

      if (calcWithHeat === null){
        this.heatPlotter.setNoData();
        this.heatCalcIdBox.innerHTML = '';
      }else{
        let url = util.getMaterialCalcURL(calcWithHeat.material, calcWithHeat.id,
            'specific_heat_cv');
        LoadingPopup.show();
        util.serverReq(url, e => {
          if (e.target.status === 200){
            let heatData= JSON.parse(e.target.response).specific_heat_cv;
            //console.log(heatData);
            this.heatPlotter.setData(heatData);
            this.heatCalcIdBox.innerHTML = 'From calculation <b>'+calcWithHeat.id+'</b>'+
            '</b> <span style="font-size: 0.8em">('+calcWithHeat.functional_type+' - '+calcWithHeat.code_name+')</span>';
          }
          LoadingPopup.hide();
        });
      }
    }

  } // setCalcsData function

}

// EXPORTS
module.exports = Overview;
