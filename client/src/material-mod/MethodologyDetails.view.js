
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  'Details' view container that shows a table with the methodology info used
  to get the material calculations.

  In the file there is a defined (class) component used as a filter widget for
  the table: FilterInGroupsComponent.
 */

"use strict";

let DetailsViewBase = require('./DetailsViewBase.js');
let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let DataStore = require('./DataStore.js');
let SortingButton = require('./SortingButton.js');


const REPOSITORY_LINK =
  'https://repository.nomad-coe.eu/NomadRepository-1.1/views/calculation.zul?pid=';

const calcTypesMap = new Map([
  ['single point', 'Single point'],
  ['GW calculation', 'GW'],
  ['geometry optimization', 'Geometry optimization'],
  ['molecular dynamics', 'Molecular dynamics'],
  ['phonon calculation', 'Phonon'],
  ['equation of state', 'Equation of state'],
  ['parameter variation', 'Parameter variation'],//['Convergence', 'convergence'],
  ['QHA calculation', 'QHA']
]);

const densityFunctionalMap = new Map([
  ['LDA', 'LDA'],
  ['GGA', 'GGA'],
  ['meta-GGA', 'meta-GGA'],
  ['hybrid-GGA', 'hybrid-GGA'],
  ['meta-hybrid-GGA', 'meta-hybrid-GGA'],
  ['HF', 'HF']
]);

const codeMap = new Map([
  ['exciting', 'exciting'],
  ['VASP', 'VASP'],
  ['FHI-aims', 'FHI-aims']
]);

const potentialMap = new Map([
  ['full all electron', 'Full potential' ],
  ['pseudopotential', 'Pseudo potential' ]
]);

const basicSetMap = new Map([
  ['numeric AOs', 'Numeric AOs' ],
  ['gaussians', 'Gaussians' ],
  ['(L)APW+lo', '(L)APW+lo'],
  ['plane waves', 'Plane waves']
]);



class FilterInGroupsComponent {

  constructor(className) {
    this.element = document.createElement('div');
    this.element.className = className;
    this.filtersOn = [];
    this.folded = true;
    this.element.innerHTML+=`
      <div>
        <div style="display: flex; justify-content: flex-end;">
          <div class="filter-groups-c-folded" >
            <span style="vertical-align: top;">Filtering &nbsp;&nbsp;</span>
          </div>
          <div class="filter-c-btn" >
             <img src="${util.IMAGE_DIR}folded.png" />
            <!--<button class="on">filter</button> -->
          </div>
        </div>
        <div class="filter-groups-c-unfolded" style="display: none">
          <table style="width: 100%">
            <thead>
            <tr>
              <th style="width: 13%;"> </th>
              <th style="width: 17%;">
                <span>Type</span>
              </th>
              <th style="width: 19%;">
                <span info-sys-data="functional-type">Density functional</span>
              </th>
              <th style="width: 13%;">
                <span info-sys-data="code-name">Code</span>
              </th>
              <th style="width: 17%;">
                <span info-sys-data="pseudopotential-type">Potential</span>
              </th>
              <th style="width: 12%;">
                <span info-sys-data="basis-set-type">Basis set</span>
              </th>
              <th style="width: 9%;"> </th>
            </tr>
            </thead>
            <tbody>
              <tr id="filter-items-row"></tr>
            </tbody>
          </table>
        </div>
      </div>
    `;
    //this.foldedPanel = this.element.querySelector('.filter-groups-c-folded');
    this.unfoldedPanel = this.element.querySelector('.filter-groups-c-unfolded');
    this.filterItemsRow = this.element.querySelector('#filter-items-row');

    this.foldBtn = this.element.querySelector('img');

    this.foldBtn.addEventListener('click', e => {
      this.folded = !this.folded;
      this.foldBtn.src = (this.folded ? util.IMAGE_DIR+'folded.png' :
        util.IMAGE_DIR+'unfolded.png');
      //this.foldedPanel.style.display = (this.folded ? 'block' : 'none');
      this.unfoldedPanel.style.display = (this.folded ? 'none' : 'block');
    });

    // Add listener for checkboxes events
    this.element.addEventListener('click', (e) => {

      if (e.target.tagName === 'INPUT'){
        let index = this.filtersOn.indexOf(e.target.value);
        if (index >= 0)  this.filtersOn.splice( index, 1 );
        else this.filtersOn.push(e.target.value);
        this.itemListener(this.filtersOn);
        //console.log('this.filtersOn',this.filtersOn);
      }
    });
  }


  addGroupsItems(calcs){
    let lCalcTypesMap = new Map();
    let lDensityFunctionalMap = new Map();
    let lCodeMap = new Map();
    let lPotentialMap = new Map();
    let lBasicSetMap = new Map();
    calcs.forEach( c => {
      if (!lCalcTypesMap.has(c.type))
        lCalcTypesMap.set(c.type, calcTypesMap.get(c.type));
      if (!lDensityFunctionalMap.has(c.functional))
        lDensityFunctionalMap.set(c.functional, densityFunctionalMap.get(c.functional));
      if (!lCodeMap.has(c.code))
        lCodeMap.set(c.code, codeMap.get(c.code));
      if (!lPotentialMap.has(c.potential))
        lPotentialMap.set(c.potential, potentialMap.get(c.potential));
      if (!lBasicSetMap.has(c.basisSet))
        lBasicSetMap.set(c.basisSet, basicSetMap.get(c.basisSet));
    });
    this.filterItemsRow.innerHTML = '<td></td>'; // calculation Id column
    this.filtersOn = [];
    this.addGroupItems(lCalcTypesMap);
    this.addGroupItems(lDensityFunctionalMap);
    this.addGroupItems(lCodeMap);
    this.addGroupItems(lPotentialMap);
    this.addGroupItems(lBasicSetMap);
    this.filterItemsRow.innerHTML += '<td></td>'; // link column
  }


  addGroupItems(groupItemsMap){
    let html = '<td>  ';
    groupItemsMap.forEach( (itemName, itemId) => {
      this.filtersOn.push(itemId);
      html += '<input type="checkbox" value="'+itemId+'" checked>'+
        '<span style="vertical-align: 20%">'+itemName+'</span> &nbsp;&nbsp; <br> ';
    });
    this.filterItemsRow.innerHTML += html+ '</td>';
  }


  setItemListener(listener){
    this.itemListener = listener;
  }
}



class MethodologyDetails extends DetailsViewBase {

  constructor() {
    super('Methodology');

    this.sortedCalcs = [];
    this.markedCalc = null;

    this.element.innerHTML+=`

      <div>
        <div class="view-box">
          <div class="title">Methodology</div>

          <div class="filter-placeholder"></div>

          <div class="dataTableWrapper"></div>
        </div>
      </div>
    `;

    // There is no this.navTreeWrapper = this.element.querySelector('.navTreeWrapper');

    this.dataTableWrapper =
        this.element.querySelector('.dataTableWrapper');

    this.dataTableWrapper.innerHTML+=`
      <table id="methodology-data">
        <thead>
        <tr>
          <th style="width: 13%;">
            <span>Calculation ID</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 17%;">
            <span>Type</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 19%;">
            <span info-sys-data="functional-type">Density functional</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 13%;">
            <span info-sys-data="code-name">Code</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 17%;">
            <span info-sys-data="pseudopotential-type">Potential</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 12%;">
            <span info-sys-data="basis-set-type">Basis set</span>
            <span class="sorting-button"></span>
          </th>
          <th style="width: 9%;">
            <span info-sys-data="basis-set-type">Link</span>

          </th>
        </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    `;

    this.tbody = this.dataTableWrapper.querySelector("tbody");
    this.moreInfoRow = document.createElement('tr'); //
    this.moreInfoRow.className= 'moreinfo';
    this.moreInfoCalcId = null;

    // filtering feature initialitation
    this.filterComponent = new FilterInGroupsComponent('meth-filter-component');
    this.element.querySelector('.filter-placeholder').
      appendChild(this.filterComponent.element);

    this.filterComponent.setItemListener( propsSel/*ected*/ => {

      let rows = this.tbody.querySelectorAll('tr');
      for (let i = 0; i < rows.length; ++i){
        let calcProps = rows[i].getAttribute('data-calc-props').split(',');
        //console.log('FILTERING calcProps: ',calcProps);
        let propsPresent = true;
        calcProps.forEach( e => {
          if (propsSel.indexOf(e) < 0) propsPresent = false;
        });

        if (propsPresent) rows[i].style.display = 'table-row';
        else rows[i].style.display = 'none';
      }
    });

    // row sorting feature initialitation
    this.sortingButtonWrappers =
      this.dataTableWrapper.querySelectorAll('.sorting-button');

    this.sortingButtons = [];

    let sortingButtonsMap = new Map([
      [ 'id', undefined ],
      [ 'type', undefined ],
      [ 'functional', undefined ],
      [ 'code', undefined ],
      [ 'potential', undefined ],
      [ 'basisSet', undefined ] ]);

    let keysIter = sortingButtonsMap.keys();
    this.sortingButtonWrappers.forEach( e => {
      let field = keysIter.next().value;
      let component = new SortingButton(field);
      e.appendChild(component.element);
      this.sortingButtons.push(component);

      component.setListener( (descendingOrder, field) => {
        //console.log(descendingOrder, field);
        this.sortingButtons.forEach( el => {
          if (el !== component) el.init();
        });
        this._sortRowsCalcDataBy(descendingOrder, field);
        this._render();
      });
    });

    // For static ones
    InfoSys.addToInfoSystem(this.element);

    this._events();
  }


  _events() {
    //super._events();

    this.dataTableWrapper.addEventListener('click', (e) => {

      let rowElement = e.target.parentElement;
      if (rowElement.className.indexOf('data-row') < 0)
        rowElement = rowElement.parentElement;

      if (rowElement.className.indexOf('data-row') >= 0){
        let id= rowElement.getAttribute('data-calc-id');

        if (this.moreInfoCalcId !== null){ // If more-info panel unfolded
          this.moreInfoRow.parentElement.removeChild(this.moreInfoRow);
          if (this.moreInfoCalcId === id){
            this.moreInfoCalcId = null;
            return;
          }
        }
        this.moreInfoCalcId = id;
        let moreInfoCalc, calcGroupType;
        this.sortedCalcs.forEach( leafId => { //console.log(leafId, id);
          if (leafId === id){
            moreInfoCalc = DataStore.getCalc(DataStore.getCalcReprIntId(leafId));
            calcGroupType = DataStore.getGroupType(leafId);

          }
        });
        this.moreInfoRow.innerHTML= ' <td></td><td colspan="7"> '+
          getHTMLCalcType(calcGroupType, moreInfoCalc)+' </td>';
        rowElement.parentElement.insertBefore(this.moreInfoRow, rowElement.nextElementSibling);
      }

    });


    function getHTMLCalcType(calcGroupType, calc){

      let result = '';
      let calcType = calcGroupType;
      if (calcGroupType === null ) calcType = calc.run_type;
      //console.log('getHTMLCalcType',calcType);
      switch (calcType){

        case calcTypesMap.get('Single point'):

        case calcTypesMap.get('Geometry optimization'):
          //console.log(calc.pseudopotential_type, calc.scf_threshold, calc.basis_set_short_name);
          result = getValueHTML('pseudopotential type',calc.pseudopotential_type) +
          getValueHTML('scf threshold',calc.scf_threshold) +
          getValueHTML('basis set short name',calc.basis_set_short_name) +
          getSmearingHTML(calc.smearing);
          break;

        case calcTypesMap.get('GW'):
          result =  getValueHTML('gw starting point',calc.gw_starting_point) +
            getValueHTML('gw type',calc.gw_type);
          break;

        case calcTypesMap.get('Equation of state'):
          result =  getValueHTML('pseudopotential type',calc.pseudopotential_type) +
            getValueHTML('basis set short name',calc.basis_set_short_name) +
            getValueHTML('scf threshold',calc.scf_threshold) +
            getSmearingHTML(calc.smearing) +
            getValueHTML('k point grid description',calc.k_point_grid_description);
          break;

        case calcTypesMap.get('Parameter variation'):
          result =  getValueHTML('pseudopotential type',calc.pseudopotential_type) +
            getValueHTML('scf threshold',calc.scf_threshold);
          break;

        case calcTypesMap.get('Phonon'):
          result =  getValueHTML('type', 'finite differences');
          break;

      }
      if (result.trim() === '') return 'NO ADDITIONAL DATA';
      else return result;
    }

  }


  updateSelection( leafIds/* Not used */ ){

    this.sortedCalcs = [];

    DataStore.getCalculations().forEach( c => {

      let calcType = c.run_type;
      if (DataStore.getGroups().has(c.id)){//leafId))
        calcType = DataStore.getGroupType(c.id);//leafId);
        if (calcType === 'convergence') calcType = 'parameter variation';
        //console.log('Group',DataStore.getGroupType(leafId));
      }

      this.sortedCalcs.push({
        id: c.id,
        type: calcType,
        functional: c.functional_type,
        code: c.code_name,
        potential: c.core_electron_treatment,
        basisSet: c.basis_set_type
      });
    });

    this.filterComponent.addGroupsItems(this.sortedCalcs);

    this._sortRowsCalcDataBy(true, 'id');
    //console.log('sortedCalcs:', this.sortedCalcs);

    this._render();
  }


  _sortRowsCalcDataBy(descendingOrder, field){

    this.sortedCalcs.sort( (a, b) => {
      if(a[field] < b[field]) return (descendingOrder ? -1 : 1);
      if(a[field] > b[field]) return (descendingOrder ? 1 : -1);
      return 0;
    });
  }


  updateMarkedLeaf(leafId){  }


  _render(){

    let html = '';
    this.sortedCalcs.forEach( rowCalcData => { //leafId => {
      //html+= getRowHtml(leafId, calc, calcType);
      html+= getRowHtml(rowCalcData);
    });
    this.tbody.innerHTML = html;

    InfoSys.addToInfoSystem(this.tbody);

    function getRowHtml(rowCalcData/*leafId, calc, calcType*/){

      let calc = DataStore.getCalc( /*DataStore.getCalcReprIntId(*/rowCalcData.id);
      let calcType = rowCalcData.type;
      let calcProps = calcType+','+calc.functional_type+','+calc.code_name+
        ','+calc.core_electron_treatment+','+calc.basis_set_type;

      let repositoryLinkHtml = '';
      if (calc.calculation_pid !== null && calc.calculation_pid !== undefined)
        repositoryLinkHtml =
          '<a href="'+REPOSITORY_LINK+calc.calculation_pid+'" target="blank"> '
          +'<img src="img/download.svg" height="20px" /> </a>';

      return  `
        <tr data-calc-id="${calc.id/*leafId*/}" data-calc-props="${calcProps}" class="data-row">
        <td>${calc.id/*leafId*/}</td>
        <td>
          <span info-sys-data="calculation-type.value:${calcType}">
          ${calcType}</span>
        </td>
        <td>
          <span info-sys-data="functional-type.value:${calc.functional_type}">
            ${calc.functional_type}</span>
          ${getOptValue(calc.functional_long_name)}
        </td>
        <td>
          <span info-sys-data="code-name.value:${calc.code_name}">
            ${calc.code_name}</span>
           ${getOptValue(calc.code_version)}
        </td>

        <td>
          <span info-sys-data="core-electron-treatment.value:${calc.core_electron_treatment}">
          ${getPotentialValue(calc.core_electron_treatment)}</span>
        </td>
        <td>
          <span info-sys-data="basis-set-type.value:${calc.basis_set_type}">
            ${calc.basis_set_type}</span>
           ${getOptValue(calc.basis_set_quality_quantifier)}
        </td>

        <td style="padding-top: 8px;padding-bottom: 4px;">${repositoryLinkHtml}
        </td>

        </tr>`;
    }

    function getOptValue(value){
      if (value === undefined || value === null)  return '';
      else return '('+value+')';
    }

    function getPotentialValue(value){
      if (value === 'pseudopotential')  return 'pseudopotential';
      else if (value === 'full all electron')  return 'full potential';
      else return value;
    }
  }

}

function getValueHTML(text,value){
  if (value === undefined || value === null)  return '';
  else return '<b>'+text+'</b>: '+value+'<br>';
}


function getSmearingHTML(value){
  let values = value.substring(1,value.length-1).split(',');
  return (values[0] === 'none' ? '' : '<b>smearing kind</b>: '+values[0]+' , ')+
         (values[1] === '0' ? '' : '<b>smearing width</b>: '+values[1]);
}



// EXPORTS
module.exports = MethodologyDetails;
