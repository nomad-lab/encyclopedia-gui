
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  'Details' view container that shows all the material info related to
  its structure.

  This container is extremely complex.

  In the file there are two defined (classes) components used in the container:
  - TreeLeafViewer: The panel on the right showing the data of the tree leaf marked
  - SummaryByFunctionalsComponent: the component (central panel, below part)
  showing a summary for the selected item on the tree (by functional)
 */

"use strict";

let DetailsViewBase = require('./DetailsViewBase.js');
let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let CalcSelectorBar = require('./CalcSelectorBar.js');
let StatsViewer = require('./StatsViewer.js');
let EquationOfStateViewer = require('./EquationOfStateViewer.js');
let DataStore = require('./DataStore.js');
let LoadingPopup = require('../common/LoadingPopup.js');



class StructureDetails extends DetailsViewBase {

  constructor() {
    super('Structure');
    this.groupsData; // the calcs groups data is necesary to be shown

    this.firstId;
    this.lastId;

    this.element.innerHTML+=`

      <div style="float: left; width: 36%;">
        <div class="view-box">
          <div class="title">Structure </div>
          <div class="viz-box" style="height: 400px; position: relative"></div>

          <div class="footer-flex-wrapper">

            <div class="fields-container">
              <div><b><span>System type</span></b>:
                <span class="struct-field" ></span>
              </div>
              <div class="structure-type-field" style="display: none">
                <b><span info-sys-data="structure-type">Structure type</span></b>:
                <span class="structure-type-value" ></span>
              </div>
              <div class="structure-prototype-field" style="display: none">
                <b><span info-sys-data="structure-prototype">Structure prototype</span></b>:
                <span class="structure-prototype-value" ></span>
              </div>
              <div class="strukturbericht-field" style="display: none">
                <b><span info-sys-data="strukturbericht">Strukturbericht designation</span></b>:
                <span class="strukturbericht-value" ></span>
              </div>
            </div>

            <div class="footer-flex" style="display: none">

              <div class="fields-container"
                style="flex-basis: 70%; border-right: 1px solid #E4E4E4; ">

                <div>
                  <b><span info-sys-data="crystal-system">Lattice</span></b>:
                  <span class="lattice-value" ></span>
                </div>
                <div>
                  <b><span info-sys-data="space-group">Space group</span></b>:
                  <span class="space-group-value" ></span>
                </div>
                <div>
                  <b><span info-sys-data="point-group">Point group</span></b>:
                  <span class="point-group-value" ></span>
                </div>
              </div>

              <div style="flex-basis: 30%; margin-left: 30px;">
                <div class="fields-container">
                  <div><b><span info-sys-data="wyckoff-position-population">Wyckoff sites</span></b></div>
                  <div class="wyckoff-sites-value"> </div>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>

      <div style="float: left; width: 36%;">
        <div class="view-box">
          <div class="title">Calculations</div>
          <div class="navTreeWrapper"></div>

          <div class="summary-title">Summary  </div>
          <div style="font-size: 0.85em; text-align: center; padding: 4px;">Based on the calculations selected above</div>

          <div class="info-fields summary-box">
          <!-- Lattice constants Cell volume, Density panel dynamically generated
            -->
          </div>
        </div>
      </div>

      <div style="float: right; width: 28%;">
        <div class="calc-specifics-box">

          <div style="padding-top: 10px; " >
            <div class="tree-leaf-title"></div>
          </div>

          <div class="tree-leaf-viewer-host"></div>

          </div>
        </div>

      </div>
    </div>
    `;

    this.navTreeWrapper =
        this.element.getElementsByClassName("navTreeWrapper")[0];

    let fields= this.element.getElementsByClassName('struct-field');
    this.systemTypeField= fields[0];
    this.structTypeField= this.element.querySelector('.structure-type-field');
    this.structTypeValue= this.element.querySelector('.structure-type-value');
    this.structPrototypeField= this.element.querySelector('.structure-prototype-field');
    this.structPrototypeValue= this.element.querySelector('.structure-prototype-value');
    this.strukturberichtField= this.element.querySelector('.strukturbericht-field');
    this.strukturberichtValue= this.element.querySelector('.strukturbericht-value');

    this.lowerBox = this.element.querySelector('.footer-flex');
    this.latticeValue= this.element.querySelector('.lattice-value');
    this.spaceGroupValue= this.element.querySelector('.space-group-value');
    this.pointGroupValue= this.element.querySelector('.point-group-value');
    this.wyckoffValue= this.element.querySelector('.wyckoff-sites-value');

    this.summaryByFunctionals = null;

    this.leafTitle = this.element.querySelector('.tree-leaf-title');

    this.summaryBox = this.element.querySelector('.summary-box');

    this.calcSpecificsBox = this.element.querySelector('.calc-specifics-box');

    this.treeLeafViewer = new TreeLeafViewer();
    this.element.querySelector('.tree-leaf-viewer-host').
      appendChild(this.treeLeafViewer.element);

    this.vizBox = this.element.querySelector('.viz-box');

    // For static ones
    InfoSys.addToInfoSystem(this.element);
  }


  setMaterialData() {
    let data = DataStore.getMaterialData();
    super.setMaterialData(data);
    this.isBulk = (data.system_type === 'bulk');

    this.systemTypeField.textContent= data.system_type;
    this.structTypeField.style.display =
      (this.isBulk && data.structure_type !== null ? 'block' : 'none');
    this.structPrototypeField.style.display =
      (this.isBulk && data.structure_prototype !== null ? 'block' : 'none');
    this.strukturberichtField.style.display =
      (this.isBulk && data.strukturbericht_designation !== null ? 'block' : 'none');

    this.lowerBox.style.display = (this.isBulk ? 'flex' : 'none');

    if (this.isBulk){
      this.structTypeValue.textContent= data.structure_type;
      this.structPrototypeValue.textContent= data.structure_prototype;
      this.strukturberichtValue.textContent= data.strukturbericht_designation;
      this.spaceGroupValue.textContent = data.space_group_number
        +' ('+data.space_group_international_short_symbol+')';
      this.pointGroupValue.textContent = data.point_group;
      this.latticeValue.textContent = data.crystal_system;

      // wyckoff processing
      let wyckoffMap = new Map();
      let valueSet= new Set();

      for (var i = 0; i < data.elements.length; i++) {
        let element = util.ELEMENTS[data.elements[i].label-1];
        if (wyckoffMap.has(element)){
          wyckoffMap.get(element).add(data.elements[i].wyckoff);
        }else {
          let newSet = new Set();
          newSet.add(data.elements[i].wyckoff);
          wyckoffMap.set(element, newSet);
        }
      }
      let wyckoffHTML= '';
      wyckoffMap.forEach((posSet, element) => {
        let firstPos = true;
        wyckoffHTML += '<tr> <td>'+element+': </td>';
        posSet.forEach( pos => {
          if (firstPos){
            firstPos = false;
            wyckoffHTML += '<td>'+pos+'</td></tr>';
          }else
            wyckoffHTML += '<tr><td> </td><td>'+pos+'</td></tr>';
        });
      });

      this.wyckoffValue.innerHTML = '<table>' + wyckoffHTML+'</table>';
    }

    InfoSys.addElementToInfoSystem(this.spaceGroupValue,
      'space-group.value:'+data.space_group_number);
    InfoSys.addElementToInfoSystem(this.latticeValue,
      'crystal-system.value:'+data.crystal_system);
    InfoSys.addElementToInfoSystem(this.pointGroupValue,
      'point-group.value:'+data.point_group);
    //InfoSys.addElementToInfoSystem(this.wyckoffValue, 'wyckoff-position-population.value:'+);
  }


  updateSelection(leafIds /*Set*/) {
    //console.log('StructureDetails.updateSelection: ', leafIds);
    if (leafIds.size > 0){
      this.summaryBox.style.visibility = 'visible';

      let summaryCalcSet = new Set();
      leafIds.forEach( leafId => {
        let calc;
        if (DataStore.getGroups().has(leafId)){
          calc = DataStore.getCalc(DataStore.getGroups().get(leafId).method_representative);
        }else
          calc = DataStore.getCalc(parseInt(leafId));
        summaryCalcSet.add(calc);
      });

      let calcMapByFunctional = getCalcMapByFunctional(summaryCalcSet);
      if (this.summaryByFunctionals === null)
        this.summaryByFunctionals =
          new SummaryByFunctionalsComponent(calcMapByFunctional, this.summaryBox);
      else
        this.summaryByFunctionals.build(calcMapByFunctional);

      let counter= 0;
      leafIds.forEach( calcId => {
        counter++;
        if (counter === 1) this.firstId = calcId;
        else if (counter === leafIds.size) this.lastId = calcId;
      });

    }else{
      this.summaryBox.style.visibility = 'hidden';
    }

    function getCalcMapByFunctional(summaryCalcSet){

      let functCalcMap = new Map();
      summaryCalcSet.forEach( calc => {
        if (functCalcMap.has(calc.functional_type)){
          functCalcMap.get(calc.functional_type).add(calc);

        }else{ // New functional
          let newFunctionalArray = new Set();
          newFunctionalArray.add(calc);
          functCalcMap.set(calc.functional_type, newFunctionalArray);
        }
      });
      //console.log('functCalcMap: ',functCalcMap);
      return functCalcMap;
    }
  }


  updateMarkedLeaf(leafId){

    if (leafId !== null){
      this.calcSpecificsBox.style.visibility = 'visible';
      //this.calcSpecificsBox.style.backgroundColor= '#FFF7EB';

      if (DataStore.getGroups().has(leafId)){
        this.leafTitle.innerHTML = leafId+
          ' ('+DataStore.getGroups().get(leafId).calcs.size+')';
      }else
        this.leafTitle.innerHTML = leafId;

    }else
      this.calcSpecificsBox.style.visibility = 'hidden';
      //this.calcSpecificsBox.style.backgroundColor= 'white';
      //this.leafTitle.innerHTML = 'NO SELECTION';

    this.treeLeafViewer.update(leafId, DataStore.getGroups().get(leafId));
  }

}


class TreeLeafViewer{

  constructor(hostClass){
    this.groupCalcs = null;
    this.element = document.createElement('div');
    this.element.innerHTML = `
    <div>

      <div class="group-components" style="display: none">
        <div style="padding: 10px 0 30px 10px; " class="eos-host">
        </div>

        <div style="padding-top: 10px; " class="calc-selector-host">
        </div>
      </div>

      <div class="info-fields">
        <div><b>Lattice constants</b></div>
        <div class="latt-constants"></div>
        <div class="volume-field"><b><span info-sys-data="cell-volume">Volume</span></b>:
          <span class="volume-value" ></span>
        </div>
        <!-- <div><b>Pressure</b>: <span class="" ></span>  </div>-->
        <div class="density-field"><b>Density</b>:
            <div class="stats-fields" >
              <span info-sys-data="mass-density">Mass density</span> =
              <span class="mass-density-value" ></span>
            </div>
            <div class="stats-fields" >
              <span info-sys-data="atomic-density">Atomic density</span> =
              <span class="atomic-density-value" ></span>
            </div>
        </div>

        <div class="energy-field"><b><span info-sys-data="energies">Energies</span></b> (code-specific)</div>
        <div class="energy-descomp"> </div>

        <div class="wyckoff-pos-calc-field" >
          <b><span info-sys-data="free-wyckoff-parameters">Wyckoff sites</span></b>
          (fractional coordinates)
          <div class="wyckoff-pos-calc-table"> </div>
        </div>

      </div>

    </div>
    `;

    this.groupComponents = this.element.querySelector('.group-components');

    this.calcSelector = new CalcSelectorBar('calc-selector-bar','60%');
    this.element.querySelector('.calc-selector-host').
      appendChild(this.calcSelector.element);

    this.lattConstantsField = this.element.querySelector('.latt-constants');
    this.volumeField = this.element.querySelector('.volume-field');
    this.volumeValue = this.element.querySelector('.volume-value');
    this.densityField = this.element.querySelector('.density-field');
    this.massDensityValue = this.element.querySelector('.mass-density-value');
    this.atomicDensityValue = this.element.querySelector('.atomic-density-value');

    this.energyField= this.element.querySelector('.energy-field');
    this.energyDescompValue= this.element.querySelector('.energy-descomp');

    this.wyckoffPosField = this.element.querySelector('.wyckoff-pos-calc-field');
    this.wyckoffPosTable = this.element.querySelector('.wyckoff-pos-calc-table');

    this.eosViewer = new EquationOfStateViewer();
    this.eosViewer.attach(this.element.querySelector('.eos-host'),320, 280);

     this.eosViewer.setClickPointListener( calc => {
      this.groupCalcUpdate(calc+'');
    });

    InfoSys.addToInfoSystem(this.element);

    this._events();
  }


  _events() {

    this.calcSelector.setPrevListener(e => {
      //console.log('calcSelectorPrevBtn');
      if (this.groupIndex > 0){
        this.groupCalcUpdate(this.groupCalcs[--this.groupIndex]+'');
        return this.groupIndex === 0; // the first
      }
    });

    this.calcSelector.setNextListener( e => {
      //console.log('calcSelectorNextBtn');
      if (this.groupIndex < this.groupCalcs.length-1){
        this.groupCalcUpdate(this.groupCalcs[++this.groupIndex]+'');
        return this.groupIndex === this.groupCalcs.length-1; // the last
      }
    });

  }


  update(leafId, groupData){

    this.representative = leafId;
    this.isGroup = false;
    //console.log('SET: ',groupData);
    if (groupData !== undefined){

      this.groupCalcs = Array.from(groupData.calcs);
      this.groupIndex = this.groupCalcs.indexOf(
        DataStore.getCalcReprIntId(this.representative));

      this.isGroup = true;
      this.groupComponents.style.display = 'block';

      this.eosViewer.clear();
      let pointsX = [], pointsY = [];//, calcIds = [];

      this.groupCalcs.forEach( calcId => {
        //calcIds.push(calcId);
        let calc = DataStore.getCalc(calcId);
        //console.log('CALC: ', calcId, calc);
        pointsX.push(calc.cell_volume/1e-30);
        let yVal = 555; // Trick: signal value
        calc.energy.forEach( e => {
          if (e.e_kind === 'Total E'){
            yVal = e.e_val/1.602176565e-19;
          }
        });
        pointsY.push(yVal);
      });

      // Method re
      let mReprCalc = DataStore.getCalc(groupData.method_representative);
      //console.log('mReprCalc: ', mReprCalc);
      let eZero;
      mReprCalc.energy.forEach( e => {
        if (e.e_kind === 'Total E')
          eZero = e.e_val/1.602176565e-19;
      });
      //console.log('CALC: ', pointsX, pointsY, groupCalcsForChart);
      this.eosViewer.draw(pointsX, pointsY, this.groupCalcs, eZero);

    }else{
      this.groupComponents.style.display = 'none';
    }
    this.groupCalcUpdate(leafId);
  }


  groupCalcUpdate(leafId){

    if (leafId !== null){

      let calcId = DataStore.getCalcReprIntId(leafId);
      if (this.groupCalcs !== null){
        this.groupIndex = this.groupCalcs.indexOf(calcId);
        if (this.groupIndex >= 0){
          let t = calcId+' ('+(this.groupIndex+1)+'/'+this.groupCalcs.length+')';
          this.calcSelector.setState(t, this.groupIndex === 0,
            this.groupIndex === this.groupCalcs.length-1);

          this.eosViewer.selectCalc(calcId);
        }
      }

      //console.log('LEAF UPDATE', calc.id, this.groupIndex);
      let is2Dsystem = (DataStore.getMaterialData().system_type === '2D');
      let isBulk = (DataStore.getMaterialData().system_type === 'bulk');
      let calc = DataStore.getCalc(calcId);
      let lattParams= util.getNumberArray(calc.lattice_parameters);

      let lattCFieldHTML = ((is2Dsystem  || isBulk) ?
        `<div>b = ${util.m2Angstrom(lattParams[1])}</div>` : '');
      lattCFieldHTML += (isBulk ?
        `<div>c = ${util.m2Angstrom(lattParams[2])}</div>` : '');

      let lattBetaGammaFieldHTML = ((is2Dsystem  || isBulk) ?
          `<div>&alpha; = ${util.rad2degree(lattParams[3])}</div>` : '');

      lattBetaGammaFieldHTML += (isBulk ?
          `<div>&beta; = ${util.rad2degree(lattParams[4])}</div>
          <div>&gamma; = ${util.rad2degree(lattParams[5])}</div>` : '');

      this.lattConstantsField.innerHTML= `
        <div style="float: left; ">
          <div>a = ${util.m2Angstrom(lattParams[0])}</div>
          ${lattCFieldHTML}
        </div>
        <div style="float: left; padding-left: 40px;">
          ${lattBetaGammaFieldHTML}
        </div>
        <div style="clear: both;padding: 0"></div>
      `;

      this.densityField.style.display = (isBulk ? 'block' : 'none');
      this.volumeField.style.display = (isBulk ? 'block' : 'none');
      if (isBulk){ // bulk type
        this.volumeValue.innerHTML= util.m3ToAngstrom3(calc.cell_volume);
      //this.pressureCalcField.textContent= calc.pressure;
        this.atomicDensityValue.innerHTML= util.toAngstromMinus3(calc.atomic_density);
        this.massDensityValue.innerHTML= calc.mass_density.toFixed(1)+' kg/m<sup>3</sup>';
      }

      let r= util.serverReq(util.getCalcEnergiesURL(DataStore.getMaterialData().id, calc.id), () => {
        let value = false;
        if (r.status === 200){
          let energies= JSON.parse(r.response).results;
          for (var i = 0; i < energies.length; i++)
            if (energies[i].e_kind === 'Total E'){
              value = true;
              this.energyDescompValue.innerHTML =
                '<div>Total E = &nbsp; '+util.J2eV(energies[i].e_val)+' eV</div>';
            }
        }
        this.energyField.style.display = (value ? 'block' : 'none');
        this.energyDescompValue.style.display = (value ? 'block' : 'none');
      });

      let thereIsWyckoffData =
        (DataStore.getMaterialData().has_free_wyckoff_parameters
          && calc.wyckoff_groups_json.length > 0);

      this.wyckoffPosField.style.display = (thereIsWyckoffData ? 'block' : 'none');

      if (thereIsWyckoffData){
        let wyckoffMap = new Map(); // Map(element, Array of pairArray[w-pos, coor])
        calc.wyckoff_groups_json.forEach( d => {
          // Only entries having items in .variables are included
          if (  Object.keys(d.variables).length !== 0 ){

            let varsHtml = '';
            //d.variables.forEach( v => { varsHtml += '<p>'+v+'</p>'; } );
            for (let v in d.variables) {
              varsHtml += ''+v+' = '+d.variables[v].toFixed(2)+'<br>';
            }

            let wyckoffVarsPair = [];
            wyckoffVarsPair.push(d.wyckoff_letter);
            wyckoffVarsPair.push(varsHtml);

            if (wyckoffMap.has(d.element)){
              wyckoffMap.get(d.element).push(wyckoffVarsPair);
            }else {
              wyckoffMap.set(d.element, [wyckoffVarsPair]);
            }
          }

        });
        //console.log('wyckoffMap', wyckoffMap);

        let wyckoffHTML= '';
        wyckoffMap.forEach((posSet, element) => {

          posSet.sort( (a, b) => {
            return (a[0] > b[0] ? 1 : -1);
          });

          let firstPos = true;
          wyckoffHTML += '<tr > <td style="width: 30%;">'+element+' </td>';
          posSet.forEach( pos => {
            if (firstPos){
              firstPos = false;
              wyckoffHTML += '<td style="width: 30%; ">'+pos[0]+'</td><td style="width: 40%;">'+pos[1]+'</td></tr>';
            }else
              wyckoffHTML += '<tr><td> </td><td>'+pos[0]+'</td><td>'+pos[1]+'</td></tr>';
          });
        });

        this.wyckoffPosTable.innerHTML = '<table id="calc-wyckoff">' + wyckoffHTML+'</table>';

/*
        for (var i = 0; i < data.elements.length; i++) {
          let element = util.ELEMENTS[data.elements[i].label-1];
          if (wyckoffMap.has(element)){
            wyckoffMap.get(element).add(data.elements[i].wyckoff);
          }else {
            let newSet = new Set();
            newSet.add(data.elements[i].wyckoff);
            wyckoffMap.set(element, newSet);
          }
        }
        */

      }


    }else{
      console.log('THIS dOESNT BE REACHED');
    }
  }

}


class SummaryByFunctionalsComponent{

  constructor(calcMapByFunctional, hostElement){
    this.calcMapByFunctional = calcMapByFunctional;
    this.hostElement = hostElement;
    this.graphTrigger = null;
    this.viewType = 'text';
    this.functional = null;
    this.hostElement.innerHTML+=`
      <div style="float: left" >
        <svg xmlns="http://www.w3.org/2000/svg" class="chart-tab"
          viewBox="0 0 15 15" width="15" height="15" style="fill: #c7c7c7;">
            <rect x="0" y="0"  width="2" height="15" />
            <rect   x="3" y="5"  width="1.8" height="7"  />
            <rect  x="6" y="3"  width="1.8" height="9"  />
            <rect   x="9" y="6"  width="1.8" height="6"  />
            <rect  x="12" y="2"  width="1.8" height="10"  />
            <rect x="2" y="13"   width="13" height="2" />
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" class="text-tab"
          viewBox="0 0 15 15" width="15" height="15" style="fill: #777;">
            <rect x="0" y="1"   width="15" height="2.5" />
            <rect   x="0" y="6"  width="15" height="2.5"  />
            <rect  x="0" y="11"  width="15" height="2.5"  />
        </svg>
      </div>

      <div class="functional-tabs" style="float: right">
      </div>

      <div style="clear: both;"></div>

      <div class="content-placeholder" >

        <div style="display: block" class="text-panel" >
          <div><b>Lattice constants</b>:
            <div class="stats-fields latt-constants-field" >
            </div>
          </div>
          <div class="volume-field"><b><span info-sys-data="cell-volume">Volume</span></b> (&#197;<sup>3</sup>):
            <div class="stats-fields volume-value" > </div>
          </div>
          <div class="density-field"><b>Density</b> :
            <div >
              <div class="stats-fields" >
                <span info-sys-data="mass-density">Mass density</span> (kg/m<sup>3</sup>) =
                <span class="mass-density-value" ></span>
              </div>
              <div class="stats-fields" >
                <span info-sys-data="atomic-density">Atomic density</span> (&#197;<sup>-3</sup>) =
                <span class="atomic-density-value" ></span>
              </div>
            </div>
          </div>
        </div>

        <div style="display:none" class="chart-panel" >
          <div class="charts-placeholder" > </div>
          <div class="charts-selector" >

          </div>
        </div>

      </div>
    `;
    this.chartTab = this.hostElement.querySelector('.chart-tab');
    this.textTab = this.hostElement.querySelector('.text-tab');
    this.functionalTabs = this.hostElement.querySelector('.functional-tabs');
    this.chartPanel = this.hostElement.querySelector('.chart-panel');
    this.textPanel = this.hostElement.querySelector('.text-panel');
    this.lattConstantsField = this.hostElement.querySelector('.latt-constants-field');
    this.volumeField = this.hostElement.querySelector('.volume-field');
    this.volumeFieldValue = this.hostElement.querySelector('.volume-value');
    this.densityField = this.hostElement.querySelector('.density-field');
    this.massDensityValue = this.hostElement.querySelector('.mass-density-value');
    this.atomicDensityValue = this.hostElement.querySelector('.atomic-density-value');


    this.calcMapByFunctional.forEach( (calcs, functionalName) =>{
      this.functionalTabs.innerHTML +=
        '<span class="tab" data-tab="'+functionalName+'">'+functionalName+'</span>';
    });


    this.statsViewer = new StatsViewer();
    let chartsPlaceholder = this.hostElement.querySelector('.charts-placeholder');
    this.statsViewer.attach(chartsPlaceholder, 350, 200);

    this.chartsSelector = this.hostElement.querySelector('.charts-selector');

    this.build(calcMapByFunctional);

    this.chartTab.addEventListener( "click", e => {
      this.chartTab.style.fill = '#777';
      this.viewType = 'chart';
      this.textTab.style.fill = '#c7c7c7';
      this.chartPanel.style.display = 'block';
      this.textPanel.style.display = 'none';
    });

    this.textTab.addEventListener( "click", e => {
      this.textTab.style.fill = '#777';
      this.viewType = 'text';
      this.chartTab.style.fill = '#c7c7c7';
      this.textPanel.style.display = 'block';
      this.chartPanel.style.display = 'none';
    });

    this.functionalTabs.addEventListener( "click", e => {
      if (e.target.className === 'tab'){
        this.statsViewer.clear();
        this.functionalTabs.querySelector('[data-tab="'+this.functional+'"]')
          .className = 'tab';
        this.functional = e.target.getAttribute('data-tab');
        this.functionalTabs.querySelector('[data-tab="'+this.functional+'"]')
          .className = 'tab-selected';
        this._setData();
      }
    });

    this.chartsSelector.addEventListener( "click", e => {
      if (e.target.className.indexOf('quantity') === 0){
        this.statsViewer.clear();
        let quantity = e.target.getAttribute('data-quantity');
        let stats = this.functionalQuantityMap.get(this.functional).get(quantity);
        this.statsViewer.drawPoints(stats.data, stats.label, stats.min, stats.max);

        this.chartsSelector.querySelector('.quantity-selected').className = 'quantity';
        e.target.className = 'quantity-selected';
      }
    });

  }

  _setData(){
    let is2Dsystem = (DataStore.getMaterialData().system_type === '2D');
    let isBulk = (DataStore.getMaterialData().system_type === 'bulk');
    let statsMap = this.functionalQuantityMap.get(this.functional);

    let lattCFieldHTML = ((is2Dsystem || isBulk) ?
      `<div>b (&#197;) = ${statsMap.get('lattice_b').html}</div>` : '');

    lattCFieldHTML += (isBulk ?
      `<div>c (&#197;) = ${statsMap.get('lattice_c').html}</div>` : '');
    // Set text data
    this.lattConstantsField.innerHTML = `
      <div style="float: left; ">
        <div>a (&#197;) = ${statsMap.get('lattice_a').html}</div>
        ${lattCFieldHTML}
      </div>
      <div style="float: left; padding-left: 40px;">
        ${util.getLatticeAnglesValues(
          this.calcMapByFunctional.get(this.functional), is2Dsystem, isBulk)}
      </div>
      <div style="clear: both;padding: 0"></div>
      `;

    let chartSelectorHTML= `
    <span class="quantity-selected" data-quantity="lattice_a">a</span>
    `;

    if (is2Dsystem || isBulk)
      chartSelectorHTML += `<span class="quantity" data-quantity="lattice_b">b</span>`;

      //console.log('statsMap', statsMap, statsMap.get('volume'));
    this.densityField.style.display = (isBulk ? 'block' : 'none');
    this.volumeField.style.display = (isBulk ? 'block' : 'none');
    if (isBulk){ // bulk type
      this.volumeFieldValue.innerHTML = statsMap.get('volume').html;
      this.massDensityValue.innerHTML = statsMap.get('mass_density').html;
      this.atomicDensityValue.innerHTML = statsMap.get('atomic_density').html;
      chartSelectorHTML += `
        <span class="quantity" data-quantity="lattice_c">c</span>
        <span class="quantity" data-quantity="volume">volume</span>
        <span class="quantity" data-quantity="mass_density">mass density</span>
        <span class="quantity" data-quantity="atomic_density">atomic density</span>
      `;
    }

    this.chartsSelector.innerHTML = chartSelectorHTML;

    // Set Charts data
    let stats = statsMap.get('lattice_a');
    this.statsViewer.drawPoints(stats.data, stats.label, stats.min, stats.max);
  }



  build(calcMapByFunctional){
    this.calcMapByFunctional = calcMapByFunctional;
    this.graphTrigger = null;
    this.statsViewer.clear();
    //this.functional = null;
    //this.functionalTabs.innerHTML = '';
    this.unfoldedElement = null;
    this.functionalQuantityMap = new Map();

    // Hide and deselect all the tabs before knowing the active ones
    for (var i = 0; i < this.functionalTabs.children.length; i++) {
      //console.log('functionalTabs',this.functionalTabs.children[i]);
      this.functionalTabs.children[i].style.display = 'none';
      this.functionalTabs.children[i].className = 'tab';
    }

    // For each active functional
    this.calcMapByFunctional.forEach( (calcs, functionalName) =>{
      let statsMap = util.getQuantityStatsMap(calcs);
      this.functionalQuantityMap.set(functionalName, statsMap);
      //show the active ones
      this.functionalTabs.querySelector('[data-tab="'+functionalName+'"]')
          .style.display = 'inline';
    });

    // Select the selected functional
    let functionals = Array.from( this.calcMapByFunctional.keys() );
    // If there isn't selected functional or the current functional is not active
    if (this.functional === null || functionals.indexOf(this.functional) < 0)
      this.functional = functionals[0]; // the first one is selected
    this._setData();
    this.functionalTabs.querySelector('[data-tab="'+this.functional+'"]')
      .className = 'tab-selected';

    InfoSys.addToInfoSystem(this.hostElement);
  }

}


// EXPORTS
module.exports = StructureDetails;
