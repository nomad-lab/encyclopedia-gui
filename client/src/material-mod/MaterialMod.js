
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
  This file implements the Material Module of the application.
  It's a UI component container that displays the selected material information.
  It's complex because of the amount and diversity of material info available.

  In this file other two inner components of the module are implemented
  (by convenience):
  StructureViewerWrapper and DropDown (used exclusively in StructureViewerWrapper)
 */


let svg = require('../common/SVG.js');
let util = require('../common/util.js');
let NavTree = require('./NavTree.js');
let Overview = require('./Overview.view.js');
let StructureDetails = require('./StructureDetails.view.js');
let ElectronicStructDetails = require('./ElectronicStructDetails.view.js');
let MethodologyDetails = require('./MethodologyDetails.view.js');
let ThermalPropsDetails = require('./ThermalPropsDetails.view.js');
let ElasticConstDetails = require('./ElasticConstDetails.view.js');
let DataStore = require('./DataStore.js');
let LoadingPopup = require('../common/LoadingPopup.js');


// Store material data at this level (material model) ?
let materialData = null, materialCalcsData = null;

// Store the default marked tree leafs
let markedTreeLeafs = { eStruct: null, thermalProps: null };


class MaterialMod{

  constructor(){
    this.element = document.createElement('div');
    this.element.setAttribute("id",'material-module');

    this.overview = new Overview();
    this.overview.attachAndSetEvents(this.element);

    this.navTree= new NavTree();

    this.structureViewer = null;

    // They are created hidden
    // Structure detail section init
    this.structureDetails = new StructureDetails();
    this.structureDetails.attachAndSetEvents(this.element);

    // Electronic structure detail section init
    this.electronicStructDetails = new ElectronicStructDetails();
    this.electronicStructDetails.attachAndSetEvents(this.element);

    // Methodology detail section init
    this.methodologyDetails = new MethodologyDetails();
    this.methodologyDetails.attachAndSetEvents(this.element);

    this.thermalDetails = new ThermalPropsDetails();
    this.thermalDetails.attachAndSetEvents(this.element);

    this.elasticDetails = new ElasticConstDetails();
    this.elasticDetails.attachAndSetEvents(this.element);

    this.currentDetailView= null;
  }


  setMaterialView(data){
    if (materialData === null || materialData.id !== parseInt(data.id) ){
      this._loadMaterial(data.id, data.view);
      // Reset the checkboxes to the initial checked value (new material)
      if (this.structureViewer !== null){
        this.structureViewer.axisCheckbox.checked = true;
        this.structureViewer.bondsCheckbox.checked = true;
      }

    }else{
      this._setView(data.view);
      document.querySelector('title').innerHTML = 'NOMAD Encyclopedia - '+
        util.getMaterialTitle(DataStore.getMaterialData(), false);
    }
  }


  getCurrentPageStatus(){

    let tempeStructCalcs = null;
    if (this.currentDetailView === null){
      tempeStructCalcs = this.overview.getEStructChosenCalcs();
    }

    return {
      pageId: this.currentDetailViewId,
      markedLeaf: this.navTree.getMarkedLeaf(),
      eStructCalcs: tempeStructCalcs
    };
  }


  _setView(view){
    //console.log('FFFFF setMaterialView: '+view);
    // Hide the current view
    if (this.currentDetailView === null)
      this.overview.element.style.display= 'none';
    else
      this.currentDetailView.element.style.display= 'none';

    if (typeof view === 'undefined'){ // Overview view
      this.currentDetailView = null;
      this.currentDetailViewId = null;
      this.overview.setVisible();//this.overview.element.style.display= 'block';
      this.navTree.setLeafMarkedListener(undefined);
      this._setCellViewer(this.overview.vizBox);
    }else{
      this.currentDetailViewId = view;
      this._setDetailView(util.MAT_VIEW[view]);
    }
  }


  _setCellViewer(hostElement){
    //console.log('_setCellViewer', this.structureViewer,materialData);
    if (this.structureViewer === null){
      this.structureViewer = new StructureViewerWrapper(hostElement);//CellViewer(hostElement);//
      if (materialData !== null){ // Case: landing at e. structure details page
        this.structureViewer.load(util.getCellDataForViewer(materialData));
        this.structureViewer.setMaterialId(materialData.id);
      }
    }else
      this.structureViewer.changeHostElement(hostElement);
  }


  _setDetailView(view) {
//    console.log('FFFFF _setDetailView: '+view);

    if (view === util.MAT_VIEW.structure){
      this.currentDetailView= this.structureDetails;
      this.navTree.showCalcsGraphDataAvalability(false);
      this.navTree.setHeight(250);
      this.navTree.setMarkedLeafIfNoneMarked(null); // Set the first leaf marked

    }else if (view === util.MAT_VIEW.electronicstruct){
      this.currentDetailView= this.electronicStructDetails;
      this.navTree.showCalcsGraphDataAvalability(true);
      this.navTree.setHeight(400);
      this.navTree.setMarkedLeafIfNoneMarked(markedTreeLeafs.eStruct);

    }else if (view === util.MAT_VIEW.methodology){ // Methodology
      this.currentDetailView = this.methodologyDetails;

    }else  if (view === util.MAT_VIEW.thermalprops){ // Thermal properties
      this.currentDetailView= this.thermalDetails;
      this.navTree.showCalcsGraphDataAvalability(true);
      this.navTree.setHeight(600);
      this.navTree.setMarkedLeafIfNoneMarked(markedTreeLeafs.thermalProps);
    }

    /* To be implemented
    else{ // Elastic constants
      this.currentDetailView = this.elasticDetails;
      this.navTree.showCalcsGraphDataAvalability(false);
      this.navTree.setHeight(600);
      this.navTree.setMarkedLeafIfNoneMarked(null);
    }*/

    this.currentDetailView.setVisible();//this.currentDetailView.element.style.display= 'block';

    if (view === util.MAT_VIEW.structure)
      this._setCellViewer(this.structureDetails.vizBox);

    //this.currentDetailView.setMaterialData(materialData); //// WHY DOES  It do always?
    if (view !== util.MAT_VIEW.methodology)
      this.currentDetailView.attachNavTree(this.navTree);

    this.currentDetailView.updateSelection(this.navTree.getTreeSelectedCalcs());

    this.currentDetailView.updateMarkedLeaf(this.navTree.getMarkedLeaf());

    this.navTree.setTreeSelectionListener( leafIds => {
      this.currentDetailView.updateSelection(leafIds);
    });
    this.navTree.setLeafMarkedListener( leafId => {
      this.currentDetailView.updateMarkedLeaf(leafId);
    });
  }


  _loadMaterial(matId,view){

    this.overview.element.style.visibility= 'hidden';
    LoadingPopup.show();
    util.serverReq(util.getMaterialURL(matId), e1 => {

      materialData= JSON.parse(e1.target.response);
      util.materialId = materialData.id;

      if (e1.target.status === 200){

        util.serverReq(util.getMaterialXsURL('elements',matId), e2 => {
          materialData.elements= JSON.parse(e2.target.response).results;

          util.serverReq(util.getMaterialXsURL('cells',matId), e3 => {
            let cells= JSON.parse(e3.target.response).results;
            if (!cells[0].is_primitive) materialData.cell= cells[0];
            else materialData.cell= cells[1];

            DataStore.setMaterialData(materialData);
            //console.log("CELLS: "+JSON.stringify(materialData.cell));
            document.querySelector('title').innerHTML =
              'NOMAD Encyclopedia - '+util.getMaterialTitle(materialData, false);
            this.overview.setMaterialData();
            this.structureDetails.setMaterialData();
            this.electronicStructDetails.setMaterialData();
            this.methodologyDetails.setMaterialData();
            this.thermalDetails.setMaterialData();
            this.elasticDetails.setMaterialData();
            //console.log("MATDATA LOADED: ");
            if (this.structureViewer !== null)
              this.structureViewer.load(util.getCellDataForViewer(materialData));

            util.serverReq(util.getMaterialXsURL('calculations',matId), e4 => {
              DataStore.setCalculations(JSON.parse(e4.target.response).results);

              util.serverReq(util.getMaterialXsURL('groups', matId), e5 => {
                DataStore.setGroups(JSON.parse(e5.target.response).groups);

                let name = (materialData.material_name === null ?
                  materialData.formula : materialData.material_name);
                this.navTree.build(name);
                this.overview.setCalcsData(markedTreeLeafs);
                this.navTree.selectAll();
                //console.log('MaterialMod - thermalPropsDetailsTreeLeaf: ', markedTreeLeafs.thermalProps);

                this._setView(view);
                this.overview.element.style.visibility= 'visible';
                LoadingPopup.hide();
              });
            });
          });
        });

      }else{ // Error - First request
         }

    });
  }
} // class MaterialMod


// Wrapper the structure viewer to be properly integrated
// on the UI components showing it
class StructureViewerWrapper{

  constructor(hostElement){
    this.hostElement = hostElement;

    this.viewer = new StructureViewer(hostElement);
    //Get the focus -> this.viewer.renderer.domElement.setAttribute('tabindex', '0');

    this.legendElement = document.createElement('div');
    this.legendElement.setAttribute('class', 'element-labels');
    this.legendElement.setAttribute('style', 'position: absolute; bottom: 50px; right: 0');
    this.hostElement.appendChild(this.legendElement);

    this.footerElement = document.createElement('div');
    this.footerElement.setAttribute('class', 'structure-viewer-legend');
    this.hostElement.appendChild(this.footerElement);
    this.footerElement.innerHTML = `

    <div style="float: left; padding-right: 12px" >
      <input type="checkbox" class="show-axis" checked> Show axis
    </div>

    <div style="float: left; padding-right: 18px" >
      <input type="checkbox" class="show-bonds" checked> Show bonds
    </div>

    <div style="float: left; position:relative;" >
      <img class="view-reset" style="cursor: pointer;" height="18px"
        src="${util.IMAGE_DIR}reset.svg" />
      <div class="view-reset-tooltip" > Set original view </div>
    </div>

    <!--
    <div class="view-reset-tooltip" style="float: left; display: none; font-size: 0.8em;" >
      Set original <br> &nbsp; view
    </div>
    -->

    <div class="vr-download" style="float: right"> </div>

    <div style="clear: both;"></div>
    `;

    this.axisCheckbox = this.footerElement.querySelector('.show-axis');
    this.axisCheckbox.addEventListener('click', e => {
      this.viewer.toggleLatticeParameters(this.axisCheckbox.checked);
    });

    this.bondsCheckbox = this.footerElement.querySelector('.show-bonds');
    this.bondsCheckbox.addEventListener('click', e => {
      this.viewer.toggleBonds(this.bondsCheckbox.checked);
    });

    this.labelsContainer = this.hostElement.querySelector('.element-labels');

    this.vrLinksContainer = this.footerElement.querySelector('.vr-download');
    this.vrDropDown = new DropDown();
    this.vrLinksContainer.appendChild(this.vrDropDown.element);

    let resetButton = this.hostElement.querySelector('.view-reset');
    resetButton.addEventListener('click', e =>  this.viewer.reset() );

    resetButton.addEventListener('mouseover', e => {
      this.hostElement.querySelector('.view-reset-tooltip').style.display = 'block';
    });
    resetButton.addEventListener('mouseout', e => {
      this.hostElement.querySelector('.view-reset-tooltip').style.display = 'none';
    });
  }

  load(data){
    this.viewer.load(data);
    this.createElementLegend();
  }

  setMaterialId(id){
    this.vrDropDown.setMaterialId(id);
  }

  changeHostElement(hostElement){
    if (this.hostElement !== hostElement){
      this.hostElement.removeChild(this.legendElement);
      this.hostElement.removeChild(this.footerElement);
      this.hostElement = hostElement;
      this.viewer.changeHostElement(hostElement);
      this.hostElement.appendChild(this.legendElement);
      this.hostElement.appendChild(this.footerElement);
    }
  }


  createElementLegend() {
    // Empty the old legend
    this.labelsContainer.innerHTML = '';

    let elements = this.viewer.elements;
    // Create a list of elements
    let elementArray = [];
    for (let property in elements) {
      if (elements.hasOwnProperty(property))
        elementArray.push([property, elements[property][0], elements[property][1]]);
    }

    // Sort by name
    elementArray.sort(function (a, b) {
      if (a[0] < b[0]) return -1;
      if (a[0] > b[0]) return 1;
      return 0;
    });

    let svgElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svgElement.setAttribute("width", 50);
    svgElement.setAttribute("height", elementArray.length*25);

    this.labelsContainer.appendChild(svgElement);

    for (let i = 0; i < elementArray.length; ++i) {
      let elementName = elementArray[i][0];
      //let elementColor = "#" + elementArray[i][1].toString(16); Lauri's bugfixing feb-2017
      let elementColor = elementArray[i][1].toString(16);
      let nZeros = 6 - elementColor.length;
      let prefix = "#" + Array(nZeros + 1).join("0");
      elementColor = prefix + elementColor;

      //let elementRadius = 25 * elementArray[i][2];
      //elementRadius = Math.min(100, elementRadius);
      //elementRadius = Math.max(30, elementRadius);
      svg.addCircle(svgElement, 10, 25*i+12, 8,  elementColor, "#777", 1);
      svg.addText(svgElement,  24, 25*i+18, elementName, 'start', 'structure-viewer-legend-labels');
    }
  }
} // class StructureViewerWrapper


// used exclusively in StructureViewerWrapper
class DropDown{

  constructor(materialId){
    this.folded = true;
    this.element = document.createElement('div');
    //this.element.className = className;

    this.element.innerHTML+=`
      <div >
        <span style=" vertical-align: 30%; ">Virtual Reality files</span>
        <img style="cursor: pointer" src="${util.IMAGE_DIR}folded.png" />
      </div>

      <div class="vr-download-panel" style="position: relative; display: none">

      </div>
    `;

    // Focus related properties (in order to hide the box when the user click out)
    this.element.tabIndex = '0'; // enabled the support of focusing
    this.element.style.outline = 'none'; // The outline is not shown when it gains the focus

    this.foldingPanel = this.element.querySelector('.vr-download-panel');
    this.foldBtn = this.element.querySelector('img');

    this.foldBtn.addEventListener('click', e => {
      this.folded = !this.folded;
      this.foldBtn.src = (this.folded ? util.IMAGE_DIR+'folded.png' :
        util.IMAGE_DIR+'unfolded.png');
      //this.foldBtn.className = (this.folded ? 'on' : 'off');
      this.foldingPanel.style.display = (this.folded ? 'none' : 'block');
    });

    this.element.addEventListener('blur' , e => {
      this.folded = true;
      this.foldBtn.src = util.IMAGE_DIR+'folded.png';
      this.foldingPanel.style.display = 'none';
    });

      //this.cellViewer.toggleLatticeParameters(false);
  }

  setMaterialId(id){
    this.foldingPanel.innerHTML = `
    <div class="vr-download-panel-unfolded" style="width: 210px;">
      <div style="padding: 5px; ">
        <a href="http://nomad.srv.lrz.de/cgi-bin/NOMAD/material?${id}">Get VR file</a>
      </div>
      <br>
      <div style="padding-bottom: 5px; ">Visualization tools for specific devices:</div>

      <div style="padding: 5px; ">
        <a href="http://nomad.srv.lrz.de/NOMAD/NOMADViveT-Setup.exe">HTC Vive</a>
      </div>
      <!--
      <div style="padding: 5px; ">
        <a href="http://nomad.srv.lrz.de/NOMAD/NOMADGearvrT.apk">Samsung GearVR</a>
      </div>
      -->
      <div style="padding: 5px; ">
        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.lrz.nomadvr">Google Cardboard</a>
      </div>

    </div>
    `
  }

} // class DropDown


// EXPORTS
module.exports = MaterialMod;
