
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  Base class from which other main 'Details' view containers inherit.
  Every of these containers shows all the material info related to a type of info:
  Structure, Electronic Structure, etc
 */

"use strict";

let util = require('../common/util.js');
let DataStore = require('./DataStore.js');


class DetailsViewBase {

  constructor(domId) {
    this.element = document.createElement('div');
    this.element.setAttribute('id',domId);
    this.gotoResultsListener= null;
    this.gotoOverviewListener= null;
    this.element.innerHTML= '<div class="material-title"></div>';
    this.element.style.display= 'none';
  }


  attachAndSetEvents(element){
    element.appendChild(this.element);
    this.materialTitle= this.element.querySelector('.material-title');
  }


  attachNavTree(navTree){
    navTree.attach(this.navTreeWrapper);
  }


  setVisible(){
    this.element.style.display= 'block';
  }


  setMaterialData() {
    this.materialTitle.innerHTML= util.getMaterialTitle(DataStore.getMaterialData());
  }


  updateCalcs(calcs){
  }


  updateMarkedCalc(calc){
  }

}

// EXPORTS
module.exports = DetailsViewBase;
