/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  Implements the Band Structure plotter.
  It's used to show either the regular Band Structure data or
  the Phonon Dispersion.
 */


"use strict";

let InteractivePlotterBase = require('../common/InteractivePlotterBase.js');
let svg = require('../common/SVG.js');
let util = require('../common/util.js');

const MULTIPLO1 = 1.602176565e-19;
const MULTIPLO2 = 1/5.034117012222e22;


class BSPlotter extends InteractivePlotterBase{

  constructor() {
    super({left: 55, right: 5, top: 0, bottom: 30});
    this.phononMode = false;
    this.factor = MULTIPLO1;
  }


  setPhononMode(){
    this.phononMode = true;
    this.factor = MULTIPLO2;
    this.outOfRangeColorActivated = false;
  }


  // detach if necessary and attach
  attach(element, width, height){
    super.attach(element, width, height);
  }


  getTopAndLowestPoints(bandStructData){
    let bandMax = -10000;
    let bandMin = 10000;
    for (let i = 0; i < bandStructData.length; i++) { // Per segment
      // spin1 - per band energy loop
      for (let j = 0; j < bandStructData[i].band_energies[0].length; j++) {
        let tempValue= Math.max.apply(null, bandStructData[i].band_energies[0]/*spin1*/[j]/*first_band*/);
        if (tempValue > bandMax) bandMax= tempValue;
        tempValue= Math.min.apply(null, bandStructData[i].band_energies[0]/*spin1*/[j]/*first_band*/);
        if (tempValue < bandMin) bandMin= tempValue;
      }

      if (bandStructData[i].band_energies.length === 2)
        // spin2 - per band energy loop
        for (let j = 0; j < bandStructData[i].band_energies[1].length; j++) { // Per segment
          let tempValue= Math.max.apply(null, bandStructData[i].band_energies[1]/*spin1*/[j]/*first_band*/);
          if (tempValue > bandMax) bandMax= tempValue;
          tempValue= Math.min.apply(null, bandStructData[i].band_energies[1]/*spin1*/[j]/*first_band*/);
          if (tempValue < bandMin) bandMin= tempValue;
        }
    }
    return [bandMin/this.factor, bandMax/this.factor];
  }


  drawKPointLabel(x, label){
    svg.addText(this.axisGroup, x*this.xRel, 16, label, 'middle', 'steps');
  }


  setBandStructureData(data){

    let bandStructData;
    if (data.segments !== undefined)  bandStructData = data.segments;
    else bandStructData = data;

    this.bandGapData = undefined;
    if (data.band_gap !== undefined && data.band_gap.position !== undefined){
      this.bandGapData = data.band_gap.position;
      this.bandGapData.cbmDistances = [];
      this.bandGapData.vbmDistances = [];
    }

    // Gather all the points per band (divided by spin) crossing the segments
    this.bandsDataSpin1= [];  // [segment][band][kpoint]
    this.bandsDataSpin2= [];
    this.segmentLimitsX = [];
    this._reset();

    let topAndLowestPoints = this.getTopAndLowestPoints(bandStructData);
    let minEnergyVal = topAndLowestPoints[0];
    let maxEnergyVal = topAndLowestPoints[1];

    if (this.phononMode){
      this.setAxisRangeAndLabels('',0,1,'Frequency (cm⁻¹)',-50, 320,
        minEnergyVal, maxEnergyVal, 100);
    }else
      this.setAxisRangeAndLabels('',0,1,'Energy (eV)' ,-6 ,11 , minEnergyVal,
        maxEnergyVal, 5 );

    // Calculates de distance
    let totalDistance= 0;
    for (let k = 0; k < bandStructData.length; k++) {
      let kPoints= bandStructData[k].band_k_points;
      totalDistance+= kPointDistance(kPoints,kPoints.length-1);
    }
    let currentDistance= 0;
    let prevLastLabel = null;
    let dataOverflow = false;

    for (let k = 0; k < bandStructData.length; k++) { // For every  segment

      let segment= bandStructData[k];
      let kPoints= segment.band_k_points;
      let labels= segment.band_segm_labels;

      let energiesSpin1= segment.band_energies[0];
      let energiesSpin2= segment.band_energies[1];
      this.bandsDataSpin1.push([]);  // Add a new array per segment
      this.bandsDataSpin2.push([]);

      let segmentDistance= kPointDistance(kPoints,kPoints.length-1);

      // keeping the segment limits (x coordenate) for after painting
      this.segmentLimitsX.push(currentDistance/totalDistance);

      if (labels !== null){
        // Set k-points labels
        if (prevLastLabel !== null && prevLastLabel !== labels[0])
          this.drawKPointLabel(currentDistance/totalDistance,
            getSymbol(prevLastLabel)+'|'+getSymbol(labels[0]));
        else
          this.drawKPointLabel(currentDistance/totalDistance,getSymbol(labels[0]));
        // The last label
        if (k === bandStructData.length -1)
          this.drawKPointLabel(1, getSymbol(labels[1]));

        prevLastLabel = labels[1];
      }

      for (let i = 0; i < kPoints.length; i++) { // For every  k-point

        let tempDistance= (currentDistance + kPointDistance(kPoints, i))/totalDistance;

        if (this.bandGapData !== undefined){

          if (this.bandGapData.lower !== undefined){
            let kpt = this.bandGapData.lower.kpt;
            if (kPoints[i][0] === kpt[0] && kPoints[i][1] === kpt[1]
              && kPoints[i][2] === kpt[2]){
                this.bandGapData.cbmDistances.push(tempDistance);
            }
          }

          if (this.bandGapData.upper !== undefined){
            let kpt = this.bandGapData.upper.kpt;
            if (kPoints[i][0] === kpt[0] && kPoints[i][1] === kpt[1]
              && kPoints[i][2] === kpt[2]){
                this.bandGapData.vbmDistances.push(tempDistance);
            }
          }

          /*
          let kpt = this.bandGapData.cbmKpt;
          if (kPoints[i][0] === kpt[0] && kPoints[i][1] === kpt[1]
            && kPoints[i][2] === kpt[2]){
              //console.log("kpoints: ", kPoints[i], kpt, tempDistance);
              this.bandGapData.cbmX = tempDistance;
          }
          kpt = this.bandGapData.vbmKpt;
          if (kPoints[i][0] === kpt[0] && kPoints[i][1] === kpt[1]
            && kPoints[i][2] === kpt[2]){
              //console.log("kpoints: ", kPoints[i], kpt, tempDistance);
              this.bandGapData.vbmX = tempDistance;
          }
          */
        }


        // All bands spin1
        for (let j = 0; j < energiesSpin1[i].length; j++) {
          if (i === 0) this.bandsDataSpin1[k][j] = [];//if (k === 0 && i === 0) this.bandsDataSpin1[j] = [];
          let currentY = energiesSpin1[i][j]/this.factor;
          this.bandsDataSpin1[k][j].push({x: tempDistance, y: currentY});
          if (!dataOverflow && currentY > 10000)  dataOverflow = true;
        }
        // All bands spin2
        if (energiesSpin2 !== undefined)
          for (let j = 0; j < energiesSpin2[i].length; j++) {
            if (i === 0) this.bandsDataSpin2[k][j] = [];
            let currentY = energiesSpin2[i][j]/this.factor;
            this.bandsDataSpin2[k][j].push({x: tempDistance, y: currentY});
            if (!dataOverflow && currentY > 10000)  dataOverflow = true;
          }
        //console.log("K PPPPPP Ponint: "+i+' DIS: '+tempDistance, this.bandsDataSpin1[k]);
      }

      currentDistance+= segmentDistance;
    }

    if (dataOverflow) throw 'Plotter Data Overflow: Probably the energy data is not in correct units'; //console.log('BSPlotter data overflow');
    else this.repaint();
  }


  repaintData(yMin, yMax){

    this.segmentLimitsX.forEach(x => {
      let yMinPx = this.transformY(yMin);
      let yMaxPx = this.transformY(yMax);
      if (this.phononMode) { yMinPx += 200; yMaxPx -= 200; }
      svg.addLine(this.plotContent, x*this.xRel, yMinPx,
        x*this.xRel, yMaxPx, 'segment');
    });

    // Drawing lines
    let polylinePoints;
    for (var i = 0; i < this.bandsDataSpin1.length; i++) // loop the segments

      for (var j = 0; j < this.bandsDataSpin1[i].length; j++) { // loop the bands
        polylinePoints = '';
        for (var k = 0; k < this.bandsDataSpin1[i][j].length; k++) { // loop the kpoints
          polylinePoints+= ' '+this.xRel*this.bandsDataSpin1[i][j][k].x+
            ' '+this.transformY(this.bandsDataSpin1[i][j][k].y);
        }
        svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin1');
      }

    if (this.bandsDataSpin2.length > 0){
      for (var i = 0; i < this.bandsDataSpin2.length; i++) // loop the segments

        for (var j = 0; j < this.bandsDataSpin2[i].length; j++) { // loop the kpoints
          polylinePoints = '';
          for (var k = 0; k < this.bandsDataSpin2[i][j].length; k++) { // loop the bands
            polylinePoints+= ' '+this.xRel*this.bandsDataSpin2[i][j][k].x+
              ' '+this.transformY(this.bandsDataSpin2[i][j][k].y);
          }
          svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin2');
        }
    }


    // Paint CBM and VBM
    // console.log("Paint CBM and VBM: ", this.vbm); this.vbm.y = -1.01577992001999e-20/this.factor;
    if (this.bandGapData !== undefined
      // If there are two spins related data the CBM and VBM points aren't shown
    /*&& this.bandsDataSpin2.length === 0*/)
       {

         this.bandGapData.cbmDistances.forEach( distance => {
           let x = this.xRel * distance;
           let y = this.transformY(this.bandGapData.lower.energy/this.factor);
           //console.log("hhhhhhhhh: ", x, y);
           svg.addPoint(this.plotContent, x, y , 3, 'cbm-vbm-points');
           svg.addText(this.plotContent, x+4, y-6, 'CBM');
         });


         this.bandGapData.vbmDistances.forEach( distance => {
           let x = this.xRel*distance;
           let y = this.transformY(this.bandGapData.upper.energy/this.factor);
           svg.addPoint(this.plotContent, x, y, 3, 'cbm-vbm-points');
           svg.addText(this.plotContent, x+4, y+14, 'VBM');
          });

        /*

      let x = this.xRel*this.bandGapData.cbmDistance;
      let y = this.transformY(this.bandGapData.lower.energy/this.factor);
      //console.log("hhhhhhhhh: ", x, y);
      svg.addPoint(this.plotContent, x, y , 3, 'cbm-vbm-points');
      svg.addText(this.plotContent, x+4, y-6, 'CBM');

      x = this.xRel*this.bandGapData.vbmDistance;
      y = this.transformY(this.bandGapData.upper.energy/this.factor);
      svg.addPoint(this.plotContent, x, y, 3, 'cbm-vbm-points');
      svg.addText(this.plotContent, x+4, y+14, 'VBM');
      */
    }

  }


}


function kPointDistance(kPoints, position){
  let p0= kPoints[0];
  let p1= kPoints[position];
  let deltaX= p1[0] - p0[0];
  let deltaY= p1[1] - p0[1];
  let deltaZ= p1[2] - p0[2];
  return Math.sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);
}

//TODO check if any code still provides these non UTF labels
function getSymbol(label){
//  if (label === 'Gamma' || label === 'G') return 'Γ';
//  else return label;
}

// EXPORTS
module.exports = BSPlotter;
