
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  'Details' view container that shows all the material info related to
  its electronic structure.

  This container is quite complex.

  In the file there are two defined (classes) components used in the container:
  - SummaryByFunctionalsComponent: the component (left panel, below part)
  showing a band gap summary info by functional.
  - BZViewerWrapper: This component is a wrapper for the Brillouin zone viewer
 */

"use strict";

let DetailsViewBase = require('./DetailsViewBase.js');
let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
let CalcSelectorBar = require('./CalcSelectorBar.js');
let StatsViewer = require('./StatsViewer.js');
let BSDOSPlotter = require('./BSDOSPlotter.js');
let PlotterBase = require('../common/PlotterBase.js');
let plotter= new PlotterBase();
let DataStore = require('./DataStore.js');
let LoadingPopup = require('../common/LoadingPopup.js');

function setFermiVizContent(fermiBox,url){
  let sceneContent;
  if (url === ''){
    fermiBox.innerHTML= '';//fermiBox.removeChild(plotter.canvas);
    plotter.attach(fermiBox,undefined,316);
    plotter.setNoData();
  }
  else{
    sceneContent= '<inline url="'+url+'"> </inline>';
    fermiBox.innerHTML=
      `<x3d id="x3dframe" width='290px' height='290px' >
        <scene>${sceneContent} </scene>
      </x3d>`;
    x3dom.reload();
  }

}

class ElectronicStructDetails  extends DetailsViewBase{

  constructor() {
    super('Electronic Structure');
    this.firstId;
    this.lastId;
    this.element.innerHTML+=`
      <div style="float: left; width: 30%;">
        <div class="view-box">
          <div class="title">Calculations </div>
          <div class="navTreeWrapper"></div>


          <div class="summary-title">Summary</div>
          <div class="footer summary-box" style="border-top: 0">
          </div>
        </div>
      </div>

      <div style="float: right; width: 70%;">
        <div class="view-box e-structure-box">

          <div class="title">Electronic structure</div>

          <div style="padding-top: 10px;">
            <div class="calc-selector-host"></div>
          </div>


          <div>

            <div  style="padding: 30px  100px 20px 100px; ">
              <div class="info-fields-label" style="float: left; width: 54%; ">
                <span info-sys-data="band-structure">Band structure</span>
              </div>
              <div class="info-fields-label" style="float: left;">
                <span info-sys-data="DOS">DOS</span>
              </div>
              <div style="clear: both;"></div>

              <div class="calc-bs-dos-plotter" >
              </div>

              <div>
              <div class="band-gap-field" style="float: left; width: 56%; text-align: right">
                <b><span info-sys-data="band-gap">Band gap</span></b>:
                <span class="band-gap-value" ></span>
              </div>
              <div style="clear: both;"></div>

              </div>
            </div>

            <div class="spin-legend" style="font-size: 0.9em; padding: 0 30px 10px; display: none">
              <svg width="15px" height="10px"> <polyline points="0,5 15,5" class="plotSpin1"/></svg>
              Spin <span style='font-size: 1.1em'>⇧</span>  &nbsp;&nbsp;&nbsp;
              <svg width="15px" height="10px"> <polyline points="0,5 15,5" class="plotSpin2"/></svg>
              Spin <span style='font-size: 1.1em'>⇩</span>
            </div>

          </div>

          <div class="footer lower-section">

            <div  style="float: left">
              <div style="padding: 16px; ">
                <div class="info-fields-label" >
                  <span info-sys-data="brillouin-zone-viewer">Brillouin zone</span>
                </div>
                <div class="bz-viewer-wrapper" style="width: 400px; height: 400px">
                </div>
              </div>
            </div>

            <div class="band" style="float: right; width: 40%;">
              <div style="padding: 16px; ">
                <div class="info-fields-label" >
                  <!-- <span info-sys-data="fermi-surface">Fermi surface </span> -->
                </div>
                <div class="fermi-box" >      </div>
              </div>
            </div>

            <div style="clear: both;"></div>

          </div> <!-- footer -->

        </div>

      </div> <!-- view-box -->
    `;

    this.navTreeWrapper =
        this.element.getElementsByClassName("navTreeWrapper")[0];

    //this.leafTitle = this.element.querySelector('.tree-leaf-title');
    this.calcSelector = new CalcSelectorBar('calc-selector-bar','70%');
    this.element.querySelector('.calc-selector-host').
      appendChild(this.calcSelector.element);

    this.rightBox = this.element.querySelector('.e-structure-box');

    this.summaryByFunctionals = null;

    this.bsDosPlotter = new BSDOSPlotter();

    this.bandGapField = this.element.querySelector('.band-gap-field');
    this.bandGapValue = this.element.querySelector('.band-gap-value');

    this.spinLegend = this.element.querySelector('.spin-legend');

    this.lowerSection = this.element.querySelector('.lower-section');

    this.fermiBox= this.element.getElementsByClassName('fermi-box')[0];
    // Load the x3dom library
    util.loadLib("lib/x3dom.js");

    this.bzViewerWrapper = this.element.querySelector('.bz-viewer-wrapper');
    this.bzViewerWrapper = new BZViewerWrapper(this.bzViewerWrapper);

    // For static ones
    InfoSys.addToInfoSystem(this.element);

    this._events();
  }

  _events() {
    //super._events();

    this.calcSelector.setPrevListener(e => {
      if (this.groupIndex > 0){
        let calcId = this.groupCalcs[--this.groupIndex];
        this._loadGraphData(calcId);
        this._updateSelectorState(calcId);
        return this.groupIndex === 0; // the first
      }
    });

    this.calcSelector.setNextListener( e => {
      if (this.groupIndex < this.groupCalcs.length-1){
        let calcId = this.groupCalcs[++this.groupIndex];
        this._loadGraphData(calcId);
        this._updateSelectorState(calcId);
        return this.groupIndex === this.groupCalcs.length-1; // the last
      }
    });
  }


  updateSelection(leafIds/*Set*/){
    //console.log('E-StructureDetails UPDATING calcs: ',calcs.values().next().value);
    if (leafIds.size > 0){
      this.rightBox.style.visibility = 'visible';
      //console.log('E-StructureDetails UPDATING calcs:leafIds.size > 0 ',leafIds);
      let calcMapByFunctional = getCalcMapByFunctional(leafIds);
      if (this.summaryByFunctionals === null)
        this.summaryByFunctionals =
          new SummaryByFunctionalsComponent(calcMapByFunctional,
            this.element.querySelector('.summary-box'));
      else
        this.summaryByFunctionals.build(calcMapByFunctional);
      // let bandGapDirect= calcs.values().next().value.band_gap_direct;
      //let bandGapType= (bandGapDirect ? "direct" : "indirect");

      let counter= 0;
      leafIds.forEach( calcId => {
        counter++;
        if (counter === 1) this.firstId = calcId;
        if (counter === leafIds.size) this.lastId = calcId;
      });
    }else
      this.rightBox.style.visibility = 'hidden';

    function getCalcMapByFunctional(leafIds/*Set*/){
      let functCalcMap = new Map();
      leafIds.forEach( leafId => {
        let calc = DataStore.getCalc(DataStore.getCalcReprIntId(leafId));
        if (calc.has_band_structure){
          //console.log('CALC BS ',calc.band_gap,calc.band_gap_direct);
          if (functCalcMap.has(calc.functional_type)){
            functCalcMap.get(calc.functional_type).add(calc);
          }else{ // New functional
            let newFunctionalArray = new Set();
            newFunctionalArray.add(calc);
            functCalcMap.set(calc.functional_type, newFunctionalArray);
          }
        }
      });
      return functCalcMap;
    }

  }



  updateMarkedLeaf(leafId){
    //console.log('updateMarkedCalc '+calc);
    this.groupCalcs = null; // if the leafId is not a group else this.groupCalcs is array
    if (leafId === null){
      //this.leafTitle.innerHTML = 'NO SELECTION';
      this.calcSelector.setState('NO SELECTION', true, true);
      return;

    }else{
      if (DataStore.getGroups().has(leafId)){
        //this.leafTitle.innerHTML = leafId+' ('+DataStore.getGroups().get(leafId).calcs.size+')';
        this.groupCalcs =  Array.from(DataStore.getGroups().get(leafId).calcs);
        let calcId = DataStore.getCalcReprIntId(leafId);
        this.groupIndex = this.groupCalcs.indexOf(calcId);
        this._updateSelectorState(calcId);
      }else
        //this.leafTitle.innerHTML = leafId;
        this.calcSelector.setState(leafId, true, true);
    }

    this._loadGraphData(DataStore.getCalcReprIntId(leafId));

    // calc.fermi_surface

  }


  _updateSelectorState(leafId){
    let t = leafId+' ('+(this.groupIndex+1)+'/'+this.groupCalcs.length+')';
    this.calcSelector.setState(t, this.groupIndex === 0,
      this.groupIndex === this.groupCalcs.length-1);
  }


  _loadGraphData(calcId){

    let calc = DataStore.getCalc(calcId);

    if (!this.bsDosPlotter.isAttached())
      this.bsDosPlotter.attach(this.element.querySelector('.calc-bs-dos-plotter')
        ,undefined,360);

    if (calc === null || (!calc.has_band_structure && !calc.has_dos)){
      this.bsDosPlotter.setNoData();
      this.bzViewerWrapper.setNoData();
      this.bandGapField.style.display = 'none';
      this.lowerSection.style.display = 'none';

    }else{
      LoadingPopup.show();
      let matId = DataStore.getMaterialData().id;

      util.serverReq(util.getMaterialCalcURL(matId, calc.id, 'dos'), e1 => {
        let dosData = JSON.parse(e1.target.response).dos;

        util.serverReq(util.getMaterialCalcURL(matId, calc.id, 'band_structure'),
        e2 => {
          let bsData= JSON.parse(e2.target.response).band_structure;
          if (bothSpins(bsData, dosData))
            this.spinLegend.style.display = 'block';

          //***util.addBandGapData(calcData, bsData);
          //console.log('CODE NAME:', calc.code_name);
          this.bsDosPlotter.setUpAndData(bsData, dosData, calc.code_name );

          if (calc.has_band_structure){

            this.bandGapField.style.display = 'block';
            this.bandGapValue.textContent= util.J2eV(calc.band_gap, 2)+' eV ';
            // console.log('calc.brillouin_zone_json',calc.brillouin_zone_json);
            if (calc.brillouin_zone_json !== null){
              this.lowerSection.style.display = 'block';
              this.bzViewerWrapper.setCalcData(calc.brillouin_zone_json, bsData.segments);
            }else{
              this.lowerSection.style.display = 'none';
              this.bzViewerWrapper.setNoData();
            }

            //this.bzViewerWrapper.setCalcData(calc.brillouin_zone_json, bsData.segments);
            // if there bandstruc data && calc.band_gap === 0  => show FermiSurface
            //if (calc.band_gap === 0)
              //setFermiVizContent(this.fermiBox, (calc === null ? '' : util.FERMI_SURFACE_URL));
          }else{
            this.lowerSection.style.display = 'none';
            this.bandGapField.style.display = 'none';
            this.bzViewerWrapper.setNoData();
          }
          LoadingPopup.hide();
        });
      });

    }

    function bothSpins(bsData, dosData){
      if (bsData !== undefined){
        if (bsData.segments[0].band_energies.length === 2) return true;
      }
      if (dosData !== undefined){
        if (dosData.dos_values.length === 2) return true;
      }
      return false;
    }
  }


  setPrevCalcListener(listener){
    this.prevCalcListener= listener;
  }


  setNextCalcListener(listener){
    this.nextCalcListener= listener;
  }

}



class SummaryByFunctionalsComponent{

  constructor(calcMapByFunctional, hostElement){
    this.calcMapByFunctional = calcMapByFunctional;
    this.hostElement = hostElement;
    this.viewType = 'text';
    this.functional = null;
    this.hostElement.innerHTML+=`
      <div style="float: left" >
        <svg xmlns="http://www.w3.org/2000/svg" class="chart-tab"
          viewBox="0 0 15 15" width="15" height="15" style="fill: #c7c7c7;">
            <rect x="0" y="0"  width="2" height="15" />
            <rect   x="3" y="5"  width="1.8" height="7"  />
            <rect  x="6" y="3"  width="1.8" height="9"  />
            <rect   x="9" y="6"  width="1.8" height="6"  />
            <rect  x="12" y="2"  width="1.8" height="10"  />
            <rect x="2" y="13"   width="13" height="2" />
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" class="text-tab"
          viewBox="0 0 15 15" width="15" height="15" style="fill: #777;">
            <rect x="0" y="1"   width="15" height="2.5" />
            <rect   x="0" y="6"  width="15" height="2.5"  />
            <rect  x="0" y="11"  width="15" height="2.5"  />
        </svg>
      </div>

      <div class="functional-tabs" style="float: right">
      </div>

      <div style="clear: both;"></div>

      <div class="content-placeholder" >

        <div style="display: block" class="text-panel" >

          <div><b><span info-sys-data="band gap">Band gap</span></b> (eV):
            <div class="stats-fields summary-bandgap-field" > </div>
          </div>
        </div>

        <div style="display:none" class="chart-panel" >
          <div class="charts-placeholder" > </div>
        </div>

      </div>
    `;
    this.chartTab = this.hostElement.querySelector('.chart-tab');
    this.textTab = this.hostElement.querySelector('.text-tab');
    this.functionalTabs = this.hostElement.querySelector('.functional-tabs');
    this.chartPanel = this.hostElement.querySelector('.chart-panel');
    this.textPanel = this.hostElement.querySelector('.text-panel');
    this.bandgapField = this.hostElement.querySelector('.summary-bandgap-field');

    this.statsViewer = new StatsViewer();
    let chartsPlaceholder = this.hostElement.querySelector('.charts-placeholder');
    this.statsViewer.attach(chartsPlaceholder, 250, 150);

    this.build(calcMapByFunctional);

    this.chartTab.addEventListener( "click", e => {
      this.chartTab.style.fill = '#777';
      this.viewType = 'chart';
      this.textTab.style.fill = '#c7c7c7';
      this.chartPanel.style.display = 'block';
      this.textPanel.style.display = 'none';
    });

    this.textTab.addEventListener( "click", e => {
      this.textTab.style.fill = '#777';
      this.viewType = 'text';
      this.chartTab.style.fill = '#c7c7c7';
      this.textPanel.style.display = 'block';
      this.chartPanel.style.display = 'none';
    });

    this.functionalTabs.addEventListener( "click", e => {
      if (e.target.className === 'tab'){
        this.functionalTabs.querySelector('[data-tab="'+this.functional+'"]')
          .className = 'tab';
        this.functional = e.target.getAttribute('data-tab');
        this.functionalTabs.querySelector('[data-tab="'+this.functional+'"]')
          .className = 'tab-selected';
        this._setData();
      }
    });

  }

  _setData(){
    let stats = this.functionalBandGapMap.get(this.functional);
    // Set text data

    this.bandgapField.innerHTML = stats.html;

    // Set Charts data
    this.statsViewer.clear();
    this.statsViewer.drawPoints(stats.data, stats.label, stats.min, stats.max);
  }



  build(calcMapByFunctional){

    if (calcMapByFunctional.size === 0){
      this.hostElement.style.display = 'none';
      this.hostElement.previousElementSibling.style.display = 'none';
    }else{
      this.hostElement.style.display = 'block';
      this.hostElement.previousElementSibling.style.display = 'block';
    }

    this.unfoldedElement = null;
    this.functionalTabs.innerHTML = '';
    this.functionalBandGapMap = new Map();
    //console.log('calcMapByFunctional',calcMapByFunctional, this.hostElement);
    calcMapByFunctional.forEach( (calcs, functionalName) =>{
      //let statsMap = util.getQuantityStatsMap(calcs);
      let array= [];
      calcs.forEach( calc => {
        array.push(calc.band_gap/1.602176565e-19);
      });
      let stats = {};
      stats.data = array;
      stats.min = Math.min.apply(null, array);
      stats.max = Math.max.apply(null, array);
      stats.equal = (stats.min === stats.max);
      stats.label = 'band gap';
      stats.html = util.getAverage(stats.data).toFixed(2)+
        ' &nbsp; <span style="font-size: 0.9em">['+stats.min.toFixed(2)
        +' , '+stats.max.toFixed(2)+']</span>';

      this.functionalBandGapMap.set(functionalName, stats);

      let tabClass= 'tab';
      if (this.functional === null)     this.functional = functionalName;
      if (this.functional === functionalName){
        this._setData();
        tabClass= 'tab-selected';
      }

      this.functionalTabs.innerHTML +=
        '<span class="'+tabClass+'" data-tab="'+functionalName+'">'+functionalName+'</span>';
    });

    InfoSys.addToInfoSystem(this.hostElement);
  }

}



class BZViewerWrapper{

  constructor(hostElement) {
    this.hostElement = hostElement;
    this.bzViewer = null;
  }


  setCalcData(bzData, bsData){
    if (this.bzViewer === null)
      this.bzViewer = new BrillouinZoneViewer(this.hostElement);
    this.bzViewer.load(this._getBZDataForViewer(bzData,bsData));
    this.hostElement.style.visibility = 'visible';
  }


  setNoData(){
    this.hostElement.style.visibility = 'hidden';
  }


  _getBZDataForViewer(bz_json, bs_json){
    let labels = [];
    let kPoints = [];
    bs_json.forEach( segment => {
      labels.push(segment.band_segm_labels);
      kPoints.push(segment.band_k_points);
    });

    let data = {
      vertices: bz_json.vertices,
      faces: bz_json.faces,
      basis: bz_json.basis,
      labels: labels,
      segments: kPoints
    }
    return data;
  }

}


// EXPORTS
module.exports = ElectronicStructDetails;
