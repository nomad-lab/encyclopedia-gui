

/*
Pending to implement
*/
"use strict";

let DetailsViewBase = require('./DetailsViewBase.js');
let util = require('../common/util.js');
let InfoSys = require('../common/InfoSys.js');
//let DataStore = require('./DataStore.js');



class ElasticConstDetails extends DetailsViewBase {

  constructor() {
    super('Elastic constants');

    this.sortedLeafs = [];
    this.markedCalc = null;

    this.element.innerHTML+=`

      <div style="float: left; width: 27%;">
        <div class="view-box">
          <div class="title">Calculation </div>
          <div class="navTreeWrapper"></div>
        </div>
      </div>

      <div style="float: right; width: 73%;">
        <div class="view-box">
          <div class="title">Elastic constants</div>

          <div>Parameters</div>
          <div ></div>


        </div>
      </div>

      <div style="clear: both;"></div>
    `;

    this.navTreeWrapper =
        this.element.querySelector('.navTreeWrapper');


    // For static ones
    InfoSys.addToInfoSystem(this.element);

    this._events();
  }


  _events() {
    //super._events();
/*
    this.dataTableWrapper.addEventListener('click', (e) => {

      let rowElement = e.target.parentElement;
      if (rowElement.className.indexOf('data-row') < 0)
        rowElement = rowElement.parentElement;
      //console.log("TABLE EVENT ",rowElement);

      if (rowElement.className.indexOf('data-row') >= 0){
        let id= rowElement.getAttribute('data-calc-id');


      }

    });
*/
  }



  updateSelection( leafIds /*Set*/){
    console.log('ElasticDetails updateSelection ',leafIds);

  }


  updateMarkedLeaf(leafId){

/* Do nothing for now...
    this.markedCalc = leafId;
    let rowElement= this.element.querySelector('.data-row-marked');
    if (rowElement !== null) rowElement.className= 'data-row';

    if (this.markedCalc  !== null){
      let rowElement1= document.querySelector('tr[data-calc-id="'+this.markedCalc+'"]');
      if (rowElement1 !== null) rowElement1.className= 'data-row-marked';
    }
    */
  }

}


// EXPORTS
module.exports = ElasticConstDetails;
