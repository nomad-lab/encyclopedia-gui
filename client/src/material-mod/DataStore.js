
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  This file create and holds the application data models.
  It stores the data loaded from the backend (immutable).
  In addition it creates app life-time entities for convenience
 */


let materialData;

function setMaterialData(dataFromAPI){
  materialData = dataFromAPI;
}

function getMaterialData(){
  return materialData;
}


let calcs;
let calcMap = new Map();

function setCalculations(calcsFromAPI){
  calcs = calcsFromAPI;
  for (let i = 0; i < calcs.length; i++)
    calcMap.set(calcs[i].id, calcs[i]);
}

function getCalculations(){
  return calcs;
}

function getCalc(calcId){
  return calcMap.get(calcId);
}



let groups;

function processCalcGroups(groupsFromAPI){

  let methodCalcsMap = new Map();
  groupsFromAPI.forEach( (group, i) => {
    if (group.group_type !== 'method'){
      let calcsMins;
      if (methodCalcsMap.has(group.method_hash) ){
        calcsMins = methodCalcsMap.get(group.method_hash);
      }else{
        calcsMins = {'ids': [], 'minEnergies': []};
        methodCalcsMap.set(group.method_hash, calcsMins);
      }
      calcsMins.ids.push(group.representative_calculation_id);
      calcsMins.minEnergies.push(group.energy_minimum);
    }
  });

  let methodRepresentativeMap = new Map();
  methodCalcsMap.forEach( (calcsData, methodHash) => {
    let minVal = Math.min.apply(null, calcsData.minEnergies);
    let index = calcsData.minEnergies.indexOf(minVal);
    methodRepresentativeMap.set(methodHash, calcsData.ids[index]);
  });

  let calcsInGroups = new Map();
  groupsFromAPI.forEach( (group, i) => {
    if (group.group_type !== 'method'){
      let groupData = {
        'calcs': new Set(),
        'method_representative': methodRepresentativeMap.get(group.method_hash)
      };
      calcsInGroups.set(
        getGroupTypeCode(group)+group.representative_calculation_id, groupData);
      //let calcSet = new Set(); // The representative is (not?) in the set
      group.calculations_list.forEach( calcId => groupData.calcs.add(calcId) );
    }
  });
  return calcsInGroups;
}

function setGroups(groupsFromAPI){
  groups = processCalcGroups(groupsFromAPI);
}

function getGroups(){
  return groups;
}

function getGroupTypeCode(group){
  if (group.group_type === 'equation of state') return 'eos';
  else if (group.group_type === 'convergence') return 'par';
}

function getGroupType(leafId){
  let code = leafId.substring(0,3);
  if (code === 'eos') return 'equation of state';
  else if (code === 'par') return 'convergence';
  else return null;
}

function isGroup(leafId){//*** ********** REFACTOR
  console.log("leafId:" + leafId);
  return (leafId.substring(0,3) === 'eos' || leafId.substring(0,3) === 'par');
}

function getCalcReprIntId(leafId){
  if (isGroup(leafId)) return parseInt(leafId.substring(3));
  else return parseInt(leafId);
}

function isInAnyGroup(calcId){
  let thereIs = false;
  groups.forEach( (groupData, groupId) => {
    //console.log('isInAnyGroup', calcId, groupData.calcs);
    if (groupData.calcs.has(calcId)) thereIs = true;//return true;
  });
  return thereIs;
}

function isInAnyNotDisabledGroup(calcId){
  let thereIs = false;

  groups.forEach( (groupData, groupId) => {
    let representative = getCalc(getCalcReprIntId(groupId));
    //let codeNameTrimed= representative.code_name.trim();
    //console.log('isInAnyGroup', calcId, groupData.calcs);

    /***** Exception: disable grouping for some codes
    if (codeNameTrimed !== 'VASP' && codeNameTrimed !== 'FHI-aims'
      && groupData.calcs.has(calcId)) thereIs = true; */
    if (groupData.calcs.has(calcId)) thereIs = true;
  });
  return thereIs;
}

function getGroupLeafId(calcId){
  let leafId = null;
  groups.forEach( (groupData, groupId) => {
    //console.log('isInAnyGroup', calcId, groupData.calcs);
    if (groupData.calcs.has(calcId)) leafId = groupId;//return true;
  });
  //console.log('getGroupLeafId', leafId);
  return leafId;
}


let hasThermalData, hasElecStructureData;

/*
function hasThermalData(bool){
  hasThermalData = bool;
}

function setHasThermalData(bool){
  hasThermalData = bool;
}

function hasElecStructureData(bool){
  hasThermalData = bool;
}

function setHasElecStructureData(bool){
  hasElecStructureData = bool;
}*/



// EXPORTS
module.exports = { setMaterialData, getMaterialData, getCalculations, getCalc,
  setCalculations, getGroups, setGroups, isGroup, getGroupType,
  getCalcReprIntId, isInAnyGroup, isInAnyNotDisabledGroup, getGroupLeafId,
 hasThermalData, hasElecStructureData};
