
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  Sorting button component implementation
 */


let util = require('../common/util.js');

class SortingButton {

  constructor(id) {

    this.id = id;
    this.ascending = true;

    this.element = document.createElement('span');
    this.element.innerHTML+=`
       <img src="img/sorting_init.png" width="12px"
        style="margin-bottom: -1px; cursor: pointer"/>
    `;
    this.image = this.element.querySelector('img');

    if (id === 'id')
      this.image.setAttribute('src','img/sorting_ascending.png');

    this.element.addEventListener('click', e => {
      this.ascending = !this.ascending;
      this.image.setAttribute('src',
        'img/sorting_'+(this.ascending ? 'ascending' : 'descending')+'.png');
      this.listener(this.ascending, this.id);
    });
  }


  init(){
    this.image.setAttribute('src','img/sorting_init.png');
  }


  setListener(listener){
    this.listener = listener;
  }

}

// EXPORTS
module.exports = SortingButton;
