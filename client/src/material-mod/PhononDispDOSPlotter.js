
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
 Implements a graphical UI component composed of
 a phonon DOS plotter (PhononDOSPlotter class, implemented in thsi file) and a
 BSPlotter (showing Phonon Dispersion)
 */


let BSPlotter = require('./BSPlotter.js');
let InteractivePlotterBase = require('../common/InteractivePlotterBase.js');
let svg = require('../common/SVG.js');
let util = require('../common/util.js');


class PhononDispDOSPlotter{

  constructor() {
    this.element = document.createElement('div');
    //this.element.setAttribute('id','elementable');
    this.parentElement= null;
    this.dispPlotter= new BSPlotter();
    this.dispPlotter.setPhononMode();

    this.dosPlotter= new PhononDOSPlotter();
  }


  attach(element, width, height){
    element.appendChild(this.element);
    this.dispPlotter.attach(this.element, height, height);
    this.dosPlotter.attach(this.element, undefined, height);
    this.parentElement= element;
  }


  isAttached(){
    return this.parentElement !== null;
  }


  setUpAndData(dispData, dosData){

    this.hasDispData = (dispData !== undefined && dispData !== null);
    this.hasDosData = (dosData !== undefined && dosData !== null);

    if (this.hasDispData){
      this.dispPlotter.setBandStructureData(dispData);
      if (this.hasDosData)
        this.dispPlotter.setRepaintListener( (yZoom, yOffset) => {
          this.dosPlotter.setYZoomAndOffset(yZoom, yOffset);
          this.dosPlotter.repaint();
        });
    }else
      this.dispPlotter.setNoData();

    if (this.hasDosData){
      this.dosPlotter.setPoints(dosData);
      if (this.hasDispData)
        this.dosPlotter.setRepaintListener( (yZoom, yOffset) => {
          this.dispPlotter.setYZoomAndOffset(yZoom, yOffset);
          this.dispPlotter.repaint();
        });
    }else
      this.dosPlotter.setNoData();
  }

  setNoData(){
    this.dispPlotter.setNoData();
    this.dosPlotter.setNoData();
  }

}



class PhononDOSPlotter extends InteractivePlotterBase{

  constructor() {
    super({left: 4, right: 16, top: 0, bottom: 30});
    this.outOfRangeColorActivated = false;
  }

  // detach if necessary and attach
  attach(element, width, height){
    super.attach(element, height/2+this.margins.left, height);
  }


  setPoints(points){

    this.pointsSpin1 = [];
    this.pointsSpin2 = [];
    this._reset();

    let pSpin1= points.dos_values[0];
    let pSpin2 = null;
    if (points.dos_values.length === 2)  pSpin2 = points.dos_values[1];
    let pointsY= points.dos_energies;
    let pointsXInPlotRange = [];
    let pointsYInPlotRange = [];

    for (var i = 0; i < pointsY.length; i++) {
        let frecuency = pointsY[i]*5.034117012222e22;
        let dosSpin1 = pSpin1[i]*0.029979;
        let dosSpin2;
        if (pSpin2 !== null)  dosSpin2 = pSpin2[i]*0.029979246;
        //console.log('POINTS : ',frecuency);
        pointsXInPlotRange.push(dosSpin1);
        if (pSpin2 !== null) pointsXInPlotRange.push(dosSpin2);
        pointsYInPlotRange.push(frecuency);
        //console.log('POINTS : ',pointsX[i], energy);
        this.pointsSpin1.push({x: dosSpin1, y: frecuency});
        if (pSpin2 !== null) this.pointsSpin2.push({x: dosSpin2, y: frecuency});
    }

    let maxDosVal = Math.max.apply(null, pointsXInPlotRange);
    let maxEnergyVal = Math.max.apply(null, pointsYInPlotRange);
    let minEnergyVal = Math.min.apply(null, pointsYInPlotRange);

    // x axis steps generation
    let t = util.generateDiagramSteps(maxDosVal);
    let xSteps = t[0], exp = t[1];

    //console.log('formattedPoints paintPointsLine : ', this.formattedPoints.length);
    this.setAxisRangeAndLabels(null,0,xSteps[xSteps.length-1]/*maxDosVal*1.2*/,
      null,-50,320,minEnergyVal, maxEnergyVal, 100);

    svg.addText(this.axisGroup, this.plotRangeX/2, this.margins.bottom,
      'DOS (states/cm⁻¹)', 'middle', 'axis-steps-big');

    for (let i = 0; i < xSteps.length; i++) {
      let stepX = (this.plotRangeX*xSteps[i])/xSteps[xSteps.length-1];
      svg.addLine(this.axisGroup, stepX, 0, stepX, 3, 1);
      //console.log('step ',xSteps[i], stepX);
      svg.addText(this.axisGroup, stepX, 13,
        (i === 0 ? '0' : xSteps[i].toFixed(exp)), 'middle', 'axis-steps-smaller');
    }

    this.repaint();
  }


  repaintData(){
    let polylinePoints = '';
    for (var i = 0; i < this.pointsSpin1.length; i++) {
      polylinePoints+= ' '+this.xRel*this.pointsSpin1[i].x+
        ' '+this.transformY(this.pointsSpin1[i].y);
        //console.log('POINTS LAT : ',this.formattedPoints[i].x, this.formattedPoints[i].y);
    }
    svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin1');

    polylinePoints = '';
    for (var i = 0; i < this.pointsSpin2.length; i++) {
      polylinePoints+= ' '+this.xRel*this.pointsSpin2[i].x+
        ' '+this.transformY(this.pointsSpin2[i].y);
        //console.log('POINTS LAT : ',this.formattedPoints[i].x, this.formattedPoints[i].y);
    }
    svg.addPolyline(this.plotContent, polylinePoints, 'plotSpin2');

  }
}

// EXPORTS
module.exports = PhononDispDOSPlotter;
