
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This component is the loading popup window 
 */


"use strict";

let util = require('./util.js');

let loadingPopup = document.querySelector('#loading-popup');

function show(){
  let ttRect = loadingPopup.getBoundingClientRect();
  let leftPos =  (window.innerWidth - ttRect.width)/2;
  let topPos = (window.innerHeight -ttRect.height)/2;
  loadingPopup.style.left = leftPos+'px';
  loadingPopup.style.top = (topPos-100)+'px';
  loadingPopup.style.visibility = 'visible';
}

function hide(){
  loadingPopup.style.visibility = 'hidden';
}

// EXPORTS
module.exports = { show, hide };
