
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  This is the base class that models a highly interactive plotter.
  It is inherited by several classes implementing specifc plotters.
  This plotter implements zoom and y-axis offset
 */


"use strict";


let svg = require('./SVG.js');

const xmlns="http://www.w3.org/2000/svg";
const xlink="http://www.w3.org/1999/xlink";


class InteractivePlotterBase{

  constructor(margins = {left: 20, right: 0, top: 0, bottom: 20}) {
    this.margins= margins;
    this.svg = document.createElementNS(xmlns, "svg");

    this.parentElement= null;
    this.plotContent = null;
    this.axisGroup = null;
    this.yAxisLabelsGroup = null;
    this.yLabelText = null; // If null, y axis label and numbers are not painted
    this.noDataGroup = null;

    this.yZoom = 1;  // Initial zoom
    this.yOffset = 0; // Initial y offset = 0
    this.repaintListener = null;
    this.nodataLabel = null;

    this.outOfRangeColorActivated = true;
  }


  attach(element, width, height){
    this.parentElement= element;
    this.parentElement.appendChild(this.svg);
    this.width = (width !== undefined ? width : this.parentElement.clientWidth);
    this.height = (height !== undefined ? height : this.svg.width);
    this.svg.setAttribute("width", this.width);
    this.svg.setAttribute("height", this.height);
    this.plotRangeX = this.width - this.margins.left - this.margins.right;
    this.plotRangeY = this.height - this.margins.top - this.margins.bottom;

    // y axis area (zoomable)
    this.yAxisArea = document.createElementNS(xmlns, "svg");
    this.svg.appendChild(this.yAxisArea);
    let yAxisAreaWidth = this.margins.left;
    this.plotAreaHeight = this.height - this.margins.bottom - this.margins.top;
    this.yAxisArea.setAttribute("width", yAxisAreaWidth);
    this.yAxisArea.setAttribute("height", this.plotAreaHeight);//-OVERLAP_CORRECTOR);
    this.yAxisArea.setAttribute("x", 0);
    this.yAxisArea.setAttribute("y", 0);//OVERLAP_CORRECTOR);

    // SVG plot area window
    this.plotArea = document.createElementNS(xmlns, "svg");
    this.svg.appendChild(this.plotArea);
    this.plotAreaWidth = this.width - this.margins.left - this.margins.right;
    this.plotArea.setAttribute("width", this.plotAreaWidth);
    this.plotArea.setAttribute("height", this.plotAreaHeight);
    this.plotArea.setAttribute("x", this.margins.left);
    this.plotArea.setAttribute("y", this.margins.top);

    // Rect filling plot area in order to support styles and events
    this.plotAreaBg  = svg.addRect(this.plotArea, 0, 0, this.plotAreaWidth, this.plotAreaHeight);
    this.plotAreaBg.setAttribute('class', 'moveable-plot');
    this.plotAreaBg.setAttribute('opacity', 0.0);

    this. _events();
  }


  isAttached(){
    return this.parentElement !== null;
  }


  setAxisRangeAndLabels(xLabel, xMin, xMax, yLabel, yMinInit, yMaxInit,
    yMin, yMax, yLabelGap, decimals = 2){
    this.xLabel= xLabel;
    this.xMin = xMin;
    this.xMax = xMax;
    this.yMinInit= yMinInit;
    this.yMaxInit= yMaxInit;
    this.yMin= yMin;
    this.yMax= yMax;
    this.yLabelGapInit = yLabelGap;
    this.xRel= this.plotRangeX/(this.xMax-this.xMin);
    this.yRel= this.plotRangeY/(this.yMaxInit-this.yMinInit);

    this._resetAxisGroup();

    // Draw axes
    svg.addLine(this.axisGroup, 0,0,this.plotRangeX,0,  'main-axis');
  	svg.addLine(this.axisGroup, 0,0,0,-this.plotRangeY ,'main-axis');
    svg.addLine(this.axisGroup, this.plotRangeX, 0, this.plotRangeX, -this.plotRangeY, 'main-axis');
  	svg.addLine(this.axisGroup, 0,-this.plotRangeY, this.plotRangeX, -this.plotRangeY, 'main-axis');

    // Paint x and y axes labels
    if (yLabel !== null){
      this.yLabelText = svg.addText(this.svg, 0, 0, yLabel, 'middle', 'axis-steps-big');
      this.yLabelText.setAttribute('transform','translate(15,'+(this.plotRangeY/2+this.margins.top)+') rotate(-90)');
    }
    if (xLabel !== null)
      svg.addText(this.axisGroup, this.plotRangeX/2, this.margins.bottom-12, this.xLabel, 'middle', 'axis-steps-big');

    // initialize y axis steps (dynamic construction) container
    this._resetYAxisLabelGroup();

    // transformY precalcultation 1
    this.precalculation_1 = this.plotAreaHeight  + this.yMinInit*this.yRel;
    this.precalculation_2 = this.precalculation_1;//- this.yOffset // when this quantity changes
  }


  _events(){

    this.plotArea.addEventListener('wheel', (e) => {
      e.preventDefault();
      if (e.deltaY > 0 && this.yZoom > 0.5 ) this.yZoom -= 0.2;
      else if (e.deltaY < 0 && this.yZoom < 2) this.yZoom += 0.2;
      this.repaint();
      if (this.repaintListener !== null)
        this.repaintListener(this.yZoom, this.yOffset);
    });

    let initPosY;
    this.plotArea.addEventListener('mousedown', (e) => {
      e.preventDefault();
      initPosY = e.clientY + this.yOffset;
      //console.log('mousedown: e.clientY + this.yOffset', e.clientY, this.yOffset);
      this.plotArea.addEventListener('mousemove', moveListener);

      this.plotArea.addEventListener('mouseup', (e) => {
        this.plotArea.removeEventListener('mousemove', moveListener);
      });
      this.plotArea.addEventListener('mouseout', (e) => {
        this.plotArea.removeEventListener('mousemove', moveListener);
      });
    });

    let self = this;
    function moveListener(e) {
      //console.log('Y offset:', e.clientY - initPosY);
      // Bad if (initPosY - e.clientY > this.yMax || initPosY - e.clientY < this.yMin)
      self.yOffset = initPosY - e.clientY ;
      self.precalculation_2 = self.precalculation_1 - self.yOffset;
      self.repaint();
      if (self.repaintListener !== null)
        self.repaintListener(self.yZoom, self.yOffset);
    }
  }


  setYZoomAndOffset(yZoom, yOffset){
    this.yZoom = yZoom;
    this.yOffset = yOffset;
    this.precalculation_2 = this.precalculation_1 - this.yOffset;
  }


  setExternalYAxisMax(externalYAxisMax){
    this.externalYAxisMax = externalYAxisMax;
  }


  getYAxisMax(){
    return this.yAxisMax;
  }


  repaint(){
    // repaint Y axis
    this._resetYAxisLabelGroup();

    let yLabelGap;
    if (this.yZoom > 1) yLabelGap = this.yLabelGapInit/5;
    else yLabelGap = this.yLabelGapInit;

    let min = Math.floor(this.yMin/yLabelGap)*yLabelGap;
    let max = Math.ceil(this.yMax/yLabelGap)*yLabelGap;
    this.yAxisMax = max;
    if (this.externalYAxisMax !== undefined) max = this.externalYAxisMax;

    if (this.yLabelText !== null) {
      for (let i = min; i < max+1; i = i + yLabelGap) {
        svg.addLine(this.yAxisLabelsGroup, this.margins.left,
          this.transformY(i), this.margins.left-3, this.transformY(i));
        svg.addText(this.yAxisLabelsGroup, this.margins.left-5,
          this.transformY(i)+5, i, 'end', 'axis-steps');
      }
    }

    // repaint plot content
    this._resetPlotContent();

    // Out of range areas
    if (this.outOfRangeColorActivated){
      let area = svg.addRect(this.plotContent, 0, this.transformY(this.yMax)-2*this.plotAreaHeight, this.plotAreaWidth, 2*this.plotAreaHeight);
      area.setAttribute('class', 'out-of-range');
      let area1 = svg.addRect(this.plotContent, 0, this.transformY(this.yMin), this.plotAreaWidth, 2*this.plotAreaHeight);
      area1.setAttribute('class', 'out-of-range');
    }

    // Zero line
    svg.addLine(this.plotContent, 0, this.transformY(0), this.plotRangeX,
      this.transformY(0), 'zeroline');

    // repaint data lines
    this.repaintData(min, max);

    // Add the top layer: rect for events
    this.plotArea.removeChild(this.plotAreaBg);
    this.plotArea.appendChild(this.plotAreaBg);
  }


  setRepaintListener(listener) {
    this.repaintListener = listener;
  }


  transformY(y){
    // Precalculation usage
    // this.plotAreaHeight -y*this.yZoom*this.yRel + this.yMinInit*this.yRel - this.yOffset
    // -y*this.yZoom*this.yRel (calculated here)  this.plotAreaHeight  + this.yMinInit*this.yRel - this.yOffset
    let result = -y*this.yZoom*this.yRel + this.precalculation_2;
    if (result > 10000) throw 'Y coordinate too large';
    return result;
  }


  _resetPlotContent(){
    // TRy to delete with textContent property
    if (this.plotContent !== null)
      this.plotArea.removeChild(this.plotContent);
    this.plotContent = document.createElementNS(xmlns, "g");
    this.plotArea.appendChild(this.plotContent);
  }


  _resetAxisGroup(){
    if (this.axisGroup !== null)
      this.svg.removeChild(this.axisGroup);
    this.axisGroup = document.createElementNS(xmlns, 'g');
    this.svg.appendChild(this.axisGroup);

    // The y axis is inverted so the y coordinate has to be multiplied by -1
    this.axisGroup.setAttribute("transform", 'matrix(1 0 0 1 '+
      this.margins.left+' '+(this.height - this.margins.bottom)+')');
  }


  _resetYAxisLabelGroup(){
    if (this.yLabelText === null) return;

    if (this.yAxisLabelsGroup !== null)
      this.yAxisArea.removeChild(this.yAxisLabelsGroup);
    this.yAxisLabelsGroup = document.createElementNS(xmlns, 'g');
    this.yAxisArea.appendChild(this.yAxisLabelsGroup);
    //this.yAxisLabelsGroup.setAttribute("transform", 'matrix(1 0 0 1 0 -'+OVERLAP_CORRECTOR+')');
  }


  _reset(){
    this.yZoom = 1;  // Initial zoom
    this.yOffset = 0; // Initial y offset = 0

    // initialize plot content (dynamic construction) container
    this._resetPlotContent();

    if (this.yLabelText !== null){
      this.svg.removeChild(this.yLabelText);
      this.yLabelText = null;
    }

    if (this.noDataGroup !== null){
      this.svg.removeChild(this.noDataGroup);
      this.noDataGroup = null;
    }
  }


  setNoData(){
    this._resetYAxisLabelGroup();
    this._resetPlotContent();
    this._resetAxisGroup();
    if (this.noDataGroup === null){
      this.noDataGroup = document.createElementNS(xmlns, 'g');
      this.svg.appendChild(this.noDataGroup);
      svg.addRect(this.noDataGroup, 0, 0, this.width, this.height);
      this.noDataGroup.setAttribute('fill', '#EEE');
      svg.addText(this.noDataGroup, this.width/2, this.height/2+10,
        'NO DATA', 'middle', 'nodata');
    }

  }

}

// EXPORTS
module.exports = InteractivePlotterBase;
