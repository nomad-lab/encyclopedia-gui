
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This is an app-level utility JavaScript file. It holds:

      - Environments conf (API URL and user cookie domain configuration)
        * Maybe this conf info can be removed from here to a better place
      - global state variables and app-level constants
      - miscellaneous app-level function
      - local vars
      - app-level util functions

   * Maybe this file should be rethought
 */


"use strict";

let DataStore = require('../material-mod/DataStore.js');
let Conf = require('../Conf.js');


// global state vars
let materialId = null;


// app-level constants

const IMAGE_DIR = 'img/';

const AUTH_REQUEST_HEADER_GUEST_USER = 'Basic '+ btoa(Conf.GuestUserToken+':');

const MAT_VIEW = {
  'structure' : 'structure',
  'electronicstruct': 'electronicstruct',
  'methodology': 'methodology',
  'thermalprops': 'thermalprops',
  'elasticconst': 'elasticconst'
};

let ELEMENTS = [
  'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si',  // Si = 14
  'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', // Nin = 28
  'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb',  // Nb = 41
  'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe',  // Xe = 54
  'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', // Ho= 67
  'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg',  // Hg = 80
  'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np',  // Np = 93
  'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Ha', 'Sg', // sg = 106
  'Ns', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og' // Mt = 109
];


// API URL and user cookie domain configuration

// Case 1: my local dev environment
let  API_HOST= 'http://enc-staging-nomad.esc.rzg.mpg.de/';
let USER_COOKIE_DOMAIN = 'localhost';
//let path = 'current/v1.0/'; // current development version of the API
let path = 'v1.0/'; // stable staging

// Case 2: production environment
if (document.location.href.indexOf('nomad-coe.eu') > 0){//
  API_HOST='https://encyclopedia.nomad-coe.eu/';
  USER_COOKIE_DOMAIN = '.nomad-coe.eu';
  path = 'api/v1.0/';
}
 // Case 3: testing-staging environment and local installations (docker containers) of full Encyclopedia stack
/*else if (document.location.href.indexOf('gui') > 0){
  API_HOST= '/'; //  USER_COOKIE_DOMAIN = 'localhost' and path = 'v1.0/';
  if (document.location.href.indexOf('staging') > 0) // testing-staging server
    USER_COOKIE_DOMAIN = '.mpg.de';
}*/

let API_BASE_URL = API_HOST + path;
//console.log('API host: ',API_HOST,'  USER_COOKIE_DOMAIN: ',USER_COOKIE_DOMAIN);

document.querySelector('#guest-user a').href = API_BASE_URL+'saml/?sso2';


// Mockup URLs
//const FERMI_SURFACE_URL= HOST+'files/fermi/'+
//'fed3fa9fbc68aa6c5e51845396889666ca37bb2e626e1da53.x3d';



// Local variables

let authRequestHeaderValue = AUTH_REQUEST_HEADER_GUEST_USER;
//console.log('user: ANONYMOUS  authRequestHeader: ',authRequestHeaderValue);
let userData = null;


// app-level util functions

function getUserData(){
  return userData;
}


function getServerLocation(){
  return document.location.hostname;
}


function authServerReq(url, callback){
  var oReq = new XMLHttpRequest();
  oReq.addEventListener("load", callback);
  console.log('util.authServerReq: ', API_BASE_URL+url);
  oReq.open("GET", API_BASE_URL+url);
  //console.log('util.authServerReq oReq: ', oReq);
  oReq.send();
  return oReq;
}


function setAuthRequestHeader(userDataP, value){

  if (value === undefined){// default value
    authRequestHeaderValue = AUTH_REQUEST_HEADER_GUEST_USER;
    userData = null;
    //console.log('user: ANONYMOUS  authRequestHeader: ',authRequestHeaderValue);
  } else{
    authRequestHeaderValue = 'Basic '+ btoa(value+':');
    userData = userDataP;
    //console.log('user',user,'authRequestHeader: ',authRequestHeaderValue);
  }
}


function serverReq(url, callback){
  var oReq = new XMLHttpRequest();
  oReq.addEventListener("load", callback);
  oReq.open("GET", url);
  //console.log('authRequestHeaderValue: ',authRequestHeaderValue);
  oReq.setRequestHeader('Authorization', authRequestHeaderValue);
  oReq.send();
  return oReq;
}


function serverReqPOST(url, data, callback){
  var oReq = new XMLHttpRequest();
  oReq.addEventListener('load', callback);
  oReq.open('POST', url);
  oReq.setRequestHeader('Content-Type', 'application/json');
  //console.log('authRequestHeaderValue: ',authRequestHeaderValue);
  oReq.setRequestHeader('Authorization', authRequestHeaderValue);
  oReq.send(data);
  return oReq;
}


function getSubscriptedFormula(formula){
  let finalFormula= '';  // 'elementCode' :  number
  for (let i = 0; i < formula.length; i++){
    if (formula.charCodeAt(i) >= 47 && formula.charCodeAt(i) < 58 )
      finalFormula += '<sub>'+formula[i]+'</sub>';
    else finalFormula += formula[i];
    //console.log(formula.charCodeAt(i) + " "+finalFormula);
  }
  return finalFormula;
}


function getSearchURL(){
  return API_BASE_URL+'esmaterials';
}

function getSuggestionURL(quantity){
  return API_BASE_URL+'suggestions?property='+quantity;
}

function getMaterialURL(matId){
  return API_BASE_URL+'materials/'+matId;//'/materials/matid'; //
}

function getMaterialCalcURL(matId, calcId, property = ''){
  let propertyString = (property === '' ? '' : '?property='+property);
  return API_BASE_URL+'materials/'+matId+'/calculations/'+calcId+propertyString;
}

function getMaterialXsURL(what, matId){
  return API_BASE_URL+'materials/'+matId+'/'+what+'?pagination=off';//page=1&per_page=5000';//'/materials/calculations';//
}

function getCalcEnergiesURL(matId,calcId){
  return API_BASE_URL+'materials/'+matId+'/calculations/'+calcId+'/energies';//'/materials/calculations';//
}

function getFlaggingURL(){
  return API_BASE_URL+'flagme';
}


// Launch an app event
function setBrowserHashPath(modulePath, finalPath){
  if (typeof finalPath === 'undefined') document.location= '#/'+modulePath;
  else document.location= '#/'+modulePath+'/'+finalPath;
}

function loadLib(url){
  let script = document.createElement('script');
  script.setAttribute('type', 'text/javascript');
  script.setAttribute('src', url);
  document.getElementsByTagName('head')[0].appendChild(script);
}

function getNumberArray(string){
  let sArray= string.substring(1,string.length-1).split(',');
  let fArray= [];
  for (var i = 0; i < sArray.length; i++) {
    fArray.push(parseFloat(sArray[i]));
  }
  //console.log('getNumberArray.SPLIT: '+fArray);
  return fArray;
}

function getCellDataForViewer(matData){
  let cellData= {};
  cellData.normalizedCell= [
    getNumberArray(matData.cell.a),
    getNumberArray(matData.cell.b),
    getNumberArray(matData.cell.c)
  ];

  cellData.periodicity = JSON.parse(matData.periodicity);

  cellData.labels= [];
  cellData.positions= [];

  for (var i = 0; i < matData.elements.length; i++) {
    cellData.labels.push(matData.elements[i].label);
    cellData.positions.push(getNumberArray(matData.elements[i].position));
  }
  return cellData;
}


function J2eV(energy, decimals){
  let result= energy/1.602176565e-19;
  if (decimals === undefined){
    if (result < 0.01) return result.toFixed(6);
    else  return result.toFixed(3);
  }else{
    return result.toFixed(decimals);
  }
}

function eV2J(energy){
  return energy*1.602176565e-19;
}


/*
function getBandGapStatsValue(calcs){
  let bandGapSum= 0;
  let bandArray= [];
  let bandGapDirect= calcs[0].band_gap_direct;
  let bandGapType= (bandGapDirect ? "direct" : "indirect");

  for (var i = 0; i < calcs.length; i++) {
    //if (calcs[i].band_gap > 0){
      bandGapSum+= calcs[i].band_gap;
      bandArray.push(calcs[i].band_gap);
      if (calcs[i].band_gap_direct !== bandGapDirect)
        bandGapType= 'various results';
    //}
    //console.log(bandGapSum+'  '+calcs[i].band_gap+' '+bandArray.length);
  }

  let html= '';//let html= ((bandGapSum / bandArray.length)/1.602176565e-19).toFixed(3)+' eV ('+bandGapType+')';;
  let min= (Math.min.apply(null, bandArray)/1.602176565e-19).toFixed(3);
  let max= (Math.max.apply(null, bandArray)/1.602176565e-19).toFixed(3);
    html+= '&nbsp;('+min+' ... '+max+' eV)';
   //html+= '&nbsp;&nbsp;&nbsp;['+bandArray.length+' / '+calcs.length+']';

  return html;
}*/

function m2Angstrom(dist){
  return (dist/1e-10).toFixed(3)+' &#197;';
}


function getLatticeAnglesValues(calcs, twoD, bulk){
  let lattParams= [0.0, 0.0, 0.0];
  calcs.forEach( (calc) => {
    if (calc.lattice_parameters !== undefined && calc.lattice_parameters !== null){
      let tempLattParams= getNumberArray(calc.lattice_parameters);
      lattParams[0] += tempLattParams[3];
      lattParams[1] += tempLattParams[4];
      lattParams[2] += tempLattParams[5];
    }
  });

  if (bulk)
    return `<div>&alpha; = ${rad2degree(lattParams[0] / calcs.size)}</div>
    <div>&beta; = ${rad2degree(lattParams[1] / calcs.size)}</div>
    <div>&gamma; = ${rad2degree(lattParams[2] / calcs.size)}</div>`;
  else if (twoD)
    return `<div>&alpha; = ${rad2degree(lattParams[0] / calcs.size)}</div>`;
  else return ''; // 1D
}


function rad2degree(angle){
  return (angle * (180 / Math.PI)).toFixed(0)+'&deg;';
}

function m3ToAngstrom3(vol){
  return (vol/1e-30).toFixed(3)+' &#197;<sup>3</sup>';
}


function getAverage(array){
  let sum = 0;
  for (var i = 0; i < array.length; i++) sum += array[i];
  return sum/array.length;
}


function getQuantityStatsMap(calcs){

  let quantities = ['volume', 'atomic_density', 'mass_density', 'lattice_a', 'lattice_b', 'lattice_c'];
  let labels = ['Volume (Å³)', 'Atomic density (Å⁻³)', 'Mass density (kg/m³)', 'a (Å)', 'b (Å)', 'c (Å)'];
  let quantitiesMap = new Map();

  if (calcs.values().next().value.cell_volume === null){ // not bulk type volume of a calc null
    quantities = ['lattice_a', 'lattice_b', 'lattice_c'];
    labels = ['a (Å)', 'b (Å)', 'c (Å)'];
  }

  quantities.forEach( (quantity, index) => {
    let array= [];
    calcs.forEach( calc => {
      let value;
      if (quantity === 'volume') value = calc.cell_volume/1e-30;
      else if (quantity === 'atomic_density') value = calc.atomic_density*1e-30;
      else if (quantity === 'mass_density') value = calc.mass_density;
      else if (quantity.indexOf('lattice') >= 0){
        let tempLattParams= getNumberArray(calc.lattice_parameters);
        if (quantity === 'lattice_a') value = tempLattParams[0]/1e-10;
        else if (quantity === 'lattice_b') value = tempLattParams[1]/1e-10;
        else if (quantity === 'lattice_c') value = tempLattParams[2]/1e-10;
      }
      array.push(value);
    });
    let stats = {};
    stats.data = array;
    stats.min = Math.min.apply(null, array);
    stats.max = Math.max.apply(null, array);
    stats.equal = (stats.min === stats.max);
    let lls = labels[index].split(':');
    stats.label = lls[0];
    if (lls.length === 2)  stats.units = lls[1];
    else stats.units = '';

    let decimals = 3;
    if (quantity === 'mass_density') decimals = 1;
    stats.html = getAverage(stats.data).toFixed(decimals)+
      ' &nbsp; <span style="font-size: 0.9em">['+stats.min.toFixed(decimals)
      +' , '+stats.max.toFixed(decimals)+']</span>';

    quantitiesMap.set(quantity, stats);
  });
  return quantitiesMap;
}


function toAngstromMinus3(density){
  return (density*1e-30).toFixed(3)+' &#197;<sup>-3</sup>';
}


function getMaterialTitle(data, html){
  let title;
  title = getSubscriptedFormula(data.formula_reduced);
  if (html !== undefined && html ===false)   title = data.formula_reduced;

  if (data.space_group_number !== null)
    title += ' - space group '+data.space_group_number;
  //return '<span style="font-size: 0.9em">'+title+' </span>';
  return title;
}

function getMinMaxHTML(calcs,prop){
  let propArray= [];

  calcs.forEach( (calc) => {
    propArray.push(calc[prop]);
  });

  return '('+Math.min.apply(null, propArray)+' ... '+Math.max.apply(null, propArray)+')';
}


function generateDiagramSteps(maxVal){

  let d = 4; // generates 0 and 4 more points
  let exp = -Math.floor(Math.log(maxVal/d) * Math.LOG10E);

  let factor = Math.pow(10,exp);//100;
  //console.log('util.generateDiagramSteps  ',exp, maxVal/d, factor);
  let ceil = Math.ceil(maxVal*factor/d);
  let stepArray = [];
  for (var i = 0; i <= d; i++) {
    stepArray[i] = ceil*i/factor;
  }
  //console.log('stepArray '+stepArray);
  exp = (exp < 0 ? 0 : exp);
  return [stepArray, exp];
}



/*
function addBandGapData(calcJson, bsData){
  if (calcJson.band_gap > 0) {
    bsData.bandGapData = {};
    bsData.bandGapData.cbmEnergy = calcJson.band_gap_lower_energy;
    bsData.bandGapData.cbmKpt = getNumberArray(calcJson.band_gap_lower_kpt);
    bsData.bandGapData.vbmEnergy = calcJson.band_gap_upper_energy;
    bsData.bandGapData.vbmKpt = getNumberArray(calcJson.band_gap_upper_kpt);
  }
}*/

/*
function is2DSystem_temporary_patch(){

//console.log('TEMPORARY PATCH is2DSystem:', DataStore.getMaterialData());
  return DataStore.getMaterialData().system_type === '2D';
}
*/


module.exports = {
  materialId,
  MAT_VIEW: MAT_VIEW,
  IMAGE_DIR: IMAGE_DIR,
  USER_COOKIE_DOMAIN,
  ELEMENTS: ELEMENTS,
  setAuthRequestHeader,
  getUserData,
  getServerLocation,
  authServerReq,
  serverReq,
  serverReqPOST,
  getSearchURL: getSearchURL,
  getSuggestionURL,
  getMaterialURL: getMaterialURL,
  getMaterialCalcURL: getMaterialCalcURL,
  getMaterialXsURL: getMaterialXsURL,
  getCalcEnergiesURL: getCalcEnergiesURL,
  getFlaggingURL,
  setBrowserHashPath: setBrowserHashPath,
  loadLib: loadLib,
  getNumberArray: getNumberArray,
  getCellDataForViewer: getCellDataForViewer,
  //FERMI_SURFACE_URL: FERMI_SURFACE_URL,
  J2eV: J2eV,
  eV2J,
  //getBandGapStatsValue: getBandGapStatsValue,
  m2Angstrom: m2Angstrom,
  getLatticeAnglesValues: getLatticeAnglesValues,
  rad2degree: rad2degree,
  m3ToAngstrom3: m3ToAngstrom3,
  getQuantityStatsMap: getQuantityStatsMap,
  toAngstromMinus3,
  getMaterialTitle,
  getMinMaxHTML: getMinMaxHTML,
  getSubscriptedFormula: getSubscriptedFormula,
  getAverage,
  generateDiagramSteps,
  //is2DSystem_temporary_patch
  //addBandGapData
}
