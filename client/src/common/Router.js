
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This file implements the app Routing system: the feature that allows
   the page navigation in a single-page environment.
 */


"use strict";

let routes = new Map();


function add(route, func){
  routes.set(route, func);
}


window.addEventListener("hashchange", route);

function route(){
  let hashPath= document.location.hash.substring(2);
  let command, param, subparam;

  // remove the ending /
  if (hashPath.lastIndexOf('/') === (hashPath.length-1))
    hashPath = hashPath.substring(0,hashPath.length-1);

  if (hashPath.indexOf('/') >0){
    let a= hashPath.split('/');
    command= a[0];
    param= a[1];
    subparam= a[2];
  }
  else command= hashPath;

  //console.log("hashPath: " + hashPath+' command: '+command+' param: '+param+' subparam: '+subparam);

  if (routes.has(command)) {
    routes.get(command)(param, subparam);
  }
};


function print(){
  console.log('Router data: ');
  routes.forEach(function(func, url) {
    console.log(url + ': ' + func);
  });
}


// EXPORTS
module.exports = { add: add, route: route };
