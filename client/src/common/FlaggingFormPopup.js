
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This component is the popup window (and the darkened background) with a form
   inside used for users to report (flagging) errors on the calculations
 */

"use strict";

let util = require('./util.js');
let DataStore = require('../material-mod/DataStore.js');

// main DOM elements
let flaggingPopupBackground = document.querySelector('#flagging-form-popup-bg');
let flaggingFormPopup = document.querySelector('#flagging-form-popup');
flaggingFormPopup.innerHTML =`

<div> <img src="img/cross.svg"  height="12px"
  style="float: right; cursor: pointer" />
</div>

<div class="form-wrapper">
  <div class="popup-title"> Error reporting</div>
  <select id="flagging-category" name="category">
    <option value="">Select a category *</option>
    <option value="structure">Structure</option>
    <option value="electronicstruct">Electronic structure</option>
    <option value="methodology">Methodology</option>
    <option value="thermalprops">Thermal properties</option>
  </select>

  <select id="flagging-subcategory" name="subcategory">

  </select>

  <textarea id="subject" name="subject" style="height:200px"
  placeholder="Write a short explanation about the error detected" ></textarea>

  <div id="form-validation-msg"> </div>

  <div style="display: flex; justify-content: space-evenly;">
    <button style="display: block">Send</button>
  </div>


</div>
`;

// Form elements
let categoryField = flaggingFormPopup.querySelector('#flagging-category');
let eStructOption = categoryField.querySelector('option[value="electronicstruct"]');
let thermalOption = categoryField.querySelector('option[value="thermalprops"]');
let subcategoryField = flaggingFormPopup.querySelector('#flagging-subcategory');
let closeButton= flaggingFormPopup.querySelector('img');
let validationMsg = flaggingFormPopup.querySelector('#form-validation-msg');
let sendButton= flaggingFormPopup.querySelector('button');

let treeLeaf = null;
let overviewEStructCalcs = null;


function _setCurrentPage(pageId){

  subcategoryField.innerHTML = '';

  if (pageId === null){
    categoryField.disabled = false;
    subcategoryField.style.display = 'none';

  }else{
    categoryField.disabled = true;
    subcategoryField.style.display = 'block';
    subcategoryField.appendChild(createOption('Choose the subcategory *', ''));

    switch (pageId) {

      case util.MAT_VIEW.structure:
        categoryField.selectedIndex = 1;
        subcategoryField.appendChild(createOption('Structure representation'));
        subcategoryField.appendChild(createOption('Calculation tree'));
        subcategoryField.appendChild(createOption('Summary'));
        subcategoryField.appendChild(createOption('Specific calculation'));
        break;

      case util.MAT_VIEW.electronicstruct:
        categoryField.selectedIndex = 2;
        subcategoryField.appendChild(createOption('Calculation tree'));
        subcategoryField.appendChild(createOption('Summary'));
        subcategoryField.appendChild(createOption('Band structure'));
        subcategoryField.appendChild(createOption('DOS'));
        subcategoryField.appendChild(createOption('Brillouin zone'));
        break;

      case util.MAT_VIEW.methodology:
        categoryField.selectedIndex = 3;
        subcategoryField.style.display = 'none';
        break;

      case util.MAT_VIEW.thermalprops:
        categoryField.selectedIndex = 4;
        subcategoryField.appendChild(createOption('Calculation tree'));
        subcategoryField.appendChild(createOption('Phonon dispersion'));
        subcategoryField.appendChild(createOption('Phonon DOS'));
        subcategoryField.appendChild(createOption('Specific heat'));
        subcategoryField.appendChild(createOption('Helmholtz free energy'));
        break;
    }
  }

} // function _setCurrentPage


function show(pageStatus){
  //console.log('pageStatus : ',pageStatus);
  treeLeaf = pageStatus.markedLeaf;
  overviewEStructCalcs = pageStatus.eStructCalcs;

  // Show/hide some dropdown list options
  eStructOption.style.display = (DataStore.hasElecStructureData ? 'block' : 'none');
  thermalOption.style.display = (DataStore.hasThermalData ? 'block' : 'none');

  _setCurrentPage(pageStatus.pageId);

  let ttRect = flaggingFormPopup.getBoundingClientRect();
  let leftPos =  (window.innerWidth - ttRect.width)/2;
  let topPos = (window.innerHeight -ttRect.height)/2;
  flaggingFormPopup.style.left = leftPos+'px';
  flaggingFormPopup.style.top = (topPos-20)+'px';

  flaggingFormPopup.style.visibility = 'visible';
  flaggingPopupBackground.style.visibility = 'visible';
}


function hide(){
  flaggingPopupBackground.style.visibility = 'hidden';
  flaggingFormPopup.style.visibility = 'hidden';
  // reset UI
  categoryField.selectedIndex = 0;
  subcategoryField.selectedIndex = 0;
  flaggingFormPopup.querySelector('textarea').value = '';
  validationMsg.innerHTML = '';
}


function createOption(text, value){
  let opt = document.createElement('option');
  opt.value = (value === undefined ? text : value);
  opt.innerHTML = text;
  return opt;
}


closeButton.addEventListener('click', e => {
  hide();
});


sendButton.addEventListener('click', e => {

  let categoryChosen = categoryField.options[categoryField.selectedIndex];

  if (!categoryField.disabled && categoryChosen.value === ''){ // Overview case
    validationMsg.innerHTML = 'The category fields must be set';

  }else if (categoryField.disabled && subcategoryField.value === '' // Detaisl pages case
            && categoryChosen.value !== util.MAT_VIEW.methodology){
    validationMsg.innerHTML = 'The subcategory fields must be set';

  }else{
    validationMsg.innerHTML = 'Sending report...';
    let textareaText = flaggingFormPopup.querySelector('textarea').value;
    let materialId = DataStore.getMaterialData().id;
    let userdata = util.getUserData();


    let titleText = 'User issue | Material '+materialId;
    let descriptionText = '**Server:** '+util.getServerLocation()+
      '\\n\\n**User:** '+userdata.username+', '+userdata.email;

    // Overview page
    if ( !categoryField.disabled){
      descriptionText += '\\n\\n**Category:** Overview / '+categoryChosen.text;

      if (categoryChosen.value === util.MAT_VIEW.electronicstruct
          && overviewEStructCalcs !== null)
        descriptionText += '\\n\\n**Chosen calculations:** '+
          (overviewEStructCalcs.bs === null ? '' : 'BS calculation '+overviewEStructCalcs.bs)+
          (overviewEStructCalcs.dos === null ? '' : ' DOS calculation '+overviewEStructCalcs.dos);

    }else{ // Details pages
      descriptionText += '\\n\\n**Category:** '+categoryChosen.text;

      if (categoryChosen.value !== util.MAT_VIEW.methodology){
        descriptionText += '\\n\\n**Subcategory:** '+
          subcategoryField.options[subcategoryField.selectedIndex].text+
          '\\n\\n**Calculation/group marked on the tree:** '+treeLeaf;
      }
    }

    descriptionText += '\\n\\n**User text:** '+textareaText;

    let queryJson =`{
      "title": "${titleText}",
      "description": "${descriptionText}"}`;
    console.log('Flagging POST request Json: ',queryJson);//, util.getFlaggingURL());


    util.serverReqPOST(util.getFlaggingURL(), queryJson, e => {
      console.log('response',e);
      if (e.target.status === 200) hide();
    });

  }

});



// EXPORTS
module.exports = { show, hide };
