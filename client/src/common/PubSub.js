
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This file implements the design pattern pub/sub, which allows one module
	 to broadcast messages to other modules.
 */

"use strict";

var messages = new Map();
var lastUid = -1;

  /**
   *	subscribe( message, func ) -> String
   *	- message (String): The message to subscribe to
   *	- func (Function): The function to call when a new message is published
   *	Subscribes the passed function to the passed message. Every returned token
   *	is unique and should be stored if you need to unsubscribe
  **/
function subscribe( message, func ){
  //console.log('SUBSCRIBING Message '+message);
	if ( typeof func !== 'function'){
		return false;
	}
	// message is not registered yet
	if ( !messages.has( message ) ){
		messages.set( message, new Map());
	}
	// forcing token as String, to allow for future expansions without breaking usage
	// and allow for easy use as key names for the 'messages' object
	var token = 'uid_' + String(++lastUid);
	messages.get(message).set(token, func); //messages[message][token] = func;
  //print();
	// return token for unsubscribing
	return token;
}


function publish( message, data){

  var hasSubscribers = messages.has( message )
                       && (messages.get( message ).size > 0);

	if ( !hasSubscribers ){
		return false;
  }

  var deliver = function (){
    //deliverMessage(message, data);
    var subscribers = messages.get(message);
    //console.log('DELIVERING Message '+message);
    subscribers.forEach(function(func, token) {
      func(data);
      //console.log('EXE funct   '+ func);
    });
  };

  setTimeout( deliver, 0 );  // async
	return true;
}


function print(){
  console.log('PubSub data: ');
  messages.forEach(function(functions, msg) {
    console.log(msg + ': ');

    functions.forEach(function(func, token) {
      console.log('    '+token + ': ' + func);
    });
  });
}

// EXPORTS
module.exports = { subscribe: subscribe, publish: publish };
