
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /*
  This is the base class that models a regular plotter.
  It is inherited by several classes implementing specifc plotters
 */

"use strict";


let svg = require('./SVG.js');

const xmlns="http://www.w3.org/2000/svg";
const xlink="http://www.w3.org/1999/xlink";


class PlotterBase{

  constructor(margins = {left: 20, right: 0, top: 10, bottom: 20}) {
    this.svg = document.createElementNS(xmlns, "svg");
    this.plotArea = document.createElementNS(xmlns, "g");
    this.svg.appendChild(this.plotArea);

    this.margins= margins;
    this.parentElement= null;
    this.yLabelText = null;
    this.noDataGroup = null;
  }


  attach(element, width, height){
    this.parentElement= element;
    this.parentElement.appendChild(this.svg);
    this.width = (width !== undefined ? width : this.parentElement.clientWidth);
    this.height = (height !== undefined ? height : this.svg.width);
    this.svg.setAttribute("width", this.width);
    this.svg.setAttribute("height", this.height);
    this.plotRangeX = this.width - this.margins.left - this.margins.right;
    this.plotRangeY = this.height - this.margins.top - this.margins.bottom;
  }


  isAttached(){
    return this.parentElement !== null;
  }


  setRangeAndLabels(xLabel, xMin, xMax, yLabel, yMin, yMax){
    this.xLabel= xLabel;
    this.xMin = xMin;
    this.xMax = xMax;
    this.yLabel= yLabel;
    this.yMin= yMin;
    this.yMax= yMax;

    this.xRel= this.plotRangeX/(this.xMax-this.xMin);
    this.yRel= this.plotRangeY/(this.yMax-this.yMin);
  }


  drawAxis(xSteps = 0, ySteps = 0, decimals = 2){

	  this.plotArea.setAttribute("transform", 'matrix(1 0 0 1 '+
      this.margins.left+' '+(this.height  - this.margins.bottom)+')');

    this.yLabelText = svg.addText(this.svg, 0, 0, this.yLabel, 'middle', 'axis-steps-big');
    this.yLabelText.setAttribute('transform','translate(13,'
      +(this.plotRangeY/2+this.margins.top)+') rotate(-90)');
	  svg.addText(this.plotArea, this.plotRangeX/2, this.margins.bottom-1,
      this.xLabel, 'middle', 'axis-steps-big');

    if (xSteps !== null){
      let xStep= this.plotRangeX/ xSteps;
      for (let i = 0; i <= xSteps; i++) {
        svg.addLine(this.plotArea, xStep*i, 0, xStep*i, 4, 1);
        svg.addText(this.plotArea, xStep*i, 14,
          +((xStep*i/this.xRel)+this.xMin).toFixed(decimals), 'middle', 'statisticsviewersteps');
      }
    }

    if (ySteps === null && this.yMax > 0 && this.yMin < 0){
      let i = 1;
      while(this.yMax*i > this.yMin) {
        svg.addLine(this.plotArea, 0, this.transformY(this.yMax*i), -3, this.transformY(this.yMax*i), 1);
        let numberText = (Math.abs(this.yMax*i) >= 10000 ? (this.yMax*i).toExponential() : this.yMax*i );
        svg.addText(this.plotArea,-5, this.transformY(this.yMax*i)+3, numberText,
          'end', 'statisticsviewersteps');
        i--;
      }
    }

    if (ySteps !== null){
        let yStep= this.plotRangeY/ ySteps;
        for (let i = 0; i <= ySteps; i++) {
          svg.addLine(this.plotArea, 0, -yStep*i, -3, -yStep*i, 1);
          let numberToPaint= (yStep*i/this.yRel) + this.yMin;
          // Fix to prevent the the -0 printing
          if (Math.abs(numberToPaint) < 0.01) numberToPaint = 0;
          //console.log('drawAxis', yStep, i, this.yRel, this.yMin, numberToPaint);
          svg.addText(this.plotArea,-5, -(yStep*i-3), numberToPaint.toFixed(decimals), 'end', 'statisticsviewersteps');
        }
    }

	  svg.addLine(this.plotArea, 0, 0, this.plotRangeX+1 ,0 ,'main-axis');
	  svg.addLine(this.plotArea, 0,0,0,-(this.plotRangeY+1) ,'main-axis');
    svg.addLine(this.plotArea, this.plotRangeX, 0, this.plotRangeX, -this.plotRangeY ,'main-axis');
  	svg.addLine(this.plotArea, 0,-this.plotRangeY, this.plotRangeX, -this.plotRangeY,'main-axis');
  }


  clear(){
    this.svg.removeChild(this.plotArea);
    this.plotArea = document.createElementNS(xmlns, "g");
    this.svg.appendChild(this.plotArea);
    this.plotArea.setAttribute("transform", 'matrix(1 0 0 1 '+
        this.margins.left+' '+(this.height  - this.margins.bottom)+')');

    if (this.yLabelText !== null){
      this.svg.removeChild(this.yLabelText);
      this.yLabelText = null;
    }
    if (this.noDataGroup !== null){
      this.svg.removeChild(this.noDataGroup);
      this.noDataGroup = null;
    }
  }


  setNoData(){
    this.clear();
    if (this.noDataGroup === null){
      this.noDataGroup = document.createElementNS(xmlns, 'g');
      this.svg.appendChild(this.noDataGroup);
      svg.addRect(this.noDataGroup, 0, 0, this.width, this.height);
      this.noDataGroup.setAttribute('fill', '#EEE');
      svg.addText(this.noDataGroup, this.width/2, this.height/2+10,
        'NO DATA', 'middle', 'nodata');
    }

  }


  // Transform from y-axis units to  y-axis pixels (svg-coordinates)
  transformY(y){
    return -this.yRel*(y - this.yMin);
  }

}


// EXPORTS
module.exports = PlotterBase;
