
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
    This file implements the user guidance system. This complex system for
    showing tips to the user is specified in the project documentation 
 */


"use strict";

//let util = require('util.js');

let body = document.querySelector('body');
let tips = [];
let tipData = [];
let currentTip;// = 1;
let independentTips = []; // if null => showing else if 'off' not showing
let addBox, table, searchBox, propertiesTabs;
let searchBarShowing = false;


function createUserMsg(i, width){
  let element= document.createElement('div');
  element.setAttribute('class','user-guidance');
  element.innerHTML = '<img src="img/tip'+i+'.png" width="'+width+'px" />';
  element.style.position = 'absolute';
  element.style.display = 'none';
  body.appendChild(element);
  return element;
}


function showTip(tipNumber){
  if (tipNumber === 8 && !searchBarShowing) return; // Do nothing
  if (tipNumber === 4 && !searchBarShowing) return; // Do nothing

  let r = tipData[tipNumber].domTarget.getBoundingClientRect();
//console.log("GGGGGGGGG: ",r);
  tips[tipNumber].style.top = (r.top + tipData[tipNumber].top + window.pageYOffset)+'px';
  tips[tipNumber].style.left = (r.left + tipData[tipNumber].left)+'px';
  tips[tipNumber].style.display = 'block';
}

// Init per user session
function init(addBoxP, tableP, searchBoxP, propertiesTabsP){
  addBox = addBoxP;
  table = tableP;
  searchBox = searchBoxP;
  propertiesTabs = propertiesTabsP;

  independentTips[3] = localStorage.getItem('tip3');
  independentTips[4] = localStorage.getItem('tip4');
  independentTips[7] = localStorage.getItem('tip7');

  createUserTips();
  let currentTip = localStorage.getItem('currentTip');
  if (currentTip === null){ // First time
    updateCurrentTip(1);
  }else{
    currentTip = parseInt(currentTip);
    if (currentTip < 10){ // regular case
      updateCurrentTip(currentTip);
    }//else {  currentTip === 10 the guidance has finished
  }
}


function createUserTips(){

  if (tips.length === 0){
    tips[1] = createUserMsg(1, 220);
    tipData[1] = {domTarget: addBox, top: -70, left: -240 };
    tips[2] = createUserMsg(2, 280);
    tipData[2] = {domTarget: addBox, top: -110, left: 80 };
    tips[3] = createUserMsg(3, 180);
    tipData[3] = {domTarget: table, top: 180, left: 720 };
    tips[4] = createUserMsg(4, 240);
    tipData[4] = {domTarget: searchBox, top: 45, left: -250 };
    tips[5] = createUserMsg(5, 210);
    tipData[5] = {domTarget: addBox, top: -130, left: 70};
    tips[6] = createUserMsg(6, 240);
    tipData[6] = {domTarget: addBox, top: -100, left: 370};
    tips[7] = createUserMsg(7, 220);
    tipData[7] = {domTarget: propertiesTabs, top: 160, left: -40};
    tips[8] = createUserMsg(8, 240);
    tipData[8] = {domTarget: searchBox, top: 30, left: 760};

    // Event
    tips[1].addEventListener( "click", closeAndShowNext);
    tips[2].addEventListener( "click", closeAndShowNext);
    tips[3].addEventListener( "click", e => { closeIndependentTip(3) });
    tips[4].addEventListener( "click", e => { closeIndependentTip(4) });
    tips[5].addEventListener( "click", closeAndShowNext);
    tips[6].addEventListener( "click", closeAndShowNext);
    tips[7].addEventListener( "click",  e => { closeIndependentTip(7) });
    tips[8].addEventListener( "click", e => {
      e.target.style.display = 'none';
      updateCurrentTip(10);
    });
  }
}


function setFinal(){
  searchBarShowing = true;
  if (currentTip < 10){
    tips[currentTip].style.display = 'none';
    updateCurrentTip(8);//currentTip = 4;
    showTip(currentTip);
  }
  showIndependentTip(4, true);
}


function closeIndependentTip( tipNumber){
  //e.preventDefault();
  tips[tipNumber].style.display = 'none';
  localStorage.setItem('tip'+tipNumber, 'off');
  independentTips[tipNumber] = 'off';
}


function closeAndShowNext(e){
  e.preventDefault();
  //console.log("closeAndShowNext",currentTip);
  e.target.style.display = 'none';

  switch (currentTip) {
    case 2:  currentTip = 5;  break;

    case 6:  currentTip = 8;  break;

    default: // 1 , 5
      currentTip++;
  }
  updateCurrentTip(currentTip);
  showTip(currentTip);
}


function showIndependentTip(tipNumber, value){
  //console.log("showIndependentTip",tipNumber);
  if (independentTips[tipNumber] === null){ // Tip has not been removed (clicked)
    if (value) showTip(tipNumber);
    else tips[tipNumber].style.display = 'none';
  }
}




function show(value, tip3, tip7){ // Global show - the UserGuidance is shown or hidden at all
  if (currentTip < 10){ // sequential tips
    if (value) showTip(currentTip);
    else tips[currentTip].style.display = 'none';
  }
  // Independent tips
  showIndependentTip(3, value && tip3);
  showIndependentTip(4, value);
  showIndependentTip(7, value && tip7);
}


function updateCurrentTip(value){
  currentTip = value;
  localStorage.setItem('currentTip', value);
  //console.log('localStorage.currentTip:',localStorage.getItem('currentTip'));
}




// EXPORTS
module.exports = {init, setFinal, show, showIndependentTip}
