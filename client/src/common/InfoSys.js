
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   This file implements an information system for the user that provides info
   related to the concepts or quantities shown on the UI
   The literals are read form the file infosys.json
 */

"use strict";

let util = require('./util.js');
let SwitchComponent = require('./SwitchComponent.js');

const INFOSYS_FILE_PATH = 'infosys.json';

let tooltip = document.querySelector('#info-tooltip');
let tooltipContent = document.querySelector('#tooltip-content');

let elements = [];
let timerSet = null;
let on = false;
let data = null;

function clearCurrentTimeoutAnSetANew(){
  if (timerSet !== null)  window.clearTimeout(timerSet);
  timerSet = window.setTimeout(t => tooltip.style.display = 'none', 1000);
}


let switchComponent = new SwitchComponent(util.IMAGE_DIR+'switch');
document.querySelector('#info-sys-switch-box').appendChild(switchComponent.element);

switchComponent.setListener( off => {
  on = !off;
  if (off) {
    elements.forEach( element => {
      element.removeEventListener('mouseover', mouseOver);
      element.className = '';
    });
  }else{
    if (data === null)
      util.serverReq(INFOSYS_FILE_PATH, e => data = JSON.parse(e.target.response));

    elements.forEach(enableTooltip);
  }


  tooltip.addEventListener('mouseover', e => {
    window.clearTimeout(timerSet);
  });

  tooltip.addEventListener('mouseout', e => {
    clearCurrentTimeoutAnSetANew();
  });

});


function addToInfoSystem(baseElement){
  let infosysLabels = baseElement.querySelectorAll('span[info-sys-data]');

  for (let i = 0; i < infosysLabels.length; ++i)
    elements.push(infosysLabels[i]);

  //if (on)  infosysLabels.forEach(enableTooltip);
  if (on)
    for (let i = 0; i < infosysLabels.length; ++i)
      enableTooltip(infosysLabels[i]);
}


function addElementToInfoSystem(element, value){
  elements.push(element);
  element.setAttribute('info-sys-data',value);

  if (on) enableTooltip(element);
}


function enableTooltip(element){
  element.addEventListener('mouseover', mouseOver);
  element.addEventListener('mouseout', e => {
    clearCurrentTimeoutAnSetANew();
    element.style.cursor = 'inherit';
  });
  element.className = 'info-sys-label';
}


function mouseOver(e){
  let r = e.target.getBoundingClientRect();
  let quantity =  e.target.getAttribute('info-sys-data');

  let index = quantity.indexOf('.value');
  if (index > 0){ // quantity value
    let quantityObject = data[quantity.split('-').join(' ').substring(0, index)];
    //console.log('VALUE', quantityObject);
    if (quantityObject.value_template === undefined){ //direct value
      let valueObj = quantityObject.values[quantity.split(':')[1]];
      tooltipContent.innerHTML = getHTML(valueObj);
    }else{ // value template
      let object = quantityObject.value_template;
      object.text = templating(object.text, quantity.split(':')[1]);
      object.link = templating(object.link, quantity.split(':')[1]);
      tooltipContent.innerHTML = getHTML(object);
      //console.log('VALUE TEMPLATE: ', object);
    }
  }else // quantity name
    tooltipContent.innerHTML = getHTML(data[quantity.split('-').join(' ')]);

  tooltip.style.visibility = 'hidden';
  tooltip.style.display = 'block';
  let ttRect = tooltip.getBoundingClientRect();
  let leftOffset = ttRect.width - r.width;
  let leftPos = r.left -leftOffset/2;
  if (leftPos + ttRect.width > window.innerWidth)
    leftPos = window.innerWidth -ttRect.width;
  //let topOffset = ttRect.height + 20 - window.pageYOffset;
  let topOffset =  - window.pageYOffset;
  tooltip.style.left = (leftPos < 0 ? 5 : leftPos)+'px';
  tooltip.style.top = (r.top + r.height -topOffset)+'px';
  tooltip.style.visibility = 'visible';
  window.clearTimeout(timerSet);
  e.target.style.cursor = 'help';
}


function templating(s, param) {
  let initIndex = s.indexOf('${');
  let finalIndex = s.indexOf('}');
  if (initIndex >= 0 && finalIndex >= 0 && finalIndex > initIndex){
    return s.substring(0,initIndex)+param+s.substring(finalIndex+1);
  }else return s;
}

function getHTML(object) {
  let html = '';
  if (object.text === undefined){
    //html = 'NO TEXT!! Comment: '+object.comment;
  }else{ // there is text attr
    html += object.text;
  }
  if (object.link !== undefined){
    if (object.text !== undefined)   html += '<br>';
    html += '<a href="'+object.link+'" target="_blank">More information</a>';
  }

  return html;
}


// EXPORTS
module.exports = { addToInfoSystem, addElementToInfoSystem };
