
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
    This component implements a generic switch/toggle button
 */


let util = require('./util.js');

class SwitchComponent {

  constructor(imageBasePath) {

    this.off = true;

    this.element = document.createElement('span');
    this.element.innerHTML+=`
       <img src="${imageBasePath}_off.png" width="24px"
        style="margin-bottom: -1px; cursor: pointer"/>
    `;
    this.image = this.element.querySelector('img');

    this.element.addEventListener('click', e => {
      this.off = !this.off;
      let imagePath = (this.off ? imageBasePath+'_off' : imageBasePath);
      this.image.setAttribute('src',imagePath+'.png');
      this.listener(this.off);
    });
  }


  setListener(listener){
    this.listener = listener;
  }
}

// EXPORTS
module.exports = SwitchComponent;
