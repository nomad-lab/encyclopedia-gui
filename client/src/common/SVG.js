
/**
 * Copyright 2016-2018 Iker Hurtado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


 /*
   SVG drawing util library
 */


"use strict";

const xmlns="http://www.w3.org/2000/svg";
const xlink="http://www.w3.org/1999/xlink";


function addPoint(parent, x, y, r, className) {
  let e = document.createElementNS(xmlns, "circle");
  e.setAttribute("r", r); // e.setAttributeNS(null, "r", 5);
  e.setAttribute("cx", x);
  e.setAttribute("cy", y);
  if (className !== undefined) e.setAttribute("class", className);
  parent.appendChild(e);
  return e;
}

function addCircle(parent, x, y, r, fillColor, strokeColor, strokeWidth) {
  let e = document.createElementNS(xmlns, "circle");
  e.setAttribute("r", r); // e.setAttributeNS(null, "r", 5);
  e.setAttribute("cx", x);
  e.setAttribute("cy", y);
  e.setAttribute("fill", fillColor);
  e.setAttribute("stroke", strokeColor);
  e.setAttribute("stroke-width", strokeWidth);
  parent.appendChild(e);
  return e;
}


function addLine(parent, x1, y1, x2, y2, className) {
  let e = document.createElementNS(xmlns, "line");
  e.setAttribute("x1", x1);
  e.setAttribute("y1", y1);
  e.setAttribute("x2", x2);
  e.setAttribute("y2", y2);
  if (className !== undefined)  e.setAttribute("class", className);
  //e.setAttribute("stroke-width", stroke);
  parent.appendChild(e);
  return e;
}


function addRect(parent, x, y, w, h) {
  let e = document.createElementNS(xmlns, "rect");
  e.setAttribute("x", x);
  e.setAttribute("y", y);
  e.setAttribute("width", w);
  e.setAttribute("height", h);
  parent.appendChild(e);
  return e;
}



function addText(parent, x, y, text, textAnchor = 'start', className) {
  let e = document.createElementNS(xmlns, "text");
  e.setAttribute("x", x);
  e.setAttribute("y", y);
  e.textContent= text;
  //e.setAttribute("stroke", 'black');
  e.setAttribute("text-anchor", textAnchor);
  if (className !== undefined)  e.setAttribute("class", className);
  parent.appendChild(e);
  return e;
}


function addPolyline(parent, points, className) {
  let e = document.createElementNS(xmlns, "polyline");
  e.setAttribute("points", points);
  if (className !== undefined)  e.setAttribute("class", className);
  //e.setAttribute("stroke-width", stroke);
  parent.appendChild(e);
}


function removeElement(element){
  element.parentElement.removeChild(element);
}



// EXPORTS
module.exports = {
  addPoint: addPoint,
  addCircle,
  addLine: addLine,
  addRect: addRect,
  addText: addText,
  addPolyline: addPolyline,
  removeElement: removeElement
};
